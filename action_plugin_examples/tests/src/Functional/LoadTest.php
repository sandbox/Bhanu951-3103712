<?php

declare(strict_types = 1);

namespace Drupal\Tests\action_plugin_examples\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test Action Plugin Examples Module.
 *
 * @group action_plugin_examples
 */
final class LoadTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['node', 'block', 'action_plugin_examples'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->drupalPlaceBlock('page_title_block');
  }

  /**
   * Tests Homepage after enabling Action Plugin Examples Module.
   */
  public function testHomepage() {
    // Test homepage.
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);

    // Minimal homepage title.
    $this->assertSession()->pageTextContains('Log in');
  }

  /**
   * Test Admin Pages access.
   */
  public function testAdminPageAccess() {
    $admin_user = $this->drupalCreateUser(['access administration pages']);
    $this->drupalLogin($admin_user);
    $this->drupalGet('admin');
    $this->assertSession()->elementExists('xpath', '//h1[text() = "Administration"]');
  }

  /**
   * Tests the Action Plugin Examples module unistall.
   */
  public function testModuleUninstall() {

    $admin_user = $this->drupalCreateUser([
      'access administration pages',
      'administer site configuration',
      'administer modules',
    ]);

    // Uninstall the module.
    $this->drupalLogin($admin_user);
    $this->drupalGet('/admin/modules/uninstall');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Action Plugin Examples');
    $this->submitForm(['uninstall[action_plugin_examples]' => TRUE], 'Uninstall');
    $this->submitForm([], 'Uninstall');
    $this->assertSession()->pageTextContains('The selected modules have been uninstalled.');
    $this->assertSession()->pageTextNotContains('Action Plugin Examples');

    // Visit the frontpage.
    $this->drupalGet('');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Tests Action Plugin Examples module reinstalling after being uninstalled.
   */
  public function testReinstallAfterUninstall() {

    $admin_user = $this->drupalCreateUser([
      'access administration pages',
      'administer site configuration',
      'administer modules',
    ]);

    // Uninstall the module.
    $this->drupalLogin($admin_user);
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    // Uninstall the Action Plugin Examples module.
    $this->container->get('module_installer')->uninstall(['action_plugin_examples'], FALSE);

    $this->drupalGet('/admin/modules');
    $page->checkField('modules[action_plugin_examples][enable]');
    $page->pressButton('Install');
    $assert_session->pageTextNotContains('Unable to install Action Plugin Examples');
    $assert_session->pageTextContains('Module Action Plugin Examples has been enabled');
  }

}
