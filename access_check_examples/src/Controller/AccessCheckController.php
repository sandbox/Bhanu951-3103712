<?php

namespace Drupal\access_check_examples\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;

/**
 * Returns responses for Access Check routes.
 */
class AccessCheckController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];

    return $build;
  }

  /**
   * Builds the response.
   */
  public function info() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('Custom Access Check Works!'),
    ];

    return $build;
  }

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account) {

    $uid = $account->id();

    return AccessResult::allowedIf($account->hasPermission('administer access check examples configuration') && $uid == 1);
  }

}
