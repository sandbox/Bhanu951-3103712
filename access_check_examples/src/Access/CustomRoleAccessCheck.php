<?php

namespace Drupal\access_check_examples\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Symfony\Component\Routing\Route;
use Drupal\Core\Database\Connection;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Checks if the route has required permission.
 */
class CustomRoleAccessCheck implements AccessInterface {

  use StringTranslationTrait;

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * User Login Subscriber constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct(Connection $database, MessengerInterface $messenger, LoggerChannelFactoryInterface $logger, DateFormatterInterface $date_formatter) {
    $this->database = $database;
    $this->messenger = $messenger;
    $this->logger = $logger->get('access_check_examples');
    $this->dateFormatter = $date_formatter;

  }

  /**
   * A custom access check.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(Route $route, AccountInterface $account) {

    if (!$route->hasRequirement('_custom_role_access_check')) {
      return AccessResult::neutral();
    }

    $permission = $route->getRequirement('_custom_role_access_check');

    $uid = $account->id();

    $this->messenger->addMessage($this->t('Your UID is :  %uid', ['%uid' => $uid]), 'notice');

    $this->logger->notice('Your UID is :  @uid', ['@uid' => $uid]);

    return AccessResult::allowedIf($account->hasPermission('administer access check examples configuration') && $permission)->addCacheContexts(['route']);
  }

}
