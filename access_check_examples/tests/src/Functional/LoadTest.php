<?php

declare(strict_types=1);

namespace Drupal\Tests\access_check_examples\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test Access Check Examples Module.
 *
 * @group access_check_examples
 */
class LoadTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['node', 'block', 'access_check_examples'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->drupalPlaceBlock('page_title_block');
  }

  /**
   * Tests Homepage after enabling Access Check Examples Module.
   */
  public function testHomepage() {
    // Test homepage.
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);

    // Minimal homepage title.
    $this->assertSession()->pageTextContains('Log in');
  }

  /**
   * Test Admin Pages access.
   */
  public function testAdminPageAccess() {
    $admin_user = $this->drupalCreateUser(['access administration pages']);
    $this->drupalLogin($admin_user);
    $this->drupalGet('admin');
    $this->assertSession()->elementExists('xpath', '//h1[text() = "Administration"]');
  }

  /**
   * Tests the Access Check Examples module unistall.
   */
  public function testModuleUninstall() {

    $admin_user = $this->drupalCreateUser([
      'access administration pages',
      'administer site configuration',
      'administer modules',
    ]);

    // Uninstall the module.
    $this->drupalLogin($admin_user);
    $this->drupalGet('/admin/modules/uninstall');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Access Check Examples');
    $this->submitForm(['uninstall[access_check_examples]' => TRUE], 'Uninstall');
    $this->submitForm([], 'Uninstall');
    $this->assertSession()->pageTextContains('The selected modules have been uninstalled.');
    $this->assertSession()->pageTextNotContains('Access Check Examples');

    // Visit the frontpage.
    $this->drupalGet('');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Tests the Access Check Examples module reinstalling after being uninstalled.
   */
  public function testReinstallAfterUninstall() {

    $admin_user = $this->drupalCreateUser([
      'access administration pages',
      'administer site configuration',
      'administer modules',
    ]);

    // Uninstall the module.
    $this->drupalLogin($admin_user);
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    // Uninstall the Access Check Examples module.
    $this->container->get('module_installer')->uninstall(['access_check_examples'], FALSE);

    $this->drupalGet('/admin/modules');
    $page->checkField('modules[access_check_examples][enable]');
    $page->pressButton('Install');
    $assert_session->pageTextNotContains('Unable to install Access Check Examples');
    $assert_session->pageTextContains('Module Access Check Examples has been enabled');
  }

}
