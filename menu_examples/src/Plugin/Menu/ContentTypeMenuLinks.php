<?php

namespace Drupal\menu_examples\Plugin\Menu;

use Drupal\Core\Menu\MenuLinkDefault;

/**
 * Represents a menu link for a single Book.
 */
class ContentTypeMenuLinks extends MenuLinkDefault {}
