<?php

namespace Drupal\menu_examples\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\node\Entity\Node;

/**
 * Derivative class that provides the menu links for the Books.
 */
class ContentTypeMenuLinksDerivative extends DeriverBase implements ContainerDeriverInterface {

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Creates a ContentTypeMenuLinksDerivative instance.
   *
   * @param string $base_plugin_id
   *   Plugin Base ID.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Type Manager.
   */
  public function __construct($base_plugin_id, EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $base_plugin_id,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $links = [];

    // Get all nodes of type book.
    $storage = $this->entityTypeManager->getStorage('node');
    $nodeQuery = $storage->getQuery();
    $nodeQuery->condition('type', 'book');
    $nodeQuery->condition('status', TRUE);
    $ids = $nodeQuery->execute();
    $ids = array_values($ids);

    $nodes = Node::loadMultiple($ids);

    foreach ($nodes as $node) {
      $links['menu_examples_book_menu_' . $node->id()] = [
        'title' => $node->get('title')->getString(),
        'menu_name' => 'books-menu',
        // 'parent'=> 'menu_examples.base',
        'route_name' => 'entity.node.canonical',
        'route_parameters' => [
          'node' => $node->id(),
        ],
      ] + $base_plugin_definition;
    }
    return $links;
  }

}
