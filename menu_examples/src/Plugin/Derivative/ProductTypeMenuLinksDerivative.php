<?php

namespace Drupal\menu_examples\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;

/**
 * Derivative class that provides the menu links for the Products.
 */
class ProductTypeMenuLinksDerivative extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $links = [];

    $products = [
      'cooking_gear' => 'Cooking Gear',
      'tents' => 'Tents' ,
      'sleeping_bags' => 'Sleeping Bags',
      'rope' => 'Rope',
      'safety' => 'Safety',
      'packs' => 'Packs',
    ];

    foreach ($products as $key => $value) {
      $links['menu_examples_products_menu_' . $key] = [
        'title' => $value . ' Controller',
        'parent' => 'menu_examples.dynamic_routes',
        'route_name' => 'menu_examples.dynamic_routes' . $key,
      ] + $base_plugin_definition;
    }
    return $links;
  }

}
