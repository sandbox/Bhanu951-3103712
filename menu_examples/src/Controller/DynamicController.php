<?php

namespace Drupal\menu_examples\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Component\Utility\Unicode;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class DynamicController.
 *
 * @package Drupal\menu_examples\Controller
 */
class DynamicController extends ControllerBase {

  /**
   * The user account data service.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * DynamicController constructor.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The currently logged in account.
   */
  public function __construct(AccountInterface $current_user) {
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user')

    );
  }

  /**
   * Returns a static message.
   */
  public function details() {

    $build['output'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Products List Controller!'),
    ];
    return $build;
  }

  /**
   * Product Type Route Callback.
   *
   * @param string $type
   *   The product type.
   *
   * @return string
   *   The product type message.
   */
  public function productType($type) {

    // Use current user after pass authentication to validate access.
    if (!$this->currentUser->hasPermission('administer menu examples ' . $type)) {

      // Display the default access denied page.
      throw new AccessDeniedHttpException('Access Denied.');
    }

    $build['results'] = [
      '#markup' => $this->t('@type Product Details.', ['@type' => Unicode::ucfirst($type)]),
    ];

    return $build;
  }

}
