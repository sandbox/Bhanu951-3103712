<?php

namespace Drupal\menu_examples\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Dynamic Route Subscriber Examples route subscriber.
 */
class DynamicRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {

    $products = [
      'cooking_gear' => 'Cooking Gear',
      'tents' => 'Tents' ,
      'sleeping_bags' => 'Sleeping Bags',
      'rope' => 'Rope',
      'safety' => 'Safety',
      'packs' => 'Packs',
    ];

    foreach ($products as $key => $value) {

      $url = preg_replace('/_/', '-', $key);

      $route = new Route(
      // The url path to match.
        '/examples-codes/menu-examples/product-details/' . $url,
        // The defaults (see the custom_module.dynamic_routes for structure)
        [
          '_title' => $value . ' Controller',
          '_controller' => '\Drupal\menu_examples\Controller\DynamicController::productType',
          'type' => $value,
        ],
        // The requirements.
        [
          '_permission' => 'administer menu examples ' . $value,
        ]
      );

      // Add our route to the collection with a unique key.
      $collection->add('menu_examples.dynamic_routes' . $key, $route);

    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {

    $events = parent::getSubscribedEvents();

    // Use a lower priority than \Drupal\views\EventSubscriber\RouteSubscriber
    // to ensure the requirement will be added to its routes.
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -300];

    return $events;
  }

}
