<?php

namespace Drupal\table_examples\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Returns responses for students marks.
 */
class StudentsListController extends ControllerBase {

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;
  /**
   * The Database Connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;
  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger channel factory.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The mssenger service.
   */
  public function __construct(LoggerChannelFactoryInterface $logger, Connection $database, MessengerInterface $messenger) {
    $this->logger = $logger->get('table_examples');
    $this->database = $database;
    $this->messenger = $messenger;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory'),
      $container->get('database'),
      $container->get('messenger')

    );
  }

  /**
   * Builds the response.
   */
  public function results(Request $request) {

    $rows = $header = [];

    // Get parameter value while submitting filter form.
    $ht_no = $request->query->get('ht_no');
    $subject_code = $request->query->get('subject_code');

    // Load Search Form.
    $build['form'] = $this->formBuilder()->getForm('Drupal\table_examples\Form\SearchForm');

    $build['add_student_detail'] = [
      '#title' => $this->t('Add'),
      '#type' => 'link',
      '#url' => Url::fromRoute('table_examples.add'),
    ];

    // Create table header.
    $header = [
      [
        'data' => $this->t('S.No.'),
        'field'     => 'serial_number',
        'specifier' => 'serial_number',
      ],
      [
        'data' => $this->t('Hallticket No'),
        'field'     => 'ht_no',
        'specifier' => 'ht_no',
      ],
      [
        'data' => $this->t('Subject Code'),
        'field'     => 'subject_code',
        'specifier' => 'subject_code',
      ],
      [
        'data' => $this->t('Subject Name'),
        'field'     => 'subject_name',
        'specifier' => 'subject_name',
      ],
      [
        'data' => $this->t('Internal Marks'),
        'field'     => 'internal_marks',
        'specifier' => 'internal_marks',
      ],
      [
        'data' => $this->t('External Marks'),
        'field'     => 'external_marks',
        'specifier' => 'external_marks',
      ],
      [
        'data' => $this->t('Total Marks'),
        'field'     => 'total_marks',
        'specifier' => 'total_marks',
      ],
      [
        'data' => $this->t('Credits'),
        'field'     => 'credits',
        'specifier' => 'credits',
      ],
      [
        'data' => $this->t('Action'),
        'field'     => 'action',
        'specifier' => 'action',
      ],
    ];

    $query = $this->database->select('table_examples_students_data', 'sd');

    $query->fields('sd', [
      'serial_number',
      'ht_no',
      'subject_code',
      'subject_name',
      'internal_marks',
      'external_marks',
      'total_marks',
      'credits',
    ]);

    if (!empty($ht_no) && !empty($subject_code)) {

      $query->condition('sd.ht_no', "%" . $query->escapeLike($ht_no) . "%", 'LIKE');

      $query->condition('sd.subject_code', "%" . $query->escapeLike($subject_code) . "%", 'LIKE');

    }
    else {

      $query->condition('sd.serial_number', 0, '<>');

    }

    // Table Sort.
    $table_sort = $query->extend('Drupal\Core\Database\Query\TableSortExtender')
      ->orderByHeader($header);

    // Pager.
    $pager = $table_sort->extend('Drupal\Core\Database\Query\PagerSelectExtender')
      ->limit(20);

    // Results count.
    $results_count = $query->countQuery()->execute()->fetchField();

    // Results.
    $results = $pager->execute()->fetchAllAssoc('serial_number');

    foreach ($results as $key => $value) {

      // Actions.
      $options = [
        'absolute' => TRUE,
        'attributes' => [
          'target' => '_blank',
          'class' => 'button button--primary js-form-submit form-submit',
          'onclick' => "return confirm('Are you Sure')",
        ],
      ];

      $edit_link = Link::createFromRoute($this->t('Edit'), 'table_examples.delete_results',
        [
          'ht_no' => $value->ht_no,
          'subject_code' => $value->subject_code,
        ], $options
      );

      $delete_link = Link::createFromRoute($this->t('Delete'), 'table_examples.delete_results',
       [
         'ht_no' => $value->ht_no,
         'subject_code' => $value->subject_code,
       ], $options
      );

      $build_link_action = [
        'action_edit' => [
          '#type' => 'html_tag',
          '#value' => $edit_link->toString(),
          '#tag' => 'div',
          '#attributes' => ['class' => ['action-edit']],
        ],
        'action_delete' => [
          '#type' => 'html_tag',
          '#value' => $delete_link->toString(),
          '#tag' => 'div',
          '#attributes' => ['class' => ['action-edit']],
        ],
      ];

      $rows[$value->serial_number] = [
        'serial_number' => $value->serial_number,
        'ht_no' => $value->ht_no,
        'subject_code' => $value->subject_code,
        'subject_name' => $value->subject_name,
        'internal_marks' => $value->internal_marks,
        'external_marks' => $value->external_marks,
        'total_marks' => $value->total_marks,
        'credits' => $value->credits,
        'action' => \Drupal::service('renderer')->render($build_link_action),
      ];

    }

    $build['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No records found'),
    ];

    $build['pager'] = [
      '#type' => 'pager',
    ];

    return $build;
  }

  /**
   * Builds the response.
   */
  public function recordDelete($ht_no, $subject_code) {

    if (!empty($ht_no) && !empty($subject_code)) {

      $query = $this->database->delete('table_examples_students_data');

      $query->condition('ht_no', $this->database->escapeLike($ht_no), 'LIKE');

      $query->condition('subject_code', $this->database->escapeLike($subject_code), 'LIKE');

      $result = $query->execute();

    }

    $message = $this->t('Student details for Hallticket No : <em> @ht_no </em> and Subject Code: <strong> @subject_code </strong> has been deleted.',
      [
        '@ht_no' => $ht_no,
        '@subject_code' => $subject_code,
      ]
    );

    $this->messenger->addStatus($message);
    $this->logger->notice($message);

    return $this->redirect('table_examples.results');

  }

}
