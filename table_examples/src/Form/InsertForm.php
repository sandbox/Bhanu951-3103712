<?php

namespace Drupal\table_examples\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Database\Connection;

/**
 * Provides a table examples insert form.
 */
class InsertForm extends FormBase {


  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;


  /**
   * Database service.
   *
   * @var \Drupal\Core\Database\Connection
   */

  protected $database;

  /**
   * {@inheritdoc}
   */
  public function __construct(LoggerChannelFactoryInterface $logger, MessengerInterface $messenger, Connection $connection) {
    $this->logger = $logger->get('table_examples');
    $this->messenger = $messenger;
    $this->database = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'table_examples_insert_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = [];

    $form['ht_no'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hall Ticket No.'),
      '#required' => TRUE,
      '#maxlength' => 20,
      '#default_value' => '',
    ];

    $form['subject_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject Code.'),
      '#required' => TRUE,
      '#maxlength' => 10,
      '#default_value' => '',
    ];

    $form['subject_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject Name.'),
      '#required' => TRUE,
      '#maxlength' => 50,
      '#default_value' => '',
    ];

    $form['internal_marks'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Internal Marks.'),
      '#required' => TRUE,
      '#maxlength' => 5,
      '#default_value' => '',
    ];

    $form['external_marks'] = [
      '#type' => 'textfield',
      '#title' => $this->t('External Marks.'),
      '#required' => TRUE,
      '#maxlength' => 20,
    ];

    $form['total_marks'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Total Marks.'),
      '#required' => TRUE,
      '#maxlength' => 20,
      '#default_value' => '',
    ];

    $form['credits'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Credits.'),
      '#required' => TRUE,
      '#maxlength' => 20,
      '#default_value' => '',
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (mb_strlen($form_state->getValue('ht_no')) != 10) {
      $form_state->setErrorByName('name', $this->t('Ht.No. should be 10 characters.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $data = $form_state->getValues();

    $input_data = [
      'ht_no' => $data['ht_no'],
      'subject_code' => $data['subject_code'],
      'subject_name' => $data['subject_name'],
      'internal_marks' => $data['internal_marks'],
      'external_marks' => $data['external_marks'],
      'total_marks' => $data['total_marks'],
      'credits' => $data['credits'],
    ];

    $query = $this->database->insert('table_examples_students_data');

    $query->fields($input_data);
    $query->execute();

    $this->logger->notice('Data Inserted.');

    $this->messenger->addStatus($this->t('The message has been sent.'));
    $this->logger->notice($this->t('The message has been sent.'));
    $form_state->setRedirect('<current>');
  }

}
