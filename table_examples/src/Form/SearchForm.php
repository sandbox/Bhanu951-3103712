<?php

namespace Drupal\table_examples\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Url;

/**
 * Provides a table examples insert form.
 */
class SearchForm extends FormBase {


  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;


  /**
   * Database service.
   *
   * @var \Drupal\Core\Database\Connection
   */

  protected $database;

  /**
   * {@inheritdoc}
   */
  public function __construct(LoggerChannelFactoryInterface $logger, MessengerInterface $messenger, Connection $connection) {
    $this->logger = $logger->get('table_examples');
    $this->messenger = $messenger;
    $this->database = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'table_examples_search_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = [];

    $form['filters'] = [
      '#type'  => 'details',
      '#title' => $this->t('Filter'),
      '#open' => TRUE,
    ];

    $form['filters']['ht_no'] = [
      '#type' => 'search',
      '#title' => $this->t('Hall Ticket No.'),
    ];

    $form['filters']['subject_code'] = [
      '#type' => 'search',
      '#title' => $this->t('Subject Code.'),
    ];

    $form['filters']['actions'] = [
      '#type' => 'actions',
    ];

    $form['filters']['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search'),
    ];

    $form['filters']['actions']['reset'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reset'),
      // Custom submission handler for 'Reset' button.
      '#submit' => ['::resetForm'],
      // We won't bother validating the required field.
      '#limit_validation_errors' => [],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    if ($form_state->getValue('ht_no') == "") {
      $form_state->setErrorByName('ht_no', $this->t('You must enter a valid HallTicket No.'));
    }
    elseif ($form_state->getValue('subject_code') == "") {
      $form_state->setErrorByName('subject_code', $this->t('You must enter a valid Subject Code.'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $field = $form_state->getValues();

    $ht_no = $field["ht_no"];
    $subject_code = $field["subject_code"];

    $url = Url::fromRoute('table_examples.results')
      ->setRouteParameters(
        [
          'ht_no' => $ht_no,
          'subject_code' => $subject_code,
        ]
      );

    $form_state->setRedirectUrl($url);
  }

  /**
   * {@inheritdoc}
   */
  public function resetForm(array &$form, FormStateInterface $form_state) {

    $field = $form_state->getValues();
    $url = Url::fromRoute('table_examples.results');
    $form_state->setRedirectUrl($url);
  }

}
