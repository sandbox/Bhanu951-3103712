<?php

namespace Drupal\field_examples\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'Custom Text' field type.
 *
 * @FieldType(
 *   id = "CustomText",
 *   label = @Translation("Custom Text"),
 *   description = @Translation("Accepts only lowercase alphabets."),
 *   module = "field_examples",
 *   category = @Translation("Custom"),
 *   default_widget = "CustomTextWidget",
 *   default_formatter = "CustomTextFormatter"
 * )
 */
class CustomText extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {

    $schema = [];
    $schema['custom_text'] = [
      'type' => 'text',
      'size' => 'tiny',
      'not null' => FALSE,
      'description' => 'The Custom Text field Which only accepts lowercase alphabets.',
    ];

    return [
      'columns' => $schema,
      'indexes' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    $properties = [];

    $properties['custom_text'] = DataDefinition::create('string')->setLabel(t('Custom Text'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('custom_text')->getValue();
    return $value === NULL || $value === '';
  }

}
