<?php

namespace Drupal\field_examples\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'CustomAddressFormatter' formatter.
 *
 * @FieldFormatter(
 *   id = "CustomAddressFormatter",
 *   label = @Translation("CustomAddress"),
 *   field_types = {
 *     "CustomAddress"
 *   }
 * )
 */
class CustomAddressFormatter extends FormatterBase {

  /**
   * Define how the field type is showed.
   *
   * Inside this method we can customize how the field is displayed inside
   * pages.
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#type' => 'markup',
        '#markup' => $item->street . ', ' . $item->city,
      ];
    }

    return $elements;
  }

}
