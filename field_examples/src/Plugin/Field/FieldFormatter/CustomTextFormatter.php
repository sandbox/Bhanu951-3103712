<?php

namespace Drupal\field_examples\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'CustomTextFormatter' formatter.
 *
 * @FieldFormatter(
 *   id = "CustomTextFormatter",
 *   label = @Translation("CustomText"),
 *   field_types = {
 *     "CustomText"
 *   }
 * )
 */
class CustomTextFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Accepts only lowercase alphabets.');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      // Render each element as markup.
      $element[$delta] = [
        '#markup' => $item->custom_text,
      ];
    }

    return $element;
  }

}
