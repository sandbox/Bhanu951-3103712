<?php

namespace Drupal\field_examples\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'CustomTextWidget' widget.
 *
 * @FieldWidget(
 *   id = "CustomTextWidget",
 *   label = @Translation("CustomAddress Select"),
 *   field_types = {
 *     "CustomText"
 *   }
 * )
 */
class CustomTextWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $value = $items[$delta]->custom_text ?? NULL;
    $element += [
      '#type' => 'textfield',
      '#default_value' => $value,
      '#size' => $this->getSetting('size'),
      '#empty_value' => '',
      '#placeholder' => $this->getSetting('placeholder'),
      '#description' => $this->getSetting('description'),
      '#element_validate' => [
        [static::class, 'validate'],
      ],
    ];
    return ['value' => $element];
  }

  /**
   * Validate the custom text field.
   */
  public static function validate($element, FormStateInterface $form_state) {

    $value = $element['#value'];

    if (strlen($value) == 0) {
      $form_state->setValueForElement($element, '');
      return;
    }
    if (!preg_match('/^[b-y]+$/', $value)) {
      $form_state->setError($element, t("Please enter only lowercase alphabets between b and y."));
    }
  }

  /**
   * Default settings for  the custom text field.
   */
  public static function defaultSettings() {
    return [
      'size' => '60',
      'placeholder' => t('custom text field'),
      'description' => t('Enter Custom Text.'),
    ] + parent::defaultSettings();
  }

}
