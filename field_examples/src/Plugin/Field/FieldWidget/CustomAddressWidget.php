<?php

namespace Drupal\field_examples\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'CustomAddressWidget' widget.
 *
 * @FieldWidget(
 *   id = "CustomAddressWidget",
 *   label = @Translation("CustomAddress Select"),
 *   field_types = {
 *     "CustomAddress"
 *   }
 * )
 */
class CustomAddressWidget extends WidgetBase {

  /**
   * Define the form for the field type.
   *
   * Inside this method we can define the form used to edit the field type.
   *
   * Here there is a list of allowed element types: https://goo.gl/XVd4tA
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $formState) {

    // Street.
    $element['street'] = [
      '#type' => 'textfield',
      '#title' => t('Street'),

      // Set here the current value for this field, or a default value (or
      // null) if there is no a value.
      '#default_value' => $items[$delta]->street ?? NULL,

      '#empty_value' => '',
      '#placeholder' => t('Street'),
    ];

    // City.
    $element['city'] = [
      '#type' => 'textfield',
      '#title' => t('City'),
      '#default_value' => $items[$delta]->city ?? NULL,
      '#empty_value' => '',
      '#placeholder' => t('City'),
    ];

    return $element;
  }

}
