<?php

namespace Drupal\controller_examples\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Serialization\Json;

/**
 * Class CustomPagerController.
 *
 * @package Drupal\controller_examples\Controller
 */
class CustomPagerController extends ControllerBase {

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The pager manager.
   *
   * @var \Drupal\Core\Pager\PagerManagerInterface
   */
  protected $pagerManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->messenger = $container->get('messenger');
    $instance->logger = $container->get('logger.factory')->get('controller_examples');
    $instance->moduleHandler = $container->get('module_handler');
    $instance->pagerManager = $container->get('pager.manager');
    return $instance;
  }

  /**
   * Returns a render-able array for a location import.
   */
  public function import() {

    $header = $decoded = [];

    $header = ['State Id', 'State Name', 'Country Id'];

    $moduleExist = $this->moduleHandler->moduleExists('example_codes');

    if ($moduleExist) {

      $modulePath = $this->moduleHandler->getModule('example_codes')->getPath();
      $file = $modulePath . '/data/states.json';
      $locationData = file_get_contents($file);
      $decoded = Json::decode($locationData);
      $this->logger->notice($this->t('Sates count is %count', ['%count' => count($decoded['states'])]));
    }
    else {
      $this->logger->error('Example Codes Module Not Exist');
      $this->messenger->addError("Example Codes Module Not Exist");
    }

    $rows = $decoded['states'];

    $rowPiece = $this->pagerArray($rows, 25);

    $build['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rowPiece,
      '#empty' => $this->t('No content has been found.'),
    ];
    $build['pager'] = [
      '#type' => 'pager',
    ];
    return $build;
  }

  /**
   * Returns pager array.
   */
  public function pagerArray($items, $itemsPerPage) {
    // Get total items count.
    $total = count($items);
    // Get the number of the current page.
    $currentPage = $this->pagerManager->createPager($total, $itemsPerPage)->getCurrentPage();
    // Split an array into chunks.
    $chunks = array_chunk($items, $itemsPerPage);
    // Return current group item.
    $currentPageItems = $chunks[$currentPage];
    return $currentPageItems;
  }

}
