<?php

namespace Drupal\controller_examples\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class StyledController.
 *
 * @package Drupal\controller_examples\Controller
 */
class StyledController extends ControllerBase {

  /**
   * Returns a render-able styled message in the page.
   */
  public function styling() {

    $olddata = $this->t('Old Text without Styling!...');
    $newdata = $this->t('New Text For Styling!...');
    $build['output'] = [
      '#theme' => 'styled_controller_output',
      '#olddata' => $olddata,
      '#newdata' => $newdata,
      '#attached' => [
        'library' => [
          'controller_examples/styled_controller.styles',
        ],
      ],
    ];
    return $build;
  }

}
