<?php

namespace Drupal\controller_examples\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatterInterface;

/**
 * Class DBQueryController.
 *
 * @package Drupal\controller_examples\Controller
 */
class DBQueryController extends ControllerBase {
  /**
   * The Database Connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('date.formatter')
    );
  }

  /**
   * DBQueryController constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct(Connection $database, DateFormatterInterface $date_formatter) {
    $this->database = $database;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * Display the query output.
   *
   * @return array
   *   Return query output table.
   */
  public function queryOutput() {

    $header = [];

    $header = [
            [
              'data' => $this->t('WID'),
              'field'     => 'wid',
              'specifier' => 'wid',
            ],
            [
              'data' => $this->t('UID'),
              'field'     => 'uid',
              'specifier' => 'uid',
            ],
            [
              'data' => $this->t('Type'),
              'field'     => 'type',
              'specifier' => 'type',
            ],
            [
              'data' => $this->t('Severity'),
              'field'     => 'severity',
              'specifier' => 'severity',
            ],
            [
              'data' => $this->t('Created'),
              'field'     => 'timestamp',
              'specifier' => 'timestamp',
            ],
    ];

    // $query = $this->database->select('watchdog', 'w');
    // $query->fields('w', ['wid', 'uid', 'type', 'severity','timestamp']);
    // $query->condition('w.wid', 0, '<>');
    // $query->range(0, 500);
    // // Use orderBy if you are not user Header sort.
    // // $query->orderBy('timestamp', 'DESC');
    // // The actual action of sorting the rows is here.
    // $table_sort = $query
    // ->extend('Drupal\Core\Database\Query\TableSortExtender')
    // ->orderByHeader($header);
    // // Rows to 20 for each page.
    // $pager = $table_sort
    // ->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(20);
    // Do Note that groupBy can not be used when
    // Building a query with the database api
    // And groupby multiple fields in Drupal
    // To overcome this you have to add multiple groupBy queries.
    // Issue Discussions.
    // https://drupal.stackexchange.com/questions/206714/ .
    // https://www.drupal.org/project/drupal/issues/2989922#comment-12715521 .
    // https://drupal.stackexchange.com/q/206169/ .
    // $query->addExpression('count(w.type)', 'type_count');
    // $query->groupBy("w.type");
    // $query->groupBy("w.wid");
    // $query->groupBy("w.uid");
    // $query->groupBy("w.severity");.
    $query = $this->database->select('watchdog', 'w');
    $query->fields('w', ['wid', 'uid', 'type', 'severity', 'timestamp']);
    $query->condition('w.wid', 0, '<>');
    $query->range(0, 500);
    // Table Sort.
    $table_sort = $query->extend('Drupal\Core\Database\Query\TableSortExtender')
      ->orderByHeader($header);
    // Pager.
    $pager = $table_sort->extend('Drupal\Core\Database\Query\PagerSelectExtender')
      ->limit(20);

    // Results count.
    $results_count = $query->countQuery()->execute()->fetchField();
    // Results.
    $results = $pager->execute()->fetchAllAssoc('wid');

    // Create the row element.
    $rows = [];
    foreach ($results as $row => $wdlog) {
      $timestamp = $wdlog->timestamp;
      $created = [
        'data' => [
          '#theme' => 'time',
          '#text' => $this->dateFormatter->format($timestamp),
          '#attributes' => [
            'datetime' => $this->dateFormatter->format($timestamp, 'custom', \DateTime::RFC3339),
          ],
        ],
      ];

      $rows[$wdlog->wid] = [
        $wdlog->wid,
        $wdlog->uid,
        $wdlog->type,
        $wdlog->severity,
        $created,
      ];
    }
    // The table description.
    $build['markup'] = [
      '#markup' => $this->t('Count of All Watchdog Errors <em> @results_count </em>',
      ['@results_count' => $results_count]),
    ];

    $build['table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No Watchdog Errors has been found.'),
    ];

    $build['pager'] = [
      '#type' => 'pager',
    ];
    return $build;
  }

  /**
   * Display the join query output.
   *
   * @return array
   *   Return join query output table.
   */
  public function joinDbQueryOutput() {
    $header = [];

    $header = [
            [
              'data' => $this->t('NID'),
              'field'     => 'nid',
              'specifier' => 'nid',
            ],
            [
              'data' => $this->t('VID'),
              'field'     => 'vid',
              'specifier' => 'vid',
            ],
            [
              'data' => $this->t('Type'),
              'field'     => 'type',
              'specifier' => 'type',
            ],
            [
              'data' => $this->t('UUID'),
              'field'     => 'uuid',
              'specifier' => 'uuid',
            ],
            [
              'data' => $this->t('Langcode'),
              'field'     => 'langcode',
              'specifier' => 'langcode',
            ],
            [
              'data' => $this->t('Revision UID'),
              'field'     => 'revision_uid',
              'specifier' => 'revision_uid',
            ],
            [
              'data' => $this->t('Revision Timestamp'),
              'field'     => 'revision_timestamp',
              'specifier' => 'revision_timestamp',
            ],
            [
              'data' => $this->t('Revision Log'),
              'field'     => 'revision_log',
              'specifier' => 'revision_log',
            ],
            [
              'data' => $this->t('Revision Default'),
              'field'     => 'revision_default',
              'specifier' => 'revision_default',
            ],
    ];

    $query = $this->database->select('node_revision', 'nr');
    $query->innerJoin('node', 'n', 'n.nid = nr.nid');
    $query->fields('n', ['nid', 'vid', 'type', 'uuid', 'langcode']);
    $query->fields('nr', ['langcode', 'revision_uid', 'revision_timestamp',
      'revision_log', 'revision_default',
    ]);
    $query->addField('n', 'nid', 'node_uid');
    $query->addField('nr', 'nid', 'revision_uid');

    // Table Sort.
    $table_sort = $query->extend('Drupal\Core\Database\Query\TableSortExtender')
      ->orderByHeader($header);

    // Pager.
    $pager = $table_sort->extend('Drupal\Core\Database\Query\PagerSelectExtender')
      ->limit(20);

    // Results count.
    $results_count = $query->countQuery()->execute()->fetchField();
    // Results.
    $results = $pager->execute()->fetchAllAssoc('nid');

    // Create the row element.
    $rows = [];
    foreach ($results as $row => $nodedata) {
      $timestamp = $nodedata->revision_timestamp;
      $created = [
        'data' => [
          '#theme' => 'time',
          '#text' => $this->dateFormatter->format($timestamp),
          '#attributes' => [
            'datetime' => $this->dateFormatter->format($timestamp, 'custom', \DateTime::RFC3339),
          ],
        ],
      ];

      $rows[$nodedata->nid] = [
        $nodedata->nid,
        $nodedata->vid,
        $nodedata->type,
        $nodedata->uuid,
        $nodedata->langcode,
        $nodedata->revision_uid,
        $created,
        $nodedata->revision_log,
        $nodedata->revision_default,

      ];
    }
    $build['markup'] = [
      '#markup' => $this->t('Count of All Watchdog Errors'),
    ];
    $build['table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No Watchdog Errors has been found.'),
    ];

    $build['pager'] = [
      '#type' => 'pager',
    ];
    return $build;
  }

}
