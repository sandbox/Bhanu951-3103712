<?php

namespace Drupal\controller_examples\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class HelloController.
 *
 * @package Drupal\controller_examples\Controller
 */
class HelloController extends ControllerBase {

  /**
   * Returns a render-able array for a Hello World page.
   */
  public function content() {
    $build['output'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Hello World!'),
    ];
    return $build;
  }

}
