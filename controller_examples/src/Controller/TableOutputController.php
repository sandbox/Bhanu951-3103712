<?php

namespace Drupal\controller_examples\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\NodeInterface;
use Drupal\Core\Datetime\DateFormatterInterface;

/**
 * Class TableOutputController.
 *
 * @package Drupal\controller_examples\Controller
 */
class TableOutputController extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * EntityModerationForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, DateFormatterInterface $date_formatter) {
    $this->entityTypeManager = $entity_type_manager;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('date.formatter')
    );
  }

  /**
   * Returns a render-able array for a Hello World page.
   */
  public function tableContent() {

    $header = [];

    $header = [
      'nid' => [
        'data' => $this->t('ID'),
        'specifier' => 'nid',
      ],
      'title' => [
        'data' => $this->t('Title'),
        'specifier' => 'title',
      ],
      'created' => [
        'data' => $this->t('Created'),
        'specifier' => 'created',
        // Set default sort criteria.
        'sort' => 'desc',
      ],
      'uid' => [
        'data' => $this->t('Author'),
        'specifier' => 'uid',
      ],
    ];

    $storage = $this->entityTypeManager()->getStorage('node');

    $query = $storage->getQuery();
    $query->condition('status', NodeInterface::PUBLISHED);
    $query->condition('type', 'article');
    $query->tableSort($header);
    // Default value is 10.
    $query->pager(15);
    $nids = $query->execute();

    $rows = [];
    foreach ($storage->loadMultiple($nids) as $node) {
      $row = [];
      $row[] = $node->id();
      $row[] = $node->toLink();
      $created = $node->get('created')->value;
      $row[] = [
        'data' => [
          '#theme' => 'time',
          '#text' => $this->dateFormatter->format($created),
          '#attributes' => [
            'datetime' => $this->dateFormatter->format($created, 'custom', \DateTime::RFC3339),
          ],
        ],
      ];
      $row[] = [
        'data' => $node->get('uid')->view(),
      ];
      $rows[] = $row;
    }

    $build['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No content has been found.'),
    ];

    $build['pager'] = [
      '#type' => 'pager',
    ];
    return $build;
  }

}
