<?php

namespace Drupal\controller_examples\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class DynamicController.
 *
 * @package Drupal\controller_examples\Controller
 */
class DynamicController extends ControllerBase {

  /**
   * Returns a render-able array for a Hello World page.
   */
  public function content($itemList) {
    $build['output'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Product @itemList', ['@itemList' => $itemList]),
    ];
    return $build;
  }

}
