(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.styled_controller = {
    attach: function (context, settings) {

      alert("Hello! I am an alert box!!");
      window.alert("Hello world!");
      console.log('Loaded');

    }
  };

})(jQuery, Drupal, drupalSettings);
