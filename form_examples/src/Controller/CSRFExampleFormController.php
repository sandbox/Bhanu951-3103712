<?php

namespace Drupal\form_examples\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Returns responses for Form Examples routes.
 */
class CSRFExampleFormController extends ControllerBase {

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The CSRF token generator.
   *
   * @var \Drupal\Core\Access\CsrfTokenGenerator
   */
  protected $csrfToken;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);

    $instance->logger = $container->get('logger.factory')->get('form_examples');
    $instance->messenger = $container->get('messenger');
    $instance->csrfToken = $container->get('csrf_token');
    $instance->connection = $container->get('database');
    $instance->token = $container->get('token');
    $instance->configFactory = $container->get('config.factory');
    $instance->renderer = $container->get('renderer');

    return $instance;

  }

  /**
   * Builds the response.
   */
  public function build(Request $request) {

    $token = $request->query->has('token') ? $request->query->get('token') : '123sdfsdjkfsdkjfgsdk';

    $link_options = [
      'attributes' => [
        'target' => '_blank',
        'rel' => 'noopener noreferrer',
        'class' => ['button'],
        'query' => ['token' => $token],
      ],
    ];

    $url = Url::fromRoute('form_examples.csrf_example_form')->setOptions($link_options);

    $register_link = Link::fromTextAndUrl($this->t('Submit Another Form.'), $url)->toString();

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works received token @token !... @register_link', [
        '@token' => $token,
        '@register_link' => $register_link,
      ]),
    ];

    $config = $this->configFactory->get('system.site');
    $this->renderer->addCacheableDependency($build, $config);

    return $build;
  }

}
