<?php

namespace Drupal\form_examples\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CssCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Mail\MailManagerInterface;

/**
 * Class for Ajax Contact Form.
 */
class AjaxContactForm extends FormBase {

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Drupal\Component\Utility\EmailValidatorInterface definition.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $emailValidator;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The mail manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * Class constructor.
   */
  public function __construct(AccountProxyInterface $current_user,
  EmailValidatorInterface $email_validator,
  ConfigFactoryInterface $config_factory,
  MailManagerInterface $mail_manager) {
    $this->currentUser = $current_user;
    $this->emailValidator = $email_validator;
    $this->configFactory = $config_factory;
    $this->mailManager = $mail_manager;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('current_user'),
      $container->get('email.validator'),
      $container->get('config.factory'),
      $container->get('plugin.manager.mail')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'form_examples_ajax_contact_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['first_name'] = [
      '#prefix' => '<div class="row"><div class="col-12 col-md-6 mb-4">',
      '#type' => 'textfield',
      '#weight' => '0',
      '#required' => TRUE,
      '#attributes' => [
        'placeholder' => $this->t('First name *'),
        'class' => ['form-control'],
      ],
      '#suffix' => '</div>',
    ];
    $form['last_name'] = [
      '#prefix' => '<div class="col-12 col-md-6 mb-4">',
      '#type' => 'textfield',
      '#weight' => '10',
      '#required' => TRUE,
      '#attributes' => [
        'placeholder' => $this->t('Last name *'),
        'class' => ['form-control'],
      ],
      '#suffix' => '  </div></div>',
    ];
    $form['email'] = [
      '#prefix' => '<div class="row"><div class="col-12 col-md-12 mb-4">',
      '#type' => 'email',
      '#weight' => '20',
      '#required' => TRUE,
      '#attributes' => [
        'placeholder' => $this->t('Your email *'),
        'class' => ['form-control'],
      ],
      '#suffix' => '</div></div>',
    ];
    $form['request_type'] = [
      '#prefix' => '<div class="row"><div class="col-12 mb-4">',
      '#type' => 'select',
      '#required' => TRUE,
      '#options' => [
        '' => $this->t('How can we help you? *'),
        'login' => $this->t("I can't login"),
        'demo' => $this->t("I'd like a live demo"),
        'sales' => $this->t("I'd like to contact sales before I sign up"),
        'email_notifications' => $this->t("I don't receive email notifications"),
        'feature_request' => $this->t('I have a feature request'),
        'billing_request' => $this->t('I have a billing question'),
        'bug_report' => $this->t("I'd like to report a bug"),
      ],
      '#attributes' => ['class' => ['form-control']],
      '#weight' => '40',
      '#suffix' => '</div></div>',
    ];

    $form['message'] = [
      '#prefix' => '<div class="row"><div class="col-12 mb-4">',
      '#type' => 'textarea',
      '#required' => TRUE,
      '#weight' => '50',
      '#attributes' => [
        'placeholder' => $this->t('Please enter your request, comment or question *'),
        'class' => ['form-control'],
      ],
      '#suffix' => '</div></div>',
    ];

    $form['actions'] = [
      '#prefix' => '<div class="text-center">',
      '#type' => 'button',
      '#weight' => '70',
      '#attributes' => ['class' => ['btn btn-success']],
      '#value' => $this->t('Submit Support Request'),
      '#ajax' => [
        'callback' => '::validateEmailAjax',
      ],
      '#suffix' => '<span class="contact-form-result-message"></span></div>',
    ];

    $form['#attached']['library'] = [
      'core/jquery',
      'core/drupal.ajax',
      'core/jquery.once',
      'core/jquery.form',
    ];

    return $form;
  }

  /**
   * Ajax callback to validate / send.
   */
  public function validateEmailAjax(array &$form, FormStateInterface $form_state) {

    // Initiate response.
    $response = new AjaxResponse();

    // Get vars.
    $first_name = Html::escape($form_state->getValue('first_name'));
    $last_name = Html::escape($form_state->getValue('last_name'));
    $email = Html::escape($form_state->getValue('email'));
    $request_type = Html::escape($form_state->getValue('request_type'));
    $message = $form_state->getValue('message');

    // Validate and response.
    $valid = $this->validateEmail($email);

    // SUCCESS.
    if ($valid && !empty($first_name) && !empty($last_name) && !empty($request_type) && !empty($message)) {

      // Build mail body.
      $mail_body = $first_name;
      $mail_body .= " | ";
      $mail_body .= $last_name;
      $mail_body .= " | ";
      $mail_body .= $email;
      $mail_body .= " | ";
      $mail_body .= $request_type;
      $mail_body .= " | ";
      $mail_body .= $message;

      // Mail it.
      $module = 'ajax_form_example';
      $key = 'public_form_submission';
      $config = $this->configFactory->get('system.site');
      $to = $config->get('mail');
      $params['message'] = $mail_body;
      $params['title'] = $first_name . ' ' . $last_name;
      $langcode = $this->currentUser->getPreferredLangcode();
      $send = TRUE;

      $result = $this->mailManager->mail($module, $key, $to, $langcode, $params, NULL, $send);

      if ($result['result'] !== TRUE) {
        $message = $this->t('There was a problem sending your message, it was NOT sent, please contact us by email or phone.');
        $response->addCommand(new HtmlCommand('.contact-form-result-message', $message));
      }
      else {
        // Success, Ajax feedback.
        // Make email green, for if it was red before.
        $css = ['border' => '1px solid #ced4da'];
        $response->addCommand(new CssCommand('#edit-email', $css));
        // Make message green, for if it was red.
        $css2 = ['color' => 'green'];
        $response->addCommand(new CssCommand('.contact-form-result-message', $css2));
        // Empty all fields, flood control.
        $response->addCommand(new InvokeCommand('#edit-first-name', 'val', ['']));
        $response->addCommand(new InvokeCommand('#edit-last-name', 'val', ['']));
        $response->addCommand(new InvokeCommand('#edit-email', 'val', ['']));
        $response->addCommand(new InvokeCommand('#edit-message', 'val', ['']));
        $response->addCommand(new InvokeCommand('#edit-request_type', 'val', ['']));
        // Success message.
        $message = $this->t("Thanks! We'll contact you asap.");
        $response->addCommand(new HtmlCommand('.contact-form-result-message', $message));
      }
    }

    // FIELD EMPTY.
    elseif (empty($first_name) || empty($last_name) || empty($request_type) || empty($message)) {
      $message = $this->t('Please fill out all fields.');
      $css = ['color' => 'red'];
      $response->addCommand(new CssCommand('.contact-form-result-message', $css));
      $response->addCommand(new HtmlCommand('.contact-form-result-message', $message));
    }

    // EMAIL INVALID.
    elseif (!$valid) {
      $css = ['border' => '2px solid red'];
      $css2 = ['color' => 'red'];
      $message = $this->t('Please enter a valid email address.');
      $response->addCommand(new CssCommand('#edit-email', $css));
      $response->addCommand(new CssCommand('.contact-form-result-message', $css2));
      $response->addCommand(new HtmlCommand('.contact-form-result-message', $message));
    }
    $this->messenger()->deleteAll();
    return $response;
  }

  /**
   * Function to validate Email.
   *
   * @param string $email
   *   Email Id.
   *
   * @return bool
   *   Returns Valid or Not
   */
  protected function validateEmail($email) {

    // Validate the format of the email.
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Nothing needed here, but mandatory by Interface.
  }

}
