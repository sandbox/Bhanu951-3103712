<?php

namespace Drupal\form_examples\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Component\Utility\Random;

/**
 * Implements an Basic Example form.
 */
class BasicForm extends FormBase {

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;
  /**
   * Drupal\Component\Utility\EmailValidatorInterface definition.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $emailValidator;

  /**
   * Class constructor.
   */
  public function __construct(AccountProxyInterface $current_user,
  EmailValidatorInterface $email_validator) {
    $this->currentUser = $current_user;
    $this->emailValidator = $email_validator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('current_user'),
      $container->get('email.validator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'form_examples_basic_example_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $random = new Random();
    $markup = '<strong>' . $random->name() . $random->sentences(10) . $random->paragraphs(10) . '</strong>';

    $form['markup'] = [
      '#type' => 'markup',
      '#markup' => $markup,
    ];
    $form['description'] = [
      '#type' => 'item',
      '#markup' => $this->t('This example demonstrates the #states property. #states makes an element visibility dependent on another.'),
    ];
    $form['user_info'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User ID'),
      '#description' => $this->t('Displays the logged in user UID.'),
      '#size' => 5,
      '#maxlength' => 5,
      '#required' => TRUE,
      '#disabled' => TRUE,
      '#default_value' => $this->currentUser->id(),
      '#weight' => '1',
    ];
    $form['email'] = [
      '#type' => 'email',
      '#value' => $this->currentUser->getEmail(),
      '#title' => $this->t('Email'),
      '#description' => $this->t('User email'),
      '#weight' => '2',
    ];
    $form['title'] = [
      '#type' => 'textfield',
      '#size' => 25,
      '#title' => $this->t('Title'),
      '#required' => TRUE,
      '#weight' => '3',
    ];
    $form['phone_number'] = [
      '#type' => 'tel',
      '#title' => $this->t('Your phone number'),
      '#size' => 32,
      '#maxlength' => 32,
      '#weight' => '4',
    ];
    $form['weight'] = [
      '#type' => 'weight',
      '#title' => $this->t('Weight'),
      '#default_value' => 0,
      '#delta' => 10,
      '#description' => $this->t('Heavier items will sink and the lighter items will float to the top.'),
      '#weight' => '5',
    ];
    $addresses = [
      'hyderabad' => $this->t('Hyderabad'),
      'delhi' => $this->t('Delhi'),
      'chennai' => $this->t('Chennai'),
    ];
    $form['addresses'] = [
      '#type' => 'radios',
      '#title' => $this->t('Addresses'),
      '#options' => $addresses,
      '#weight' => '10',
    ];
    // An html checkbox for our drupal form
    // The options to display in our checkboxes.
    $toppings = [
      'pepperoni' => $this->t('Pepperoni'),
      'black_olives' => $this->t('Black olives'),
      'veggies' => $this->t('Veggies'),
    ];

    // The drupal checkboxes form field definition.
    $form['pizza'] = [
      '#title' => $this->t('Pizza Toppings'),
      '#type' => 'checkboxes',
      '#description' => $this->t('Select the pizza toppings you would like.'),
      '#options' => $toppings,
      '#weight' => '6',
    ];

    // An html hidden field for our drupal form.
    $form['wizard_page'] = [
      '#type' => 'hidden',
      '#value' => 2,
      '#weight' => '7',
    ];

    $form['video'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Youtube video'),
      '#weight' => '9',
    ];
    $form['develop'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('I would like to be involved in developing this material'),
      '#weight' => '14',
    ];
    // Details containers replace D7's collapsible field sets.
    $form['author'] = [
      '#type' => 'details',
      '#title' => 'Author Info (type = details)',
      '#weight' => '11',
    ];

    $form['author']['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
    ];

    $form['author']['pen_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Pen Name'),
    ];

    // Conventional field set.
    $form['book'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Book Info (type = fieldset)'),
      '#weight' => '12',
    ];

    $form['book']['book_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
    ];

    $form['book']['publisher'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Publisher'),
    ];

    // Containers have no visual display but wrap any contained elements in a
    // div tag.
    $form['accommodation'] = [
      '#type' => 'container',
      '#weight' => '13',
    ];

    $form['accommodation']['accommodation_title'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('Special Accommodations (type = container)'),
    ];

    $form['accommodation']['diet'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Dietary Restrictions'),
    ];
    $form['expiration'] = [
      '#type' => 'date',
      '#title' => $this->t('Content expiration'),
      '#default_value' => ['year' => 2020, 'month' => 2, 'day' => 15],
      '#default_value' => '2020-02-05',
      '#weight' => '14',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#button_type' => 'primary',
      '#weight' => '100',
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    // Get the email value from the field.
    $mail = $form_state->getValue('email');
    // Validate the format of the email.
    if (!$this->emailValidator->isValid($mail)) {
      $form_state->setErrorByName('email', $this->t('The %email is not valid email.',
        ['%email' => $mail]));
    }
    // Validate Phone Number.
    if (strlen($form_state->getValue('phone_number')) < 3) {
      $form_state->setErrorByName('phone_number', $this->t('The phone number is too short. Please enter a full phone number.'));
    }
    // Validate video URL.
    if (!UrlHelper::isValid($form_state->getValue('video'), TRUE)) {
      $form_state->setErrorByName('video', $this->t("The video url '%url' is invalid.", ['%url' => $form_state->getValue('video')]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->messenger()->addStatus($this->t('The form has been submitted with the following details.'));

    // Display result.
    foreach ($form_state->getValues() as $key => $value) {
      if (!is_array($value)) {
        $this->messenger()->addStatus($this->t('Submitted @key value is : %value',
        [
          '@key' => $key,
          '%value' => $form_state->getValue($key),
        ]));
      }
      else {
        foreach ($value as $a => $b) {
          if ($b !== 0) {
            $this->messenger()->addStatus($this->t('Submitted @key value is : %value',
            [
              '@key' => $key,
              '%value' => $a,
            ]));

          }

        }
      }

    }
    // Redirect to the Basic Form Page.
    $form_state->setRedirect('<current>');

  }

}
