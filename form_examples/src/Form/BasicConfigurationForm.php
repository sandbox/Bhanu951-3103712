<?php

namespace Drupal\form_examples\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\Entity\User;

/**
 * Defines a form that configures forms module settings.
 */
class BasicConfigurationForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const FORM_EXAMPLES_SETTINGS = 'form_examples.settings';

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Class constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory, AccountProxyInterface $current_user) {
    parent::__construct($config_factory);
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'form_examples_basic_config_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::FORM_EXAMPLES_SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('form_examples.settings');
    // Get current user id.
    $uid = $this->currentUser->id();
    // Load the current user.
    $user = User::load($uid);
    $email = $user->get('mail')->value;
    $name = $user->get('name')->value;
    // $name = $user->getUsername();
    $uid = $user->get('uid')->value;
    $uuid = $user->get('uuid')->value;
    $roles = $user->getRoles();
    $langcode = $user->get('langcode')->value;
    $preferred_langcode = $user->get('preferred_langcode')->value;

    // Submitted form values should be nested.
    $form['#tree'] = TRUE;

    $form['description'] = [
      '#type' => 'item',
      '#title' => $this->t('Configuration Form Example.'),
      '#description' => $this->t('This form shows the example for configuration fields.'),
    ];
    $form['site_details'] = [
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#title' => $this->t('Site Details'),
    ];
    $form['site_details']['site_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site Details'),
      '#required' => TRUE,
      '#disabled' => TRUE,
      '#size' => 20,
      '#default_value' => !empty($config->get('site_settings.site_name')) ? $config->get('site_settings.site_name') : 'NA',
    ];

    $form['user_details'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('User Details'),
    ];
    $form['user_details']['markup_element'] = [
      '#type' => 'markup',
      '#markup' => '<strong>Provides User Details</strong>.',
    ];
    $form['user_details']['uid'] = [
      '#type' => 'textfield',
      '#title' => $this->t("User's UID"),
      '#description' => $this->t('Displays the logged user UID.'),
      '#size' => 5,
      '#required' => TRUE,
      '#disabled' => TRUE,
      '#default_value' => !empty($config->get('user_details.uid')) ? $config->get('user_details.uid') : $uid,
    ];
    $form['user_details']['email'] = [
      '#type' => 'textfield',
      '#title' => $this->t("User's Email"),
      '#description' => $this->t('Displays the logged user Email.'),
      '#size' => 30,
      '#required' => TRUE,
      '#disabled' => TRUE,
      '#default_value' => !empty($config->get('user_details.email')) ? $config->get('user_details.email') : $email,
    ];
    $form['user_details']['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t("User's Name"),
      '#description' => $this->t('Displays the logged user Name.'),
      '#size' => 30,
      '#required' => TRUE,
      '#disabled' => TRUE,
      '#default_value' => !empty($config->get('user_details.name')) ? $config->get('user_details.name') : $name,
    ];
    $form['user_details']['uuid'] = [
      '#type' => 'textfield',
      '#title' => $this->t("User's UUID"),
      '#description' => $this->t('Displays the logged user UUID.'),
      '#size' => 35,
      '#required' => TRUE,
      '#disabled' => TRUE,
      '#default_value' => !empty($config->get('user_details.uuid')) ? $config->get('user_details.uuid') : $uuid,
    ];

    $form['user_details']['user_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User message'),
      '#default_value' => $config->get('user_details.user_message'),
      '#description' => $this->t('Text format, #type = textfield'),
    ];
    // Textarea.
    $form['user_details']['text_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Text'),
      '#description' => $this->t('Textarea, #type = textarea'),
      '#default_value' => $config->get('user_details.text_message'),
    ];

    // Text format.
    $form['user_details']['user_description'] = [
      '#type' => 'text_format',
      '#title' => 'Text format',
      '#format' => 'basic_html',
      '#default_value' => $config->get('user_details.user_description'),
      '#description' => $this->t('Text format, #type = text_format'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $config = $this->configFactory->getEditable(static::FORM_EXAMPLES_SETTINGS);
    // Set the submitted configuration setting.
    $config->set('user_details.user_message',
     $form_state->getValue(['user_details', 'user_message']
    ));

    $config->set('user_details.text_message',
     $form_state->getValue(['user_details', 'text_message']
    ));

    $config->set('user_details.user_description',
      $form_state->getValue(['user_details', 'user_description', 'value'])
    );

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
