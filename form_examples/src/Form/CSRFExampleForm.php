<?php

namespace Drupal\form_examples\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;
use Drupal\Core\Routing\TrustedRedirectResponse;

/**
 * Provides a Form Examples form.
 */
class CSRFExampleForm extends FormBase {

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The CSRF token generator.
   *
   * @var \Drupal\Core\Access\CsrfTokenGenerator
   */
  protected $csrfToken;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {

    // Instantiates this form class.
    $instance = parent::create($container);

    $instance->account = $container->get('current_user');
    $instance->messenger = $container->get('messenger');
    $instance->logger = $container->get('logger.factory')->get('form_examples');
    $instance->request = $container->get('request_stack')->getCurrentRequest();
    $instance->csrfToken = $container->get('csrf_token');

    return $instance;

  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'form_examples_csrf_example';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['markup'] = [
      '#markup' => $this->t('<h1>Enter your message!...</h1><br>'),
    ];

    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message'),
      '#required' => TRUE,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (mb_strlen($form_state->getValue('message')) < 10) {
      $form_state->setErrorByName('name', $this->t('Message should be at least 10 characters.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->messenger->addStatus($this->t('The message has been sent.'));

    if ($this->account->hasPermission('administer form examples')) {

      $url = Url::fromRoute('form_examples.csrf_example_form_controller');
      $token = $this->csrfToken->get($url->getInternalPath());

      $url->setOptions(['absolute' => TRUE, 'query' => ['token' => $token]]);
      $absolute_url = $url->toString();

      $form_state->setResponse(new TrustedRedirectResponse($absolute_url, 302));

    }
    else {
      $form_state->setRedirect('<front>');

    }
  }

}
