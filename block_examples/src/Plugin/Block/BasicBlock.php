<?php

namespace Drupal\block_examples\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a 'Hello World' Block.
 *
 * @Block(
 *   id = "hello_world_block",
 *   admin_label = @Translation("Hello World Block"),
 *   category = @Translation("Block Examples"),
 * )
 */
class BasicBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build['output'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Hello World!'),
    ];
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'administer block examples');
  }

}
