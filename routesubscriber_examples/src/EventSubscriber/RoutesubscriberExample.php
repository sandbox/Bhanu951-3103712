<?php

namespace Drupal\routesubscriber_examples\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\RouteCollection;

/**
 * Route Subscriber Examples route subscriber.
 */
class RoutesubscriberExample extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {

    foreach ($collection->all() as $route) {

      // Change path '/routesubscriber-examples/example'
      // to '/routesubscriber-examples/hello'.
      if ($route = $collection->get('routesubscriber_examples.example')) {
        $route->setPath('/routesubscriber-examples/hello');
        $route->setRequirement('_permission', 'administer routesubscriber examples configurations');
        $route->setDefaults([
          '_title' => 'Hello Modified Controller.',
          '_controller' => '\Drupal\routesubscriber_examples\Controller\RoutesubscriberExamplesController::buildUpdated',
        ]);
      }

      // Re-add the collection to override the existing route.
      $collection->add('routesubscriber_examples.example', $route);

    }

  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {

    $events = parent::getSubscribedEvents();

    // Use a lower priority than \Drupal\views\EventSubscriber\RouteSubscriber
    // to ensure the requirement will be added to its routes.
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -300];

    return $events;
  }

}
