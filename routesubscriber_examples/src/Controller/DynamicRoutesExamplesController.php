<?php

namespace Drupal\routesubscriber_examples\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Route Subscriber Examples Module Routes.
 */
class DynamicRoutesExamplesController extends ControllerBase {

  /**
   * Builds response for dynamic routes.
   */
  public function productType() {

    $build['results'] = [
      '#markup' => $this->t('Controller to get Products List.'),
    ];

    return $build;
  }

}
