<?php

namespace Drupal\routesubscriber_examples\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for RouteSubscriber Examples routes.
 */
class RoutesubscriberExamplesController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];

    return $build;
  }

  /**
   * Builds updated response.
   */
  public function buildUpdated() {

    $build['content'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Updated Route works!'),
    ];

    return $build;
  }

}
