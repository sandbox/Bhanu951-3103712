<?php

namespace Drupal\filter_examples\Plugin\Filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Provides a 'CustomTextFilter' filter.
 *
 * @Filter(
 *   id = "filter_examples_customtextfilter",
 *   title = @Translation("CustomTextFilter"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_HTML_RESTRICTOR,
 *   settings = {
 *     "customtextfilter" = "CustomTextForFilter",
 *   },
 *   weight = -10
 * )
 */
class CustomTextFilter extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['customtextfilter'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom Text Filter Settings'),
      '#default_value' => $this->settings['customtextfilter'],
      '#description' => $this->t('Enter Cusom Filter Settings.'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    // @DCG Process text here.
    $custom_filter = $this->settings['customtextfilter'];
    $text = str_replace($custom_filter, "<b>$custom_filter</b>", $text);
    return new FilterProcessResult($text);
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    return $this->t('Cusom Filter to replace configured custom text.');
  }

}
