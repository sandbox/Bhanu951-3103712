<?php

namespace Drupal\breadcrumb_examples\Breadcrumb;

use Drupal\Core\Access\AccessManagerInterface;
use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Link;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * {@inheritdoc}
 */
class BreadcrumbsBuilderExample implements BreadcrumbBuilderInterface {

  use StringTranslationTrait;

  /**
   * The access manager.
   *
   * @var \Drupal\Core\Access\AccessManagerInterface
   */

  protected $accessManager;

  /**
   * The user currently logged in.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */

  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public function __construct(AccountInterface $current_user, AccessManagerInterface $access_manager) {
    $this->currentUser = $current_user;
    $this->accessManager = $access_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {

    $parameters = $route_match->getParameters()->all();
    if (isset($parameters['node'])) {
      $nodeType = $parameters['node']->getType();

      if ($nodeType != 'card') {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {

    $currentTitle = [];

    $parameters = $route_match->getParameters()->all();

    $nodeType = $parameters['node']->getType();

    if (!empty($parameters['node'])) {
      $currentTitle = $parameters['node']->getTitle();
    }

    $breadcrumb = new Breadcrumb();

    $access = $this->accessManager->check($route_match, $this->currentUser, NULL, TRUE);

    $breadcrumb->addCacheableDependency($access);
    $breadcrumb->addCacheContexts(['url.path']);
    $breadcrumb->addLink(Link::createFromRoute($this->t('Home Page'), '<front>'));

    if ($this->currentUser->hasPermission('access content')) {
      $breadcrumb->addLink(Link::createFromRoute($this->t('Front Page'), 'view.frontpage.page_1'));
    }

    if ($this->currentUser->isAuthenticated()) {
      $breadcrumb->addLink(Link::createFromRoute($this->t('Content List'), 'view.content.page_1'));
    }

    switch ($nodeType) {
      // If node type is "page".
      case 'page':
        $breadcrumb->addLink(Link::createFromRoute('Page', 'view.frontpage.page_1'));

        break;

      // If node type is "article".
      case 'article':
        $breadcrumb->addLink(Link::createFromRoute('Article', 'view.frontpage.page_1'));

        break;
    }

    $breadcrumb->addLink(Link::createFromRoute($this->t('@currentTitle', ['@currentTitle' => $currentTitle]), '<none>'));
    return $breadcrumb;

  }

}
