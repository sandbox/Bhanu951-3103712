<?php

namespace Drupal\twig_extension_examples\Twig;

/**
 * CustomTwigFilter extension.
 */
class CustomTwigFilter extends \Twig_Extension {

  /**
   * This is the same name we used on the services.yml file.
   */
  public function getName() {
    return 'twig_extension_examples.twig_filter';
  }

  /**
   * Generates a list of all Twig filters that this extension defines.
   */
  public function getFilters() {

    $filters = [
      new \Twig_SimpleFilter('removenum', [$this, 'removeNumbers']),
      new \Twig_SimpleFilter('colorize', [$this, 'filterColorize'], ['is_safe' => ['html']]),
    ];
    return $filters;
  }

  /**
   * Filter to return colorized text.
   */
  public static function filterColorize($string, $color) {
    return '<span style="color: ' . $color . '">' . $string . '</span>';
  }

  /**
   * Replaces all numbers from the string.
   */
  public static function removeNumbers($string) {
    return preg_replace('#[0-9]*#', '', $string);
  }

}
