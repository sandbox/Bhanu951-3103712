<?php

namespace Drupal\twig_extension_examples\Twig;

/**
 * CustomTwigFunction extension.
 */
class CustomTwigFunction extends \Twig_Extension {

  /**
   * This is the same name we used on the services.yml file.
   */
  public function getName() {
    return 'twig_extension_examples.twig_function';
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new \Twig_SimpleFunction('word_count', [$this, 'wordCountFunction']),
    ];
  }

  /**
   * The actual implementation of the twig function.
   */
  public function wordCountFunction($content) {
    if (is_string($content)) {
      $content = str_word_count($content);
    }
    return $content;
  }

}
