<?php

namespace Drupal\queue_examples\Form;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\ClientInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Queue\QueueWorkerManager;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Component\Datetime\Time;

/**
 * Configure queue_examples settings for this site.
 */
class LocationsBatchImportForm extends FormBase {

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Batch Builder.
   *
   * @var \Drupal\Core\Batch\BatchBuilder
   */
  protected $batchBuilder;

  /**
   * Drupal\Component\Datetime\Time instance.
   *
   * @var \Drupal\Component\Datetime\Time
   */
  public $time;

  /**
   * Drupal\Core\Queue\QueueFactory instance.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * LocationsBatchImportForm constructor.
   */
  public function __construct(ClientInterface $http_client, QueueFactory $queue_factory, QueueWorkerManager $queue_manager, ModuleHandlerInterface $module_handler, Time $time) {

    $this->httpClient = $http_client;
    $this->batchBuilder = new BatchBuilder();
    $this->queue_factory = $queue_factory;
    $this->queue_manager = $queue_manager;
    $this->moduleHandler = $module_handler;
    $this->time = $time;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client'),
      $container->get('queue'),
      $container->get('plugin.manager.queue_worker'),
      $container->get('module_handler'),
      $container->get('datetime.time')

    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'queue_examples_batch_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $host = $this->getRequest()->getSchemeAndHttpHost();

    $moduleExist = $this->moduleHandler->moduleExists('example_codes');

    if ($moduleExist) {

      $modulePath = $this->moduleHandler->getModule('example_codes')->getPath();
      $file = $modulePath . '/data/cities.json';
      $url = $host . '/' . $file;
    }

    $form['help'] = [
      '#markup' => $this->t('This form is used to get the location data from remote URL and push data to queue in Batches.'),
    ];

    $sourceUrl = 'https://raw.githubusercontent.com/dr5hn/countries-states-cities-database/master/cities.json';

    $link = Link::fromTextAndUrl('here', Url::fromUri($sourceUrl, ['attributes' => ['target' => '_blank']]));

    $form['remote'] = [
      '#type' => 'url',
      '#title' => $this->t('Locations Remote URL'),
      '#maxlength' => 2048,
      '#default_value' => !empty($url) ? $url : $sourceUrl,
      '#description' => $this->t('Enter the URL of an Locations file. Sample file can be downloaded from @link and data is processed only once on submission of the form.', ['@link' => $link->toString()]),
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['run'] = [
      '#type' => 'submit',
      '#value' => $this->t('Run Batch'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Check whether the URL is valid or not.
    $url = $form_state->getValue('remote');
    if (!UrlHelper::isValid($url, TRUE)) {
      $this->messenger()->addError($this->t('The URL %url is invalid.', ['%url' => $url]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $data = [];

    $link = Link::fromTextAndUrl('here', Url::fromRoute('queue_examples.locations_batch_import_form'));

    $remoteUrl = $form_state->getValue('remote');

    if (!empty($remoteUrl)) {

      $data = $this->fetchData($remoteUrl);

      if (!empty($data)) {
        $decodedData = Json::decode($data);
        $decodedLocationData = $decodedData['cities'] ?? $decodedData;

        $this->batchBuilder
          ->setTitle($this->t('Processing Batch...'))
          ->setInitMessage($this->t('Initializing Batch...'))
          ->setProgressMessage($this->t('Completed @current of @total. Estimated remaining time: @estimate.'))
          ->setErrorMessage($this->t('An error has occurred.Click @link to return to form', ['@link' => $link->toString()]));

        // @todo Move Batch Builder Operation and Finished callback Functions into seperate Batch File.
        // Name Spaces as namespace Drupal\queue_examples\Batch;
        // Chec whether $this->batchBuilder->setFile() is needed or not.
        // Check replace for drupal_get_path() and its DI.
        $this->batchBuilder->addOperation([$this, 'processItems'], [$decodedLocationData]);
        $this->batchBuilder->setFinishCallback([$this, 'finished']);

        batch_set($this->batchBuilder->toArray());

      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function fetchData(string $url) {
    $data = '';
    try {

      // @todo check whether implementation of 'timeout' => 600
      // is correct or not in httpClient.
      $response = $this->httpClient->get($url, ['headers' => ['Content-Type' => 'application/hal+json']]);
      if ($response->getStatusCode() == 200) {
        $data = (string) $response->getBody();
      }

    }
    catch (RequestException $exception) {

      $this->messenger()->addError('Failed to download JSON data from URL due to "%error".', ['%error' => $exception->getMessage()]);

      $this->logger('queue_examples')->warning('Failed to download JSON data from URL due to "%error".', ['%error' => $exception->getMessage()]);
    }
    return $data;
  }

  /**
   * Processor for batch operations.
   */
  public function processItems($items, array &$context) {
    // Elements per operation.
    $limit = 50;

    // Set default progress values.
    if (empty($context['sandbox']['progress'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($items);
    }

    // Save items to array which will be changed during processing.
    if (empty($context['sandbox']['items'])) {
      $context['sandbox']['items'] = $items;
    }

    $counter = 0;
    if (!empty($context['sandbox']['items'])) {
      // Remove already processed items.
      if ($context['sandbox']['progress'] != 0) {
        array_splice($context['sandbox']['items'], 0, $limit);
      }

      foreach ($context['sandbox']['items'] as $item) {
        if ($counter != $limit) {
          $this->processItem($item);

          $counter++;
          $context['sandbox']['progress']++;

          $context['message'] = $this->t('Now Processing Data Item :progress of :count', [
            ':progress' => $context['sandbox']['progress'],
            ':count' => $context['sandbox']['max'],
          ]);

          // Increment total processed item values. Will be used in finished
          // callback.
          $context['results']['processed'] = $context['sandbox']['progress'];
        }
      }
    }

    // If not finished
    // If not finished all tasks, we count percentage of process. 1 = 100%.
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  /**
   * Process single item.
   *
   * @param array $item
   *   Single Location Data.
   */
  public function processItem(array $item) {

    if (!empty($item)) {

      foreach ($item as $key => $value) {

        $data = [
          'city_id' => $item['id'],
          'city_name' => $item['name'],
          'state_id' => $item['state_id'],
          'created' => date("d/m/y, H:i:s", $this->time->getRequestTime()),
        ];

      }

      // Create new queue item.
      $queue = $this->queue_factory->get('locations_batch_queue_processor');
      $queue->createItem($data);
      // $queue->deleteItem($data);
      $this->logger('queue_examples')->notice($this->t('Data Item Pushed to Queue: @data', ['@data' => implode(',', $data)]));
    }

  }

  /**
   * Finished callback for batch.
   */
  public function finished($success, $results, $operations) {
    $message = $this->t('Number of Items processed by batch: @count', [
      '@count' => $results['processed'],
    ]);

    $this->messenger()->addStatus($message);
    $this->logger('queue_examples')->info($message);
  }

}
