<?php

namespace Drupal\queue_examples\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Component\Datetime\Time;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Class LocationsImportForm.
 *
 * @package Drupal\queue_examples\Form
 */
class LocationsImportForm extends FormBase {


  /**
   * Queue Factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * Queue Manager.
   *
   * @var \Drupal\Core\Queue\QueueWorkerManagerInterface
   */
  protected $queueManager;

  /**
   * Email Validator.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $emailValidator;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Drupal\Component\Datetime\Time instance.
   *
   * @var \Drupal\Component\Datetime\Time
   */
  public $time;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public function __construct(EmailValidatorInterface $email_validator, QueueFactory $queue_factory, QueueWorkerManagerInterface $queue_manager, ModuleHandlerInterface $module_handler, AccountProxyInterface $current_user, Time $time, LoggerChannelFactoryInterface $logger, MessengerInterface $messenger) {
    $this->emailValidator = $email_validator;
    $this->queueFactory = $queue_factory;
    $this->queueManager = $queue_manager;
    $this->moduleHandler = $module_handler;
    $this->currentUser = $current_user;
    $this->time = $time;
    $this->logger = $logger->get('queue_examples');
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('email.validator'),
      $container->get('queue'),
      $container->get('plugin.manager.queue_worker'),
      $container->get('module_handler'),
      $container->get('current_user'),
      $container->get('datetime.time'),
      $container->get('logger.factory'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'locations_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $header = $decoded = [];

    $header = ['id', 'shortname', 'name'];

    $moduleExist = $this->moduleHandler->moduleExists('example_codes');

    if ($moduleExist) {

      $modulePath = $this->moduleHandler->getModule('example_codes')->getPath();
      $file = $modulePath . '/data/countries.json';
      $locationData = file_get_contents($file);
      $decoded = Json::decode($locationData);

    }

    $form['email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email'),
      '#required' => TRUE,
      '#size' => 25,
      '#value' => $this->currentUser->getEmail(),
      '#attributes' => ['readonly' => 'readonly'],
    ];
    $form['data'] = [
      '#type' => 'hidden',
      '#default_value' => [],
      '#value' => $decoded,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#button_type' => 'primary',
    ];
    $form['info'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Click on Submit to create Queue'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Validate email.
    if (!$this->emailValidator->isValid($form_state->getValues()['email'])) {
      $form_state->setErrorByName('Email', $this->t('Email address is not a valid one.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $locationData = $form_state->getValues()['data']['countries'];

    if (!empty($locationData)) {

      foreach ($locationData as $value) {

        $data = [
          'country_id' => $value['id'],
          'country_shortname' => $value['shortname'],
          'country_name' => $value['name'],
          'created' => date("d/m/y, H:i:s", $this->time->getRequestTime()),
        ];

        // Create new queue item.
        $queue = $this->queueFactory->get('locations_queue_processor');
        $queue->createItem($data);
        // $queue->deleteItem($data);
      }

      $this->logger->notice($this->t('Pushed %count items into Queue', ['%count' => count($locationData)]));

      $this->messenger->addMessage($this->t('Pushed %count items into Queue', ['%count' => count($locationData)]));

    }
  }

}
