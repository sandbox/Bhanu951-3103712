<?php

namespace Drupal\queue_examples\Form;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\ClientInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Queue\QueueWorkerManager;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Component\Datetime\Time;
use Drupal\Component\Utility\Unicode;

/**
 * Configure queue_examples settings for this site.
 */
class QueueDataImportForm extends FormBase {

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Batch Builder.
   *
   * @var \Drupal\Core\Batch\BatchBuilder
   */
  protected $batchBuilder;

  /**
   * Drupal\Component\Datetime\Time instance.
   *
   * @var \Drupal\Component\Datetime\Time
   */
  public $time;

  /**
   * Drupal\Core\Queue\QueueFactory instance.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * LocationsBatchImportForm constructor.
   */
  public function __construct(ClientInterface $http_client, QueueFactory $queue_factory, QueueWorkerManager $queue_manager, ModuleHandlerInterface $module_handler, Time $time) {

    $this->httpClient = $http_client;
    $this->batchBuilder = new BatchBuilder();
    $this->queue_factory = $queue_factory;
    $this->queue_manager = $queue_manager;
    $this->moduleHandler = $module_handler;
    $this->time = $time;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client'),
      $container->get('queue'),
      $container->get('plugin.manager.queue_worker'),
      $container->get('module_handler'),
      $container->get('datetime.time')

    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'queue_examples_batch_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['help'] = [
      '#markup' => $this->t('This form is used to get the location data from remote URL and push data to queue in Batches.'),
    ];

    $form['explanation_html_tag'] = [
      '#type' => 'html_tag',
      '#tag' => 'b',
      '#value' => $this->t('A better way to add extra content is to use the html_tag render element.'),
    ];

    $form['explanation'] = [
      '#type' => 'inline_template',
      '#template' => '<p>{{ explanation }}</p>',
      '#context' => [
        'explanation' => $this->t('When a bit more complicated output is requested, the <b> inline_template </b> can be a welcome render element.'),
      ],
    ];

    $sourceUrl = 'https://raw.githubusercontent.com/dr5hn/countries-states-cities-database/master/cities.json';

    $link = Link::fromTextAndUrl('here', Url::fromUri($sourceUrl, ['attributes' => ['target' => '_blank']]));

    $locations = [
      'countries' => $this->t('Countries'),
      'states' => $this->t('States'),
      'cities' => $this->t('Cities'),
      'cities_one' => $this->t('Cities One'),
    ];

    $form['locations_list'] = [
      '#title' => $this->t('Select Location Type.'),
      '#type' => 'select',
      '#options' => $locations,
      '#empty_option' => $this->t('Choose a Location'),
      '#required' => TRUE,
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['run'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import Locations'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Check whether the Location Type is valid or not.
    $location = $form_state->getValue('locations_list');
    if (!in_array($location, ['countries', 'states', 'cities'], TRUE)) {
      $form_state->setErrorByName('locations_list', $this->t('The selected location type %location is invalid.', ['%location' => $location]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $data = [];

    $link = Link::fromTextAndUrl('here', Url::fromRoute('queue_examples.queue_data_import_form'));

    $location = $form_state->getValue('locations_list');

    if (!empty($location)) {

      $host = $this->getRequest()->getSchemeAndHttpHost();

      $moduleExist = $this->moduleHandler->moduleExists('example_codes');

      if ($moduleExist) {

        $modulePath = $this->moduleHandler->getModule('example_codes')->getPath();
        $file = $modulePath . '/data/' . $location . '.json';
        $url = $host . '/' . $file;
      }
      else {

        $url = 'https://raw.githubusercontent.com/dr5hn/countries-states-cities-database/master/' . $location . '.json';

      }

      $data = $this->fetchData($url);

      if (!empty($data)) {

        $decodedData = Json::decode($data);

        $decodedLocationData = $decodedData[$location] ?? $decodedData;

        $this->batchBuilder
          ->setTitle($this->t('Processing @location Batch...', ['@location' => Unicode::ucfirst($location)]))
          ->setInitMessage($this->t('Initializing Batch...'))
          ->setProgressMessage($this->t('Completed @current of @total. Estimated remaining time: @estimate. Elapsed time : @elapsed.'))
          ->setErrorMessage($this->t('An error has occurred.Click @link to return to form', ['@link' => $link->toString()]));
        $this->batchBuilder->addOperation(
          [$this, 'processItems'],
          [$location, $decodedLocationData]
        );
        $this->batchBuilder->setFinishCallback([$this, 'finished']);
        batch_set($this->batchBuilder->toArray());
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function fetchData(string $url) {
    $data = [];
    try {

      // @todo check whether implementation of 'timeout' => 600
      // is correct or not in httpClient.
      $response = $this->httpClient->get($url, ['headers' => ['Content-Type' => 'application/hal+json']]);
      if ($response->getStatusCode() == 200) {
        $data = (string) $response->getBody();
      }

    }
    catch (RequestException $exception) {

      $this->messenger()->addError('Failed to download JSON data from URL due to an error check logs for more details.');

      $this->logger('queue_examples')->warning('Failed to download JSON data from URL due to "%error".', ['%error' => $exception->getMessage()]);
    }
    return $data;
  }

  /**
   * Processor for batch operations.
   */
  public function processItems($location, $items, array &$context) {
    // Elements per operation.
    $limit = 50;

    // Set default progress values.
    if (empty($context['sandbox']['progress'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($items);
    }

    // Save items to array which will be changed during processing.
    if (empty($context['sandbox']['items'])) {
      $context['sandbox']['items'] = $items;
    }

    $counter = 0;
    if (!empty($context['sandbox']['items'])) {
      // Remove already processed items.
      if ($context['sandbox']['progress'] != 0) {
        array_splice($context['sandbox']['items'], 0, $limit);
      }

      foreach ($context['sandbox']['items'] as $item) {
        if ($counter != $limit) {
          $this->processItem($location, $item);
          $counter++;
          $context['sandbox']['progress']++;

          $context['message'] = $this->t('Now Processing Data Item :progress of :count', [
            ':progress' => $context['sandbox']['progress'],
            ':count' => $context['sandbox']['max'],
          ]);

          // Increment total processed item values. Will be used in finished
          // callback.
          $context['results']['processed'] = $context['sandbox']['progress'];
        }
      }
    }

    // If not finished
    // If not finished all tasks, we count percentage of process. 1 = 100%.
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  /**
   * Process single item.
   *
   * @param string $location
   *   Location Type.
   * @param array $item
   *   Single Location Data.
   */
  public function processItem(string $location, array $item) {

    if (!empty($item)) {

      // Create new queue item.
      $queue = $this->queue_factory->get('queue_data_processor:' . $location);
      $queue->createItem($item);
      // $queue->deleteItem($data);
      $this->logger('queue_examples')
        ->notice($this->t('@location Data Item Pushed to Queue: @data',
          [
            '@data' => implode(',', $item),
            '@location' => Unicode::ucfirst($location),
          ])
        );
    }

  }

  /**
   * Finished callback for batch.
   */
  public function finished($success, $results, $operations) {
    $message = $this->t('Number of Items processed by batch: @count', [
      '@count' => $results['processed'],
    ]);

    $this->messenger()->addStatus($message);
    $this->logger('queue_examples')->info($message);
  }

}
