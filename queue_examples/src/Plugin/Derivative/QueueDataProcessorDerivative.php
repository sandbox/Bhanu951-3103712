<?php

namespace Drupal\queue_examples\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;

/**
 * The simple derivative example for Queue.
 */
class QueueDataProcessorDerivative extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {

    $locations = ['countries', 'states', 'cities'];

    foreach ($locations as $location) {

      $this->derivatives[$location] = $base_plugin_definition;
      $this->derivatives[$location]['title'] = $location . 'Queue';

    }

    return $this->derivatives;
  }

}
