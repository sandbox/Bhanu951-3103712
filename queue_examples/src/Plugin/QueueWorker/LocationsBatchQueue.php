<?php

namespace Drupal\queue_examples\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Processes Locations Data.
 *
 * @QueueWorker(
 *   id = "locations_batch_queue_processor",
 *   title = @Translation("Locations Batch Queue Processor."),
 *   cron = {"time" = 10}
 * )
 */
class LocationsBatchQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * Database service.
   *
   * @var \Drupal\Core\Database\Connection
   */

  protected $database;

  /**
   * {@inheritdoc}
   */
  public function __construct(LoggerChannelFactoryInterface $logger, MessengerInterface $messenger, Connection $connection) {
    $this->logger = $logger->get('queue_examples');
    $this->messenger = $messenger;
    $this->database = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {

    if (!empty($data)) {

      $query = $this->database
        ->select('queue_examples_cities_data', 'qecd')
        ->fields('qecd', ['city_id']);
      $query->condition('qecd.city_name', $data['city_name']);

      $results = $query->execute()->fetchObject();

      if (!isset($results->city_id)) {

        $query = $this->database->insert('queue_examples_cities_data');
        $query->fields($data);
        $query->execute();

        $this->logger->notice($this->t('City Data Inserted : @data', ['@data' => implode(',', $data)]));

      }
      else {

        $query = $this->database->update('queue_examples_cities_data');
        $query->fields($data);
        $query->condition('city_id', $data['city_id']);
        $query->execute();

        $this->logger->notice($this->t('City Data Updated : @data', ['@data' => implode(',', $data)]));

      }

    }
    else {
      $this->logger->warning($this->t('No Data available to Process.'));

    }

  }

}
