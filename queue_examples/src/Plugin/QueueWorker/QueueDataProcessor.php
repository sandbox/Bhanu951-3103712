<?php

namespace Drupal\queue_examples\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Component\Utility\Unicode;

/**
 * Processes Locations Data.
 *
 * @QueueWorker(
 *   id = "queue_data_processor",
 *   title = @Translation("Queue Data Processor."),
 *   cron = {"time" = 10},
 *   deriver = "Drupal\queue_examples\Plugin\Derivative\QueueDataProcessorDerivative",
 * )
 */
class QueueDataProcessor extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Drupal\Component\Datetime\Time instance.
   *
   * @var \Drupal\Component\Datetime\Time
   */
  public $time;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->logger = $container->get('logger.factory')->get('queue_examples');
    $instance->messenger = $container->get('messenger');
    $instance->database = $container->get('database');
    $instance->time = $container->get('datetime.time');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {

    $plugin_id = $this->getPluginId();
    $id = $this->getDerivativeId();

    // @todo if id = countries insert data into countries table.
    // if id = states insert data into states table.
    // if id = cities insert data into cities table.
    // $this->logger->info($id . ' Queue Processed. ' . $plugin_id);
    if (!empty($data)) {

      switch ($id) {
        case 'countries':

          $query = $this->database
            ->select('queue_examples_countries_data', 'qecd')
            ->fields('qecd', ['country_id']);
          $query->condition('qecd.country_name', $data['name']);

          $results = $query->execute()->fetchObject();

          if (!isset($results->country_id)) {

            $query = $this->database->insert('queue_examples_countries_data');
            $query->fields([
              'country_id' => $data['id'],
              'country_shortname' => $data['shortname'],
              'country_name' => $data['name'],
              'created' => date("d/m/y, H:i:s", $this->time->getRequestTime()),
            ]);
            // $query->fields($data);
            $query->execute();

            $this->logger->notice($this->t('@id Data Inserted : @data',
              [
                '@id' => Unicode::ucfirst($id),
                '@data' => implode(',', $data),
              ]
            ));

          }
          else {

            $query = $this->database->update('queue_examples_countries_data');
            $query->fields(['created' => date("d/m/y, H:i:s", $this->time->getRequestTime())]);
            $query->condition('country_id', $data['id']);
            $query->execute();

            $this->logger->notice($this->t('@id Data Updated : @data',
              [
                '@id' => Unicode::ucfirst($id),
                '@data' => implode(',', $data),
              ]
            ));

          }

          break;

        case 'states':

          $query = $this->database
            ->select('queue_examples_states_data', 'qesd')
            ->fields('qesd', ['state_id']);
          $query->condition('qesd.state_name', $data['name']);
          $results = $query->execute()->fetchObject();

          if (!isset($results->state_id)) {

            $query = $this->database->insert('queue_examples_states_data');
            $query->fields([
              'state_id' => $data['id'],
              'state_name' => $data['name'],
              'country_id' => $data['country_id'],
              'created' => date("d/m/y, H:i:s", $this->time->getRequestTime()),
            ]);
            $query->execute();

            $this->logger->notice($this->t('@id Data Inserted : @data',
              [
                '@id' => Unicode::ucfirst($id),
                '@data' => implode(',', $data),
              ]
            ));

          }
          else {

            $query = $this->database->update('queue_examples_states_data');
            $query->fields(['created' => date("d/m/y, H:i:s", $this->time->getRequestTime())]);
            $query->condition('state_id', $data['id']);
            $query->execute();

            $this->logger->notice($this->t('@id Data Updated : @data',
            [
              '@id' => Unicode::ucfirst($id),
              '@data' => implode(',', $data),
            ]
            ));

          }

          break;

        case 'cities':

          $query = $this->database
            ->select('queue_examples_cities_data', 'qecd')
            ->fields('qecd', ['city_id']);
          $query->condition('qecd.city_name', $data['name']);

          $results = $query->execute()->fetchObject();

          if (!isset($results->city_id)) {

            $query = $this->database->insert('queue_examples_cities_data');
            $query->fields([
              'city_id' => $data['id'],
              'city_name' => $data['name'],
              'state_id' => $data['state_id'],
              'created' => date("d/m/y, H:i:s", $this->time->getRequestTime()),
            ]);
            $query->execute();

            $this->logger->notice($this->t('@id Data Inserted : @data',
              [
                '@id' => Unicode::ucfirst($id),
                '@data' => implode(',', $data),
              ]
            ));

          }
          else {

            $query = $this->database->update('queue_examples_cities_data');
            $query->fields(['created' => date("d/m/y, H:i:s", $this->time->getRequestTime())]);
            $query->condition('state_id', $data['state_id']);
            $query->execute();

            $this->logger->notice($this->t('@id Data Updated : @data',
            [
              '@id' => Unicode::ucfirst($id),
              '@data' => implode(',', $data),
            ]
            ));

          }

          break;
      }
    }
    else {
      $this->logger->warning($this->t('No Data available to Process.'));

    }

  }

}
