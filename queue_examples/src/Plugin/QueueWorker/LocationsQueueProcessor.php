<?php

namespace Drupal\queue_examples\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Processes Locations Data.
 *
 * @QueueWorker(
 *   id = "locations_queue_processor",
 *   title = @Translation("Locations Queue Processor."),
 *   cron = {"time" = 10}
 * )
 */
class LocationsQueueProcessor extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * Database service.
   *
   * @var \Drupal\Core\Database\Connection
   */

  protected $database;

  /**
   * {@inheritdoc}
   */
  public function __construct(LoggerChannelFactoryInterface $logger, MessengerInterface $messenger, Connection $connection) {
    $this->logger = $logger->get('queue_examples');
    $this->messenger = $messenger;
    $this->database = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {

    if (!empty($data)) {

      if (isset($data['country_name'])) {

        $query = $this->database
          ->select('queue_examples_countries_data', 'qecd')
          ->fields('qecd', ['country_id']);
        $query->condition('qecd.country_name', $data['country_name']);

        $results = $query->execute()->fetchObject();

        if (!isset($results->country_id)) {

          $query = $this->database->insert('queue_examples_countries_data');
          $query->fields($data);
          $query->execute();

          $this->logger->notice($this->t('Country Data Inserted : @data', ['@data' => implode(',', $data)]));

        }
        else {

          $query = $this->database->update('queue_examples_countries_data');
          $query->fields($data);
          $query->condition('country_id', $data['country_id']);
          $query->execute();

          $this->logger->notice($this->t('Country Data Updated : @data', ['@data' => implode(',', $data)]));

        }

      }
      elseif ($data['state_name']) {

        $query = $this->database
          ->select('queue_examples_states_data', 'qesd')
          ->fields('qesd', ['state_id']);
        $query->condition('qesd.state_name', $data['state_name']);
        $results = $query->execute()->fetchObject();

        if (!isset($results->state_id)) {

          $query = $this->database->insert('queue_examples_states_data');
          $query->fields($data);
          $query->execute();

          $this->logger->notice($this->t('State Data Inserted : @data', ['@data' => implode(',', $data)]));

        }
        else {

          $query = $this->database->update('queue_examples_states_data');
          $query->fields($data);
          $query->condition('state_id', $data['state_id']);
          $query->execute();

          $this->logger->notice($this->t('State Data Updated : @data', ['@data' => implode(',', $data)]));

        }

      }

    }
  }

}
