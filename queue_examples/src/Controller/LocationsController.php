<?php

namespace Drupal\queue_examples\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Serialization\Json;

/**
 * Class LocationsController.
 *
 * @package Drupal\queue_examples\Controller
 */
class LocationsController extends ControllerBase {

  /**
   * Guzzle\Client instance.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The time.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Drupal\Core\Queue\QueueFactory instance.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * Drupal\Core\Queue\QueueWorkerManager instance.
   *
   * @var \Drupal\Core\Queue\QueueWorkerManager
   */
  protected $queueManager;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Database service.
   *
   * @var \Drupal\Core\Database\Connection
   */

  protected $database;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The pager manager.
   *
   * @var \Drupal\Core\Pager\PagerManagerInterface
   */
  protected $pagerManager;

  /**
   * Pager Params service.
   *
   * @var Drupal\Core\Pager\PagerParameters
   */
  protected $pagerParam;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {

    $instance = parent::create($container);
    $instance->messenger = $container->get('messenger');
    $instance->logger = $container->get('logger.factory')->get('queue_examples');
    $instance->moduleHandler = $container->get('module_handler');
    $instance->pagerManager = $container->get('pager.manager');
    $instance->database = $container->get('database');
    $instance->messenger = $container->get('messenger');
    $instance->httpClient = $container->get('http_client');
    $instance->time = $container->get('datetime.time');
    $instance->queueFactory = $container->get('queue');
    $instance->request = $container->get('request_stack')->getCurrentRequest();

    return $instance;
  }

  /**
   * Returns a render-able array for a location import.
   */
  public function import() {

    $header = $decoded = [];

    $header = ['State Id', 'State Name', 'Country Id'];

    $moduleExist = $this->moduleHandler->moduleExists('example_codes');

    if ($moduleExist) {

      $modulePath = $this->moduleHandler->getModule('example_codes')->getPath();
      $file = $modulePath . '/data/states.json';
      $locationData = file_get_contents($file);
      $decoded = Json::decode($locationData);

      foreach ($decoded['states'] as $value) {

        $data = [
          'state_id' => $value['id'],
          'state_name' => $value['name'],
          'country_id' => $value['country_id'],
          'created' => gmdate("d/m/y, H:i:s", $this->time->getRequestTime()),
        ];

        // Create new queue item.
        $queue = $this->queueFactory->get('locations_queue_processor');
        $queue->createItem($data);
        // $queue->deleteItem($data);
        $this->logger->notice($this->t('Pushed %count items into Queue', ['%count' => count($decoded['states'])]));
      }
    }
    else {
      $this->logger->error('Example Codes Module Not Exist');
      $this->messenger->addError("Example Codes Module Not Exist");
    }

    $rows = $decoded['states'];

    $rowPiece = $this->pagerArray($rows, 50);

    $build['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rowPiece,
      '#empty' => $this->t('No content has been found.'),
    ];
    $build['pager'] = [
      '#type' => 'pager',
    ];
    return $build;
  }

  /**
   * Returns pager array.
   */
  public function pagerArray($items, $itemsPerPage) {

    // Get total items count.
    $total = count($items);
    // Get the number of the current page.
    $currentPage = $this->pagerManager->createPager($total, $itemsPerPage)->getCurrentPage();
    // Split an array into chunks.
    $chunks = array_chunk($items, $itemsPerPage);
    // Return current group item.
    $currentPageItems = $chunks[$currentPage];

    return $currentPageItems;

  }

}
