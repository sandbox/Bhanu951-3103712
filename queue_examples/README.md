# Queue Examples

To List Queues :

>  drush @site.alias queue:list

To Run a Queue :

>  drush @site.alias queue:run QUEUE_NAME

To Delete a Queue :

>  drush @site.alias queue:delete QUEUE_NAME
