<?php

declare(strict_types=1);

namespace Drupal\Tests\command_examples\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test Command Examples Module.
 *
 * @group command_examples
 */
final class LoadTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'block',
    'command_examples',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->drupalPlaceBlock('page_title_block');
  }

  /**
   * Tests Homepage after enabling Command Examples Module.
   */
  public function testHomepage() {
    // Test homepage.
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);

    // Minimal homepage title.
    $this->assertSession()->pageTextContains('Log in');
  }

  /**
   * Test Admin Pages access.
   */
  public function testAdminPageAccess() {
    $admin_user = $this->drupalCreateUser(['access administration pages']);
    $this->drupalLogin($admin_user);
    $this->drupalGet('admin');
    $this->assertSession()->elementExists('xpath', '//h1[text() = "Administration"]');
  }

  /**
   * Tests the Command Examples module unistall.
   */
  public function testModuleUninstall() {

    $admin_user = $this->drupalCreateUser([
      'access administration pages',
      'administer site configuration',
      'administer modules',
    ]);

    // Uninstall the module.
    $this->drupalLogin($admin_user);
    $this->drupalGet('/admin/modules/uninstall');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Command Examples');
    $this->submitForm(['uninstall[command_examples]' => TRUE], 'Uninstall');
    $this->submitForm([], 'Uninstall');
    $this->assertSession()->pageTextContains('The selected modules have been uninstalled.');
    $this->assertSession()->pageTextNotContains('Command Examples');

    // Visit the frontpage.
    $this->drupalGet('');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Tests Command Examples module reinstalling after being uninstalled.
   */
  public function testReinstallAfterUninstall() {

    $admin_user = $this->drupalCreateUser([
      'access administration pages',
      'administer site configuration',
      'administer modules',
    ]);

    // Uninstall the module.
    $this->drupalLogin($admin_user);
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    // Uninstall the Command Examples module.
    $this->container->get('module_installer')->uninstall(['command_examples'], FALSE);

    $this->drupalGet('/admin/modules');
    $page->checkField('modules[command_examples][enable]');
    $page->pressButton('Install');
    $assert_session->pageTextNotContains('Unable to install Command Examples');
    $assert_session->pageTextContains('Module Command Examples has been enabled');
  }

}
