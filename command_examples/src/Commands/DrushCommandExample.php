<?php

namespace Drupal\command_examples\Commands;

use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 */
class DrushCommandExample extends DrushCommands {

  /**
   * Drush command that displays the given text.
   *
   * @param string $text
   *   Argument with message to be displayed.
   * @param array $options
   *   Argument with functiona call.
   *
   * @command command_examples:message
   * @aliases ce:msg, ce:m
   * @option uppercase
   *   Uppercase the message.
   * @option reverse
   *   Reverse the message.
   * @usage command_examples:message --uppercase --reverse DruPaL8
   */
  public function message($text = 'Hello world!', array $options = [
    'uppercase' => FALSE,
    'reverse' => FALSE,
  ]) {
    if ($options['uppercase']) {
      $text = strtoupper($text);
    }
    if ($options['reverse']) {
      $text = strrev($text);
    }
    $this->output()->writeln($text);
  }

}
