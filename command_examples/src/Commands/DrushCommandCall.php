<?php

namespace Drupal\command_examples\Commands;

use Drush\Commands\DrushCommands;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use GuzzleHttp\ClientInterface;
use Drupal\Component\Utility\Random;

/**
 * A Drush Command Call command file.
 */
class DrushCommandCall extends DrushCommands {

  /**
   * The class resolver.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected $classResolver;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $class_resolver
   *   The class resolver.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   */
  public function __construct(ClassResolverInterface $class_resolver, EntityTypeManagerInterface $entity_type_manager, ClientInterface $http_client) {
    $this->classResolver = $class_resolver;
    $this->entityTypeManager = $entity_type_manager;
    $this->httpClient = $http_client;
  }

  /**
   * Drush command that calls controller.
   *
   * @param string $action
   *   Argument to call controller method.
   *
   * @command command_examples:call controller
   * @aliases ce:call con
   * @usage command_examples:call controller
   */
  public function call($action) {

    if ($action == 'controller' || $action == 'con') {

      $con = $this->classResolver->getInstanceFromDefinition('\Drupal\command_examples\Controller\CommandExamplesController');
      $con->build();
      $this->logger()->notice(dt('Command Examples ' . $action . ' Called.'));

    }
    elseif ($action == 'rand' || $action == 'random') {

      $random = new Random();
      $email = $random->name() . '@example.com';
      $this->logger()->notice(dt('Geerated Random Email is : ' . $email));

    }
    elseif ($action == 'crypt') {

      $number = rand(10, 10000);
      $crypt = random_bytes($number);
      $this->logger()->notice(dt('Generated Random Number is : ' . $crypt));

    }
    else {
      $text = strtoupper($action);
      $this->output()->writeln($text);
    }

  }

}
