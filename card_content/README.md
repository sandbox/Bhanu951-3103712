# Card Content Type.

Provides Card content type.
    : Where you can use this to add new Card item.
    : Includes block view display to list all the Cards in table 
    format with exposed filter.

Sourced From :

https://hostadvice.com/how-to/how-to-create-a-custom-content-type-for-drupal-8-with-fields/
