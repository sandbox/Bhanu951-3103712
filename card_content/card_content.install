<?php

/**
 * @file
 * Install file for Card Content module.
 *
 * Used for hook functions that are executed during module install.
 */

use Drupal\taxonomy\Entity\Vocabulary;

use Drupal\taxonomy\Entity\Term;

/**
 * Implements hook_install().
 */
function card_content_install() {

  // Machine name of the Taxonomy vocabulary.
  $vid = 'card_terms';

  // Check if the Vocabuary exists.
  $vocabulary = Vocabulary::load($vid);

  $language = \Drupal::languageManager()->getCurrentLanguage()->getId();

  if (empty($vocabulary)) {
    Vocabulary::create([
      'vid' => $vid,
      'description' => 'Creates Card Terms Vocabulary.',
      'langcode' => $language,
      'name' => 'Card Terms',
      'weight' => 0,
    ])->save();
  }

  $weight = 0;

  // List of Terms.
  $terms = [
    'People',
    'Business',
    'Invitation',
    'Marriage Invitation',
    'Others',
    'News Room',
    'Holiday Invitation',
  ];

  $tid = [];

  foreach ($terms as $term) {

    $query = \Drupal::entityQuery('taxonomy_term');
    $query->condition('vid', $vid);
    $query->condition('name', $term);
    $tid = $query->execute();

    if (empty($tid)) {

      $newTerm = Term::create([
        'langcode' => $language,
        'name' => $term,
        'description' => [
          'value' => '<p>' . $term . ' description.</p>',
          'format' => 'full_html',
        ],
        'weight' => $weight++,
        'vid' => $vid,
        'parent' => [],
      ]);

      $createdTerm = $newTerm->save();

      $string = str_replace(' ', '-', mb_strtolower($term));

      $path_alias = \Drupal::entityTypeManager()->getStorage('path_alias')->create([
        'path' => '/taxonomy/term/' . $newTerm->id(),
        'alias' => '/card/' . $string,
        'langcode' => $language,
      ]);
      $path_alias->save();

    }
    else {

      foreach ($tid as $index => $id) {
        // Check if the Term exists.
        $oldTerm = Term::load($id);

        if (empty($oldTerm->id())) {

          $newTerm = Term::create([
            'langcode' => $language,
            'name' => $term,
            'description' => [
              'value' => '<p>' . $term . ' description.</p>',
              'format' => 'full_html',
            ],
            'weight' => $weight++,
            'vid' => $vid,
            'parent' => [],
          ]);

          $createdTerm = $newTerm->save();

          $string = str_replace(' ', '-', mb_strtolower($term));

          $path_alias = \Drupal::entityTypeManager()->getStorage('path_alias')->create([
            'path' => '/taxonomy/term/' . $newTerm->id(),
            'alias' => '/card/' . $string,
            'langcode' => $language,
          ]);
          $path_alias->save();

        }
      }
    }

  }
}

/**
 * Implements hook_uninstall().
 */
function card_content_uninstall() {

  // Delete all nodes of given content type.
  $storage_handler = \Drupal::entityTypeManager()->getStorage('node');
  $nodes = $storage_handler->loadByProperties(['type' => 'card']);

  if (!empty($nodes)) {
    $storage_handler->delete($nodes);
  }

  // Machine name of the Taxonomy vocabulary.
  $vid = 'card_terms';

  // Check if the Vocabuary exists.
  $vocabulary = Vocabulary::load($vid);

  if (!empty($vocabulary)) {

    // Delete Vocabulary and Terms.
    $tids = \Drupal::entityQuery('taxonomy_term')
      ->condition('vid', $vid)
      ->execute();

    if (!empty($tids)) {
      $controller = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
      $entities = $controller->loadMultiple($tids);
      $controller->delete($entities);
    }

    $vocabulary->delete();

    \Drupal::logger('card_content')->info('Card Terms Vocabulary Deleted.');

  }

  // Delete content type.
  $content_type = \Drupal::entityTypeManager()
    ->getStorage('node_type')
    ->load('card');
  $content_type->delete();

  \Drupal::logger('card_content')->info('Card Content Deleted.');

}
