<?php

namespace Drupal\tab_examples\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Component\Utility\Unicode;

/**
 * Controller for our dynamic tab.
 */
class UserDetailsController extends ControllerBase {

  /**
   * The user account data service.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * DynamicController constructor.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The currently logged in account.
   */
  public function __construct(AccountInterface $current_user) {
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user')

    );
  }

  /**
   * Returns a static message.
   */
  public function details() {

    $userName = $this->currentUser->getAccountName();

    $build['output'] = [
      '#type' => 'markup',
      '#markup' => $this->t('User Details Page of @user', ['@user' => Unicode::ucfirst($userName)]),
    ];
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    if ($this->currentUser->isAuthenticated()) {

      $userName = $this->currentUser->getAccountName();

      return $this->t('@userName Details', ['@userName' => Unicode::ucfirst($userName)]);
    }
  }

}
