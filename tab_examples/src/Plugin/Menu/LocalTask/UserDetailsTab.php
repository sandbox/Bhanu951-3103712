<?php

namespace Drupal\tab_examples\Plugin\Menu\LocalTask;

use Drupal\Core\Menu\LocalTaskDefault;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Session\AccountInterface;
use Drupal\Component\Utility\Unicode;

/**
 * Provides route parameters needed to link to the current user tab.
 */
class UserDetailsTab extends LocalTaskDefault implements ContainerFactoryPluginInterface {
  use StringTranslationTrait;
  /**
   * Current user object.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Construct the UserDetailsTab object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The currently logged in account.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, AccountInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getRouteParameters(RouteMatchInterface $route_match) {
    $user_id = $route_match->getParameter('user');
    return ['user' => empty($user) ? $this->currentUser->Id() : $user_id];
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(Request $request = NULL) {
    if ($this->currentUser->isAuthenticated()) {

      $userName = $this->currentUser->getAccountName();

      return $this->t('@userName Details', ['@userName' => Unicode::ucfirst($userName)]);
    }
  }

}
