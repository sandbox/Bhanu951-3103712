<?php

namespace Drupal\tab_examples\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;

/**
 * Derivative class that provides the local task links for the Products.
 */
class ProductTypeLocalTaskLinksDerivative extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $links = [];

    $products = [
      'cooking_gear' => 'Cooking Gear',
      'tents' => 'Tents' ,
      'sleeping_bags' => 'Sleeping Bags',
      'rope' => 'Rope',
      'safety' => 'Safety',
      'packs' => 'Packs',
    ];

    foreach ($products as $key => $value) {
      $links['tab_examples_products_menu_' . $key] = [
        'title' => $value . ' Controller',
        'parent_id' => 'tab_examples.base',
        'route_name' => 'tab_examples.dynamic_routes' . $key,
      ] + $base_plugin_definition;
    }
    return $links;
  }

}
