<?php

namespace Drupal\constraint_examples\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the Example Url constraint.
 */
class ExampleUrlConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($entity, Constraint $constraint) {

    $entity = $entity->getEntity();

    $youtubeDomains = [
      'www.youtube.com',
      'youtube.com',
      'www.youtu.be',
      'youtu.be',
    ];

    // Get Field Value.
    $urlField = $entity->get('field_youtube_url')->getString();
    // Get URL from Field value.
    $url = explode(",", $urlField);

    $parse = parse_url($url[0]);

    // Check host name exists in domain or not.
    if (in_array($parse['host'], $youtubeDomains)) {

      $this->context->addViolation($constraint->errorMessage, ['@domains' => implode(", ", $youtubeDomains)]);
    }

  }

}
