<?php

namespace Drupal\constraint_examples\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Provides an Example url constraint.
 *
 * Accepts only Youtube URLs.
 *
 * @Constraint(
 *   id = "ExampleUrlConstraint",
 *   label = @Translation("URL Validation", context = "Validation"),
 * )
 */
class ExampleUrlConstraint extends Constraint {
  /**
   * The Error Message for Youtube URL.
   *
   * @var string
   */
  public $errorMessage = 'Invalid Domain, accepted domains are @domains';

}
