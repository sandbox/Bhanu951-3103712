id: subscription_list_csv_import
label: Import Subscription List from CSV
migration_group: subscription_list
migration_tags:
  - Subscription List CSV Migration

source:
  plugin: 'csv'
  # Full path to the file.
  path: modules/custom/example_codes/migration_examples/sources/subscription-list-small.csv
  # Column delimiter. Comma (,) by default.
  delimiter: ','
  # Field enclosure. Double quotation marks (") by default.
  enclosure: '"'
  # The row to be used as the CSV header (indexed from 0),
  # or null if there is no header row.
  header_offset: 0
  # The column(s) to use as a key. Each column specified will
  # create an index in the migration table and too many columns
  # may throw an index size error.
  ids:
    - title
  # Here we identify the columns of interest in the source file.
  # Each numeric key is the 0-based index of the column.
  # For each column, the key below is the field name assigned to
  # the data on import, to be used in field mappings below.
  # The label value is a user-friendly string for display by the
  # migration UI.
  fields:
    0:
      name: title
      label: 'Unique Subs Id'
    1:
      name: field_account_name
      label: 'Account Name'
    2:
      name: field_account_id
      label: 'Account ID.'
    3:
      name: field_account_no
      label: 'Account No.'
    4:
      name: field_phone
      label: 'Phone'
    5:
      name: field_account_address
      label: 'Account Address'
    6:
      name: field_account_status
      label: 'Account Status'
    7:
      name: field_domain
      label: 'Domain'
    8:
      name: field_rate_plan
      label: 'Rate Plan'
    9:
      name: field_rate_plan_type
      label: 'Rate Plan Type'
    10:
      name: field_activation_date
      label: 'Activation Date'
    11:
      name: field_expiry_date
      label: 'Expiry Date'
    12:
      name: field_balances
      label: 'Balances'
    13:
      name: field_subscription_status
      label: 'Subscription Status'
    14:
      name: field_account_external_id
      label: 'Account External ID'
    15:
      name: field_upload
      label: 'FUP Balance Upload'
    16:
      name: field_download
      label: 'FUP Balance Download'
    17:
      name: field_total
      label: 'FUP Balance Total'
    18:
      name: field_profile_picture
      label: 'Profile Picture'
    19:
      name: field_document_name
      label: 'Address Proof Type'
    20:
      name: field_document
      label: 'Address Proof'
  constants:
    bool_0: 0
    bool_1: 1
    UID_ONE: 1
    UID_TWO: 2
    restricted_html: restricted_html
    PROFILE_PICTURE_TITLE: "'s Profile Picture"
    PROFILE_PICTURES_FILE_DIRECTORY: 'public://profile_pictures/'
process:
  title:
    -
      plugin: concat
      source:
        - title
        - field_account_id
      delimiter: ' - '
    -
      plugin: callback
      callable: ucfirst

  # Use the default_value plugin process for the uid destination field.
  uid:
    plugin: default_value
    default_value: 2
  status:
    plugin: default_value
    default_value: 1
  created: created
  changed: changed
  revision_uid: revision_uid
  revision_log: log
  revision_timestamp: timestamp
  langcode:
    plugin: default_value
    source: language
    default_value: "en"
  field_account_name: field_account_name
  field_account_id: field_account_id
  field_account_no: field_account_no
  field_phone:
    -
      plugin: explode
      delimiter: '-'
      source: field_phone
    -
      plugin: callback
      callable: trim
  field_account_address/value:
    -
      plugin: get
      source: field_account_address
    -
      plugin: str_replace
      case_insensitive: true
      source: field_account_address
      search: ["H no", " flat"]
      replace: ["H.No.: ", "Flat No.:"]

  field_account_address/format:
    -
      plugin: default_value
      default_value: basic_html
  field_account_status:
    -
      plugin: callback
      callable: strtolower
      source: field_account_status
  field_domain: field_domain
  field_rate_plan: field_rate_plan
  field_rate_plan_type: field_rate_plan_type

  #activation date of the account.
  field_activation_date:
    -
      plugin: format_date
      from_format: Y-m-d G:i:s
      to_format: 'Y-m-d\TH:i:s'
      from_timezone: 'America/Managua'
      to_timezone: 'UTC'
      source: field_activation_date

  # expiry date of the account.
  field_expiry_date:
    -
      plugin: format_date
      from_format: Y-m-d G:i:s
      to_format: 'Y-m-d\TH:i:s'
      from_timezone: 'America/Managua'
      to_timezone: 'UTC'
      source: field_expiry_date

  field_balances: field_balances
  field_subscription_status:
    -
      plugin: callback
      callable: strtolower
      source: field_subscription_status
  field_account_external_id:
    -
      plugin: default_value
      source: field_account_external_id
      default_value: 'Drupal Admin'
    -
      plugin: callback
      callable: ucfirst

  # Paragraphs field.
  pseudo_field_fup_details:
    -
      plugin: migration_lookup
      migration: fup_balance_csv_import
      source: title # Unique idetifier.
  field_fup_details:
    -
      plugin: sub_process
      source:
        - '@pseudo_field_fup_details'
      process:
        target_id: '0'
        target_revision_id: '1'

  # Image Field Import
  field_profile_picture/target_id:
    -
      plugin: migration_lookup
      migration: subscription_list_profile_picture_import
      source: title # Unique idetifier.
  field_profile_picture/alt:
    -
      plugin: concat
      source:
        - field_account_name
        - constants/PROFILE_PICTURE_TITLE
      delimiter: ''

  # Address Proof Paragraph Field Import
  pseudo_field_address_proof:
    -
      plugin: migration_lookup
      migration: address_proof_import
      source: title # Unique idetifier.
  field_address_proof:
    -
      plugin: sub_process
      source:
        - '@pseudo_field_address_proof'
      process:
        target_id: '0'
        target_revision_id: '1'

destination:
  plugin: entity:node
  default_bundle: subscription_list

migration_dependencies:
  required: { }
  optional:
    - subscription_list_profile_picture_import
    - address_proof_import
    - fup_balance_csv_import

dependencies:
  enforced:
    module:
      - migration_examples
