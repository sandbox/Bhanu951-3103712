<?php

namespace Drupal\migration_examples\Commands;

use Drupal\Core\Serialization\Yaml;
use Drush\Commands\DrushCommands;

/**
 * A Drush Command File Class to Generate Csv Migration Column Headers.
 */
class GenerateCsvMigrationColumnHeaders extends DrushCommands {

  /**
   * Generates the YAML representation of the CSV headers.
   *
   * @param string $file
   *   The relative path to the file.
   *
   * @usage generate-csv-migration-column-headers foo/relative/path
   *   Accept Relative path of file as argument.
   *
   * @command generate-csv-migration-column-headers
   * @aliases gcsvmch
   *
   * @see https://www.webomelette.com/quickly-generate-headers-csv-migrate-source-plugin-using-drush
   *   For Source.
   */
  public function generateYaml($file) {
    $spl = new \SplFileObject($this->getConfig()->cwd() . DIRECTORY_SEPARATOR . $file, 'r');
    $spl->next();
    $headers = $spl->fgetcsv();

    $source_headers = [];
    foreach ($headers as $header) {
      $source_headers[] = [$header => $header];
    }

    $yml = Yaml::encode($source_headers);
    $this->output()->write($yml);
  }

}
