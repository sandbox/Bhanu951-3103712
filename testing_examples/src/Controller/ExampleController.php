<?php

namespace Drupal\testing_examples\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Testing Examples routes.
 */
class ExampleController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('Example Controller Works!'),
      '#cache' => ['max-age' => 0],
    ];

    return $build;

  }

}
