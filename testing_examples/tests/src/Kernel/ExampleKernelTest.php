<?php

namespace Drupal\Tests\testing_examples\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\user\RoleInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Example Kernel Test.
 *
 * @group testing_examples
 */
class ExampleKernelTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'system',
    'testing_examples',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Mock required services here.
    $this->installConfig(['user']);
    $this->installSchema('system', ['sequences']);
    $this->installEntitySchema('user');
    // Create anonymous user.
    $anonymous = $this->container->get('entity_type.manager')
      ->getStorage('user')
      ->create([
        'uid' => 0,
        'status' => 0,
        'name' => '',
      ]);
    $anonymous->save();
    /** @var \Drupal\user\RoleInterface $anonymous_role */
    $anonymous_role = $this->container->get('entity_type.manager')
      ->getStorage('user_role')
      ->load(RoleInterface::ANONYMOUS_ID);
    $anonymous_role->grantPermission('access content');
    $anonymous_role->save();

    $this->installConfig(['testing_examples']);

    $this->routeProvider = $this->container->get('router.route_provider');

  }

  /**
   * Test callback.
   */
  public function testSomething() {
    $result = $this->container->get('transliteration')->transliterate('Друпал');
    $this->assertSame('Drupal', $result);
  }

  /**
   * Test that the no_cache and _admin_route options are added to the routes.
   */
  public function testRouteDefinitions() {

    // Check if the no_cache options is added to the example controller.
    $route = $this->routeProvider->getRouteByName('testing_examples.example_controller');
    $this->assertEquals('TRUE', $route->getOption('no_cache'));

    // Check if the _admin_route options is added for the config form.
    $route = $this->routeProvider->getRouteByName('testing_examples.example_config_form');
    $this->assertEquals('TRUE', $route->getOption('_admin_route'));

  }

  /**
   * Test Module Config Import.
   */
  public function testModuleConfigImport() {

    // Get the config factory service.
    $config_factory = $this->container->get('config.factory');

    // Get api_url variables.
    $api_url = $config_factory->get('testing_examples.settings')->get('api_url');

    // Get the module handler service.
    $module_handler = $this->container->get('module_handler');
    $module_path = $module_handler->getModule('testing_examples')->getPath();
    $file_path = DRUPAL_ROOT . DIRECTORY_SEPARATOR . $module_path . '/config/install/testing_examples.settings.yml';

    if ($file_path) {
      $file_contents = file_get_contents($file_path);
      if (!empty($file_contents)) {
        $yml_data = Yaml::parse($file_contents);
        // Verify the stored config values are matched.
        $this->assertEquals($yml_data["api_url"], $api_url);
      }
    }

  }

}
