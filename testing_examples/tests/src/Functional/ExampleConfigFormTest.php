<?php

namespace Drupal\Tests\testing_examples\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Class to test Example Config Form.
 *
 * @group testing_examples
 */
class ExampleConfigFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stable';

  /**
   * The profile to install as a basis for testing.
   *
   * @var string
   */
  protected $profile = 'minimal';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['node', 'block', 'testing_examples'];

  /**
   * {@inheritdoc}
   */
  protected function tearDown(): void {
    parent::tearDown();
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Set up the test here.
    // Place Title Block.
    $this->drupalPlaceBlock('page_title_block');

  }

  /**
   * Tests Homepage.
   */
  public function testHomepage() {

    // Test homepage.
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);

    // Minimal homepage title.
    // By default there's nothing visible on the site except for a login form.
    $this->assertSession()->pageTextContains('Log in');
    $this->assertSession()->pageTextContains('Enter your Drupal username');
    $this->assertSession()->pageTextContains('Enter the password that accompanies your username');

  }

  /**
   * Test Example Config Form Access.
   */
  public function testExampleConfigFormAccess() {

    // Test Example Form Access with Test User.
    $test_user = $this->drupalCreateUser(['administer testing_examples configuration']);
    $this->drupalLogin($test_user);
    $this->drupalGet('testing-examples/example-config-form');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('This example demonstrates the use of Config Form API.');
    $this->assertSession()->pageTextContains('Example Config Form');
    $this->assertSession()->pageTextContains('API URL');
    $this->assertSession()->pageTextContains('This example demonstrates the use of Config Form API.');
    $this->drupalLogout();

    // Test Example Form Access with Admin User.
    $admin_user = $this->drupalCreateUser(['administer site configuration']);
    $this->drupalLogin($admin_user);
    $this->drupalGet('testing-examples/example-config-form');
    $this->assertSession()->statusCodeEquals(403);
    $this->assertSession()->pageTextContains('Access denied');
    $this->assertSession()->pageTextContains('You are not authorized to access this page.');
    $this->drupalLogout();

    // Test Example Form Access with Annon User.
    $annon_user = $this->drupalCreateUser(['access content']);
    $this->drupalLogin($annon_user);
    $this->drupalGet('testing-examples/example-config-form');
    $this->assertSession()->statusCodeEquals(403);
    $this->assertSession()->pageTextContains('Access denied');
    $this->assertSession()->pageTextContains('You are not authorized to access this page.');

  }

  /**
   * Test Example Config Form Submit.
   */
  public function testExampleConfigFormSubmit() {

    $valid_urls = [
      'http://example-server',
      'http://example.com',
      'http://example.com/',
      'http://subdomain.example.com/path/?parameter1=value1&parameter2=value2',
      'random-protocol://example.com',
      'http://example.com:80',
      'http://example.com?no-path-separator',
      'http://example.com/pa%20th/',
      'ftp://example.org/resource.txt',
      'file://../../../relative/path/needs/protocol/resource.txt',
      'http://example.com/#one-fragment',
      'http://example.edu:8080#one-fragment',
      'urn:isbn:0-486-27557-4',
      'urn:example:mammal:monotreme:echidna',
      'urn:mpeg:mpeg7:schema:2001',
      'urn:uuid:6e8bc430-9c3a-11d9-9669-0800200c9a66',
      'rare-urn:uuid:6e8bc430-9c3a-11d9-9669-0800200c9a66',
      'urn:FOO:a123,456',
    ];

    $invalid_urls = [
      'only-text',
      'http//missing.colon.example.com/path/?parameter1=value1&parameter2=value2',
      'missing.protocol.example.com/path/',
      'http://example.com\\bad-separator',
      'http://example.com|bad-separator',
      'ht tp://example.com',
      'http://exampl e.com',
      'http://example.com/pa th/',
      '../../../relative/path/needs/protocol/resource.txt',
      'http://example.com/#two-fragments#not-allowed',
      'http://example.edu:portMustBeANumber#one-fragment',
      'urn:mpeg:mpeg7:sch ema:2001',
      'urn|mpeg:mpeg7:schema:2001',
      'urn?mpeg:mpeg7:schema:2001',
      'urn%mpeg:mpeg7:schema:2001',
      'urn#mpeg:mpeg7:schema:2001',
    ];

    // Test Example Form Access with Test User.
    $test_user = $this->drupalCreateUser(['administer testing_examples configuration']);
    $this->drupalLogin($test_user);
    $this->drupalGet('testing-examples/example-config-form');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('xpath', '//div[contains(@class, \'form-item-description\')]');
    $this->assertSession()->elementExists('css', '.form-item, .form-actions');
    $this->assertSession()->elementExists('xpath', '//h1[text() = "Example Config Form"]');
    $this->assertSession()->buttonExists('Save configuration');
    $this->assertSession()->fieldExists('api_url');
    $this->assertSession()->fieldNotExists('message');
    // $this->assertSession()->fieldExists('description');
    // Create invalid form data array item.
    $data1 = [];
    $data1 = [
      'api_url' => $invalid_urls[mt_rand(0, 15)],
    ];
    $this->submitForm($data1, 'Save configuration');
    $this->assertSession()->pageTextContains("The Entered API URL is not valid.");
    $this->assertSession()->addressEquals('testing-examples/example-config-form');

    // Create valid form data array item.
    $data2 = [];
    $index = mt_rand(0, 17);
    $data2 = [
      'api_url' => $valid_urls[$index],
    ];

    $this->submitForm($data2, 'Save configuration');
    $this->assertSession()->pageTextContains("The configuration options have been saved.");
    $this->assertSession()->pageTextContains("The Entered API URL has been saved");

    // Get the config factory service.
    $config_factory = $this->container->get('config.factory');

    // Get api_url variables.
    $api_url = $config_factory->get('testing_examples.settings')->get('api_url');

    // Verify the config values are stored.
    $this->assertEquals($valid_urls[$index], $api_url);

    $this->assertSession()->addressEquals('testing-examples/example-config-form');

    // Create invalid form data array item.
    $data3 = [];
    $data3 = [
      'api_url' => '',
    ];

    $this->submitForm($data3, 'Save configuration');
    $this->assertSession()->pageTextContains("API URL field is required.");
    $this->assertSession()->pageTextNotContains("The Entered API URL has been saved");

    // $this->createScreenshot('public://test.png');
    // $this->assertSession()->addressEquals("<front>");
    // Verify that the creation message contains a link to a term.
    // $this->assertSession()->elementExists('xpath', '//div[@data-drupal-messages]//a[contains(@href, "term/")]');
  }

}
