<?php

namespace Drupal\Tests\testing_examples\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Class to test Example Controller.
 *
 * @group testing_examples
 */
class ExampleControllerTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stable';

  /**
   * The profile to install as a basis for testing.
   *
   * @var string
   */
  protected $profile = 'minimal';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['node', 'block', 'testing_examples'];

  /**
   * {@inheritdoc}
   */
  protected function tearDown(): void {
    parent::tearDown();
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Set up the test here.
    // Place Title Block.
    $this->drupalPlaceBlock('page_title_block');

  }

  /**
   * Tests Homepage.
   */
  public function testHomepage() {

    // Test homepage.
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);

    // Minimal homepage title.
    // By default there's nothing visible on the site except for a login form.
    $this->assertSession()->pageTextContains('Log in');
    $this->assertSession()->pageTextContains('Enter your Drupal username');
    $this->assertSession()->pageTextContains('Enter the password that accompanies your username');

  }

  /**
   * Test Example Controller Access.
   */
  public function testExampleControllerAccess() {

    // Test Example Controller Access with Test User.
    $test_user = $this->drupalCreateUser(['administer testing_examples configuration']);
    $this->drupalLogin($test_user);
    $this->drupalGet('testing-examples/example-controller');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Example Controller Works!');
    $this->assertSession()->pageTextContains('Example Controller');
    $this->drupalLogout();

    // Test Example Controller Access with Admin User.
    $admin_user = $this->drupalCreateUser(['administer site configuration']);
    $this->drupalLogin($admin_user);
    $this->drupalGet('testing-examples/example-controller');
    $this->assertSession()->statusCodeEquals(403);
    $this->assertSession()->pageTextContains('Access denied');
    $this->assertSession()->pageTextContains('You are not authorized to access this page.');
    $this->drupalLogout();

    // Test Example Controller Access with Annon User.
    $annon_user = $this->drupalCreateUser(['access content']);
    $this->drupalLogin($annon_user);
    $this->drupalGet('testing-examples/example-controller');
    $this->assertSession()->statusCodeEquals(403);
    $this->assertSession()->pageTextContains('Access denied');
    $this->assertSession()->pageTextContains('You are not authorized to access this page.');

  }

  /**
   * Test the cache properties in response header data.
   */
  public function testControllerCache() {

    // Test Example Controller Access with Test User.
    $test_user = $this->drupalCreateUser(['administer testing_examples configuration']);
    $this->drupalLogin($test_user);
    $this->drupalGet('testing-examples/example-controller');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Example Controller Works!');
    $this->assertSession()->pageTextContains('Example Controller');

    $this->assertSession()->responseContains('Example Controller Works!');
    $this->assertSession()->responseHeaderContains('Cache-Control', 'must-revalidate, no-cache, private');
    $this->assertSession()->responseHeaderEquals('Cache-Control', 'must-revalidate, no-cache, private');
    $this->assertSession()->responseHeaderEquals('Content-Type', 'text/html; charset=UTF-8');

    // The testing-examples/example-controller page is currently uncacheable.
    $this->assertSession()->responseHeaderEquals('X-Drupal-Dynamic-Cache', 'UNCACHEABLE');
    $this->assertSession()->responseHeaderEquals('X-Drupal-Cache-Max-Age', '0 (Uncacheable)');
    $this->getSession()->reload();
    $this->assertSession()->responseHeaderEquals('X-Drupal-Dynamic-Cache', 'UNCACHEABLE');

    $this->assertSession()->responseHeaderEquals('X-Frame-Options', 'SAMEORIGIN');
    $this->assertSession()->responseHeaderEquals('X-Generator', 'Drupal 9 (https://www.drupal.org)');

  }

}
