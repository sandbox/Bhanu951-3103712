<?php

namespace Drupal\Tests\testing_examples\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Class to test Example Form.
 *
 * @group testing_examples
 */
class ExampleFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stable';

  /**
   * The profile to install as a basis for testing.
   *
   * @var string
   */
  protected $profile = 'minimal';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['node', 'block', 'testing_examples'];

  /**
   * {@inheritdoc}
   */
  protected function tearDown(): void {
    parent::tearDown();
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Set up the test here.
    // Place Title Block.
    $this->drupalPlaceBlock('page_title_block');

  }

  /**
   * Tests Homepage.
   */
  public function testHomepage() {

    // Test homepage.
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);

    // Minimal homepage title.
    // By default there's nothing visible on the site except for a login form.
    $this->assertSession()->pageTextContains('Log in');
    $this->assertSession()->pageTextContains('Enter your Drupal username');
    $this->assertSession()->pageTextContains('Enter the password that accompanies your username');

  }

  /**
   * Test Example Form Access.
   */
  public function testExampleFormAccess() {

    // Test Example Form Access with Test User.
    $test_user = $this->drupalCreateUser(['administer testing_examples configuration']);
    $this->drupalLogin($test_user);
    $this->drupalGet('testing-examples/example-form');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('This example demonstrates the use of Form API.');
    $this->assertSession()->pageTextContains('Example Form');
    $this->assertSession()->pageTextContains('Name');
    $this->assertSession()->pageTextContains('Message');
    $this->assertSession()->pageTextContains('This example demonstrates the use of Form API.');
    $this->drupalLogout();

    // Test Example Form Access with Admin User.
    $admin_user = $this->drupalCreateUser(['administer site configuration']);
    $this->drupalLogin($admin_user);
    $this->drupalGet('testing-examples/example-form');
    $this->assertSession()->statusCodeEquals(403);
    $this->assertSession()->pageTextContains('Access denied');
    $this->assertSession()->pageTextContains('You are not authorized to access this page.');
    $this->drupalLogout();

    // Test Example Form Access with Annon User.
    $annon_user = $this->drupalCreateUser(['access content']);
    $this->drupalLogin($annon_user);
    $this->drupalGet('testing-examples/example-form');
    $this->assertSession()->statusCodeEquals(403);
    $this->assertSession()->pageTextContains('Access denied');
    $this->assertSession()->pageTextContains('You are not authorized to access this page.');

  }

  /**
   * Test Example Form Submit.
   */
  public function testExampleFormSubmit() {

    // Test Example Form Access with Test User.
    $test_user = $this->drupalCreateUser(['administer testing_examples configuration']);
    $this->drupalLogin($test_user);
    $this->drupalGet('testing-examples/example-form');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('xpath', '//div[contains(@class, \'form-item-description\')]');
    $this->assertSession()->elementExists('css', '.form-item, .form-actions');
    $this->assertSession()->elementExists('xpath', '//h1[text() = "Example Form"]');
    $this->assertSession()->buttonExists('Send');
    $this->assertSession()->fieldExists('name');
    $this->assertSession()->fieldExists('message');
    // $this->assertSession()->fieldExists('description');
    // Create invalid form data array item.
    $data1 = [];
    $data1 = [
      'name' => $this->randomMachineName(10),
      'message' => $this->randomMachineName(5),
    ];
    $this->submitForm($data1, 'Send');
    $this->assertSession()->pageTextContains("Message should be at least 10 characters.");
    $this->assertSession()->addressEquals('testing-examples/example-form');

    // Create valid form data array item.
    $data2 = [];
    $data2 = [
      'name' => $this->randomMachineName(10),
      'message' => $this->randomMachineName(15),
    ];

    $this->submitForm($data2, 'Send');
    $this->assertSession()->pageTextContains("The message has been sent.");
    $this->assertSession()->addressEquals('testing-examples/example-form');

    // Create invalid form data array item.
    $data3 = [];
    $data3 = [
      'name' => $this->randomMachineName(10),
    ];

    $this->submitForm($data3, 'Send');
    $this->assertSession()->pageTextContains("Message field is required.");
    $this->assertSession()->pageTextNotContains("The message has been sent.");

    // $this->createScreenshot('public://test.png');
    // $this->assertSession()->addressEquals("<front>");
    // Verify that the creation message contains a link to a term.
    // $this->assertSession()->elementExists('xpath', '//div[@data-drupal-messages]//a[contains(@href, "term/")]');
  }

}
