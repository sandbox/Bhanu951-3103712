<?php

namespace Drupal\Tests\testing_examples\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Class to test site load.
 *
 * @group testing_examples
 */
class LoadTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stable';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['node', 'block', 'testing_examples'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalPlaceBlock('page_title_block');
  }

  /**
   * Tests Homepage.
   */
  public function testHomepage() {
    // Test homepage.
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);

    // Minimal homepage title.
    $this->assertSession()->pageTextContains('Log in');
  }

  /**
   * Test Admin Page.
   */
  public function testAdminPage() {

    $admin_user = $this->drupalCreateUser(['access administration pages']);
    $this->drupalLogin($admin_user);
    $this->assertSession()->pageTextContains($admin_user->getDisplayName());
    $this->drupalGet('admin');
    $this->assertSession()->elementExists('xpath', '//h1[text() = "Administration"]');

  }

  /**
   * Test Block Placement.
   */
  public function testBlockPlacement() {

    $content_editor = $this->drupalCreateUser(['administer blocks']);
    // Log in as a content Editor.
    $this->drupalLogin($content_editor);

    // Place the powered by block.
    $this->drupalPlaceBlock('system_powered_by_block', ['id' => 'powered_by']);

    // Visit the frontpage. We should see the block.
    $this->drupalGet('<front>');
    $this->assertSession()->pageTextContains('Powered by Drupal');
    $this->drupalLogout();

  }

  /**
   * Tests the module unistall.
   */
  public function testModuleUninstall() {

    $admin_user = $this->drupalCreateUser([
      'access administration pages',
      'administer site configuration',
      'administer modules',
    ]);

    // Uninstall the module.
    $this->drupalLogin($admin_user);
    $this->drupalGet('/admin/modules/uninstall');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Testing Examples');
    $this->submitForm(['uninstall[testing_examples]' => TRUE], 'Uninstall');
    $this->submitForm([], 'Uninstall');
    $this->assertSession()->pageTextContains('The selected modules have been uninstalled.');
    $this->assertSession()->pageTextNotContains('Testing Examples');

    // Visit the frontpage.
    $this->drupalGet('');
    $this->assertSession()->statusCodeEquals(200);

  }

}
