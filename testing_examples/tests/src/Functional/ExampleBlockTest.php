<?php

namespace Drupal\Tests\testing_examples\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Test description.
 *
 * @group testing_examples
 */
class ExampleBlockTest extends BrowserTestBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stable';

  /**
   * The profile to install as a basis for testing.
   *
   * @var string
   */
  protected $profile = 'minimal';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['node', 'block', 'testing_examples'];

  /**
   * {@inheritdoc}
   */
  protected function tearDown(): void {
    parent::tearDown();
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Set up the test here.
    // Place Title Block.
    $this->drupalPlaceBlock('page_title_block');
  }

  /**
   * Tests Homepage.
   */
  public function testHomepage() {

    // Test homepage.
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);

    // Minimal homepage title.
    // By default there's nothing visible on the site except for a login form.
    $this->assertSession()->pageTextContains('Log in');
    $this->assertSession()->pageTextContains('Enter your Drupal username');
    $this->assertSession()->pageTextContains('Enter the password that accompanies your username');
  }

  /**
   * Test that the block is available.
   */
  public function testBlockAvailability() {
    // Test Example Block Access with Test User.
    $test_user = $this->drupalCreateUser([
      'administer blocks',
      'administer site configuration',
      'access administration pages',
      'administer testing_examples configuration',
    ]);
    $this->drupalLogin($test_user);
    $this->drupalGet('/admin/structure/block');
    $this->clickLink('Place block');
    $this->assertSession()->pageTextContains('Example Block');
    $this->assertSession()->linkByHrefExists('admin/structure/block/add/example_block/', 0);
  }

  /**
   * Test Example Block Appearence.
   */
  public function testExampleBlockAppearence() {

    // http://grep.xnddx.ru/node/30937121
    // Test Example Block Access with Test User.
    $test_user = $this->drupalCreateUser(['administer testing_examples configuration']);
    $this->drupalLogin($test_user);
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    $theme_name = \Drupal::config('system.theme')->get('default');
    $random_string = $this->randomString(10);
    // $this->drupalPlaceBlock('example_block');
    $this->drupalPlaceBlock('example_block', [
      'region' => 'header',
      'id' => 'example_block',
      'example_block_message' => $random_string,
      'theme' => $theme_name,
      'label' => 'Example Block ' . $theme_name,
      'label_display' => 'visible',
    ]);

    // Gets the home page.
    $this->drupalGet('<front>');

    // Check that the custom block is rendered.
    $this->assertSession()->responseContains($this->t('It works! @message', [
      '@message' => $random_string,
    ]));

    // Get the config factory service.
    // $config_factory = $this->container->get('config.factory');
    // $message = $config_factory->get('block_settings')->get('example_block_message');
    // $this->assertSession()->pageTextContains('It works! @message', [
    //   '@message' => $message,
    // ]);.
  }

  /**
   * Test that the Permalink block can be placed and works.
   */
  public function testExampleBlockPlacement() {

    // Test Example Block Access with Test User.
    $test_user = $this->drupalCreateUser([
      'administer blocks',
      'administer site configuration',
      'access administration pages',
      'administer testing_examples configuration',
    ]);
    $this->drupalLogin($test_user);

    // Test Example Block availability in admin "Place blocks" list.
    \Drupal::service('theme_installer')->install(['bartik', 'seven', 'stark']);
    $theme_settings = $this->config('system.theme');

    foreach (['bartik', 'seven', 'stark'] as $theme) {
      $this->drupalGet('admin/structure/block/list/' . $theme);
      $random_string = $this->randomString(10);

      // Configure and save the block.
      $this->drupalPlaceBlock('example_block', [
        'region' => 'header',
        'theme' => $theme,
        'label' => 'Example Block ' . $theme,
        'label_display' => 'visible',
        'id' => 'example_block_' . $theme,
        'example_block_message' => $random_string,
      ]);

      // Set the default theme and ensure the block is placed.
      $theme_settings->set('default', $theme)->save();
      $this->drupalGet('<front>');

      // Check that the custom block is rendered.
      $this->assertSession()->responseContains($this->t('It works! @message', [
        '@message' => $random_string,
      ]));
      // Open admin page and check if HTML copy box is placed.
      $this->drupalGet('admin');
      $this->assertSession()->pageTextContains('Example Block ' . $theme);
    }
  }

}
