<?php

namespace Drupal\batch_examples\Batch;

use Drupal\node\Entity\Node;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Batch for updating content.
 */
class SimpleBatchExample {

  /**
   * {@inheritdoc}
   */
  public static function updateNodes($data, $date, &$context) {
    foreach ($data as $data => $nid) {

      $context['sandbox']['current_item'] = $data;
      $message = 'Updating content with Node Id - ' . $nid . ' at ' . $date;
      $results = [];

      // Calling Node creating function.
      self::importExamResults($nid, $date);

      $context['message'] = $message;
      $context['results'][] = $data;

    }
  }

  /**
   * {@inheritdoc}
   */
  public static function nodesUpdateFinishedCallback($success, $results, $operations) {
    // The 'success' parameter means no fatal PHP errors were detected. All
    // other error management should be handled using 'results'.
    if ($success) {
      $message = \Drupal::translation()->formatPlural(count($results), 'One post processed.', '@count posts processed.'
      );
    }
    else {
      $message = t('Finished with an error.');
    }
    \Drupal::messenger()->addMessage($message);
  }

  /**
   * Function to Create new Nodes.
   */
  private static function importExamResults($nid, DrupalDateTime $date) {

    // Checking if nid is empty.
    if (!empty($nid)) {
      // Creating New Nodes.
      $node = Node::load($nid);
      $node->set('title', 'Article Title for Node ' . $nid);
      $node->setCreatedTime($date->getTimestamp())->save();
      $node->save();
      \Drupal::logger('batch_examples')->info("Node with nid @nid saved!\n", ['@nid' => $node->id()]);
    }
    else {
      \Drupal::logger('batch_examples')->error("No Nodes are present!\n");
    }

  }

}
