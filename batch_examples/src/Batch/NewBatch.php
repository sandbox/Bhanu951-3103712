<?php

namespace Drupal\batch_examples\Batch;

use Drupal\node\Entity\Node;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Batch for updating content.
 */
class NewBatch {
  use StringTranslationTrait;

  /**
   * Processor for batch operations.
   */
  public static function processItems($items, DrupalDateTime $date, array &$context) {
    // Elements per operation.
    $limit = 50;

    // Set default progress values.
    if (empty($context['sandbox']['progress'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($items);
    }

    // Save items to array which will be changed during processing.
    if (empty($context['sandbox']['items'])) {
      $context['sandbox']['items'] = $items;
    }

    $counter = 0;
    if (!empty($context['sandbox']['items'])) {
      // Remove already processed items.
      if ($context['sandbox']['progress'] != 0) {
        array_splice($context['sandbox']['items'], 0, $limit);
      }

      foreach ($context['sandbox']['items'] as $item) {
        if ($counter != $limit) {
          self::processItem($item, $date);

          $counter++;
          $context['sandbox']['progress']++;

          $context['message'] = t('Now processing node :progress of :count', [
            ':progress' => $context['sandbox']['progress'],
            ':count' => $context['sandbox']['max'],
          ]);

          // Increment total processed item values. Will be used in finished
          // callback.
          $context['results']['processed'] = $context['sandbox']['progress'];
        }
      }
    }

    // If not finished all tasks, we count percentage of process. 1 = 100%.
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  /**
   * Process single item.
   *
   * @param int|string $nid
   *   An id of Node.
   * @param \Drupal\Core\Datetime\DrupalDateTime $date
   *   An object with new published date.
   */
  public static function processItem($nid, DrupalDateTime $date) {

    $node = Node::load($nid);
    $node->set('title', $node->type->entity->label() . ' Title for Node ' . $nid);
    $node->setCreatedTime($date->getTimestamp())->save();
    $node->save();
    \Drupal::logger('batch_examples')->info("Node with nid @nid saved!\n", ['@nid' => $node->id()]);

  }

  /**
   * Finished callback for batch.
   */
  public static function finished($success, $results, $operations) {
    $message = t('Number of nodes affected by batch: @count', [
      '@count' => $results['processed'],
    ]);

    \Drupal::messenger()->addStatus($message);
  }

}
