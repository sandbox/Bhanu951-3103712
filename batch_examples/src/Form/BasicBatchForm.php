<?php

namespace Drupal\batch_examples\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\NodeInterface;
use Drupal\Core\Datetime\DateFormatterInterface;

/**
 * Implements an Basic Batch Example form.
 */
class BasicBatchForm extends FormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Node Storage Interface.
   *
   * @var \Drupal\node\NodeStorageInterface
   */
  protected $nodeTypeStorage;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Basic Batch Form constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, DateFormatterInterface $date_formatter) {
    $this->entityTypeManager = $entity_type_manager;
    $this->nodeTypeStorage = $this->entityTypeManager->getStorage('node_type');
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'batch_examples_basic_batch_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['help'] = [
      '#markup' => $this->t('This form set entered publication date to all content of selected type.'),
    ];

    $nodeTypes = $this->nodeTypeStorage->loadMultiple();

    $nodeTypesList = [];
    if (!empty($nodeTypes)) {

      foreach ($nodeTypes as $nodeType) {

        $nodeTypesList[$nodeType->id()] = $nodeType->label();

      }

    }
    $form['node_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Content type'),
      '#options' => $nodeTypesList,
      '#required' => TRUE,
    ];

    $form['date'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Publication date'),
      '#required' => TRUE,
      '#default_value' => new DrupalDateTime('now'),
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['run'] = [
      '#type' => 'submit',
      '#value' => $this->t('Run batch'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->messenger()->addStatus($this->t('The form has been submitted with the following details.'));

    $nids = $this->getNodes($form_state->getValue(['node_type']));
    $date = $form_state->getValue('date');
    if (!empty($nids)) {

      $chunk_nid = array_chunk($nids, 20);

      foreach ($chunk_nid as $data) {
        $operations[] = ['\Drupal\batch_examples\Batch\SimpleBatchExample::updateNodes', [
          $data,
          $date,
        ],
        ];
      }
      $batch = [
        'operations' => $operations,
        'finished' => '\Drupal\batch_examples\Batch\SimpleBatchExample::nodesUpdateFinishedCallback',
          // We can define custom messages instead of the default ones.
        'title' => $this->t('Updating Nodes'),
        'progress_message' => $this->t('Completed @current of @total Records.... Estimation time: @estimate; @elapsed taken till now'),
        'error_message' => $this->t('error occured at @current'),
      ];
      batch_set($batch);

    }
    // Redirect to the Basic Form Page.
    $form_state->setRedirect('batch_examples.basic_batch_form');

  }

  /**
   * Load all nids for specific type.
   *
   * @return array
   *   An array with nids.
   */
  public function getNodes($type) {

    $storage = $this->entityTypeManager->getStorage('node');

    $query = $storage->getQuery();
    $query->condition('status', NodeInterface::PUBLISHED);
    $query->condition('type', $type);
    $query->accessCheck(TRUE);
    $nids = $query->execute();

    return $nids;
  }

}
