<?php

namespace Drupal\ckeditor_plugin_examples\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "Text Transform" plugin.
 *
 * @CKEditorPlugin(
 *   id = "ckeditor_plugin_examples_texttransform",
 *   label = @Translation("Text Transform"),
 *   module = "ckeditor_plugin_examples"
 * )
 */
class CkeditorTextTransform extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * Returns texttransform plugin path relative to drupal root.
   *
   * @return string
   *   Relative path to the plugin folder.
   */
  protected function getPluginPath() {
    $module_handler = \Drupal::service('module_handler');
    $module_path = $module_handler->getModule('ckeditor_plugin_examples')->getPath();
    return $module_path . '/libraries/texttransform';
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return $this->getPluginPath() . '/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'TransformTextSwitcher' => [
        'label' => $this->t('Transform Text Switcher'),
        'image' => $this->getPluginPath() . '/images/transformSwitcher.png',
      ],
      'TransformTextToUppercase' => [
        'label' => $this->t('Transform Text to Uppercase'),
        'image' => $this->getPluginPath() . '/images/transformToUpper.png',
      ],
      'TransformTextToLowercase' => [
        'label' => $this->t('Transform Text to Lowercase'),
        'image' => $this->getPluginPath() . '/images/transformToLower.png',
      ],
      'TransformTextCapitalize' => [
        'label' => $this->t('Capitalize Text'),
        'image' => $this->getPluginPath() . '/images/transformCapitalize.png',
      ],
    ];
  }

}
