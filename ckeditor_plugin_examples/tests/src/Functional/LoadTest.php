<?php

namespace Drupal\Tests\ckeditor_plugin_examples\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test class for ckeditor_plugin_examples module.
 *
 * @group ckeditor_plugin_examples
 */
class LoadTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected $defaultProfile = 'standard';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    // Modules for core functionality.
    'node',
    'views',
    'user',
    'config',
    'ckeditor_plugin_examples',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {

    // Make sure to complete the normal setup steps first.
    parent::setUp();

    // Set the front page to "/node".
    \Drupal::configFactory()
      ->getEditable('system.site')
      ->set('page.front', '/node')
      ->save(TRUE);

  }

  /**
   * Tests the home page loads for Anonymous users.
   */
  public function testHomePage() {

    // Test homepage.
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);

    // Minimal homepage title.
    // $this->assertSession()->pageTextContains('Log in');.
    $this->assertSession()->pageTextContains('Welcome to Drupal');

  }

  /**
   * Test callback.
   */
  public function testAdminPage() {

    $this->drupalLogout();
    $admin_user = $this->drupalCreateUser(['access administration pages']);
    $this->drupalLogin($admin_user);

    $this->assertSession()->pageTextContains($admin_user->getDisplayName());
    $this->drupalGet('admin');
    $this->assertSession()->elementExists('xpath', '//h1[text() = "Administration"]');

  }

  /**
   * Tests the admin page response.
   */
  public function testAdminConfigPage() {

    // Logout authenticated, try to access the page as admin user.
    $this->drupalLogout();
    $admin_user = $this->drupalCreateUser(['access administration pages']);
    $this->drupalLogin($admin_user);
    // $this->drupalLogin($this->adminUser);
    $this->drupalGet('/admin/config');
    // Make sure we don't get a 403 code.
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalLogout();
    $normal_user = $this->drupalCreateUser(['access content']);
    $this->drupalLogin($normal_user);
    $this->drupalGet('/admin/config');
    // Make sure we don't get a 200 code.
    $this->assertSession()->statusCodeEquals(403);

  }

  /**
   * Tests the module unistall.
   */
  public function testModuleUninstall() {

    $assert_session = $this->assertSession();

    // Uninstall the module.
    $this->drupalLogin($this->rootUser);
    $this->drupalGet('/admin/modules/uninstall');
    $assert_session->pageTextContains('Ckeditor Plugin Examples');
    $this->submitForm(['uninstall[ckeditor_plugin_examples]' => TRUE], 'Uninstall');
    $this->submitForm([], 'Uninstall');
    $assert_session->pageTextContains('The selected modules have been uninstalled.');
    $assert_session->pageTextNotContains('Ckeditor Plugin Examples');
    $this->drupalLogout();

  }

  /**
   * Tests reinstalling after being uninstalled.
   */
  public function testReinstallAfterUninstall() {

    $this->drupalLogin($this->rootUser);

    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    // Uninstall the ckeditor_plugin_examples module.
    $this->container->get('module_installer')->uninstall(['ckeditor_plugin_examples'], FALSE);

    $this->drupalGet('/admin/modules');
    $page->checkField('modules[ckeditor_plugin_examples][enable]');
    $page->pressButton('Install');
    $assert_session->pageTextNotContains('could not be moved/copied because a file by that name already exists in the destination directory');
    $assert_session->pageTextContains('Module Ckeditor Plugin Examples has been enabled');

  }

  /**
   * Tests the block placement.
   */
  public function testBlockPlacement() {

    // Warm the page cache.
    $this->drupalGet('');
    $this->assertSession()->pageTextNotContains('Powered by Drupal');
    $this->assertSession()->responseHeaderNotContains('X-Drupal-Cache-Tags', 'config:block_list');

    // Install the block module, and place the "Powered by Drupal" block.
    $this->container->get('module_installer')->install(['block', 'shortcut']);
    $this->rebuildContainer();
    $this->drupalPlaceBlock('system_powered_by_block');

    // Check the same page, block.module's hook_install() should have
    // invalidated the 'rendered' cache tag to make blocks show up.
    $this->drupalGet('');
    $this->assertSession()->responseHeaderContains('X-Drupal-Cache-Tags', 'config:block_list');
    $this->assertSession()->pageTextContains('Powered by Drupal');

  }

}
