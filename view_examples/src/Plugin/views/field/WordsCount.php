<?php

namespace Drupal\view_examples\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Database\Connection;

/**
 * Field handler to count the words in Body Field.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("node_body_field_words_count")
 */
class WordsCount extends FieldPluginBase {


  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * Database service.
   *
   * @var \Drupal\Core\Database\Connection
   */

  protected $database;

  /**
   * Constructor for View Field.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The id of the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger interface.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger interface.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerChannelFactoryInterface $logger, MessengerInterface $messenger, Connection $connection) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->logger = $logger->get('view_examples');
    $this->messenger = $messenger;
    $this->database = $connection;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // We don't need to modify query for this particular example.
    // Read https://niklan.net/blog/156 .
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['node_body_field_words_count'] = ['default' => 'minutes'];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $units = [
      'hours' => $this->t('Hours'),
      'minutes' => $this->t('Minutes'),
      'seconds' => $this->t('Seconds'),
    ];

    $form['node_body_field_words_count'] = [
      '#type' => 'select',
      '#options' => $units,
      '#required' => TRUE,
      '#empty_option' => $this->t('Choose a Time Unit'),
      '#title' => $this->t('Select Reading Time Unit'),
      '#description' => $this->t('Displays the count of words present in Body Field and its reading time.'),
      '#default_value' => $this->options['node_body_field_words_count'],
    ];
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {

    $measuringUnit = $this->options['node_body_field_words_count'];

    // Get Node Object.
    $node = $values->_entity;

    $bodyField = [];

    if ($node->hasField('body')) {
      // Get Body Field Value.
      $bodyField = $node->body->value;
      $wordsCount = str_word_count(strip_tags($bodyField));

    }
    else {

      $wordsCount = $this->t("Body Field Doesn't exist.");
    }

    $readingTime = $this->readingTime($wordsCount, $measuringUnit);

    $message = $this->t('Words Count is  : @wordsCount words and estimated reading time is : @readingTime %measuringUnit',
          [
            '@wordsCount' => $wordsCount,
            '@readingTime' => $readingTime,
            '%measuringUnit' => $measuringUnit,
          ]
      );

    $this->logger->notice($message);

    $element = [
      '#type' => 'html_tag',
      '#tag' => 'h2',
      '#cache' => [
        'max-age' => 3600,
      ],
      '#value' => $message,
      '#attributes' => [
        'class' => ['words-count'],
      ],
      '#attached' => [
        'library' => [
          'view_examples/styles',
        ],
      ],
    ];

    return $element;
  }

  /**
   * Reading Time.
   *
   * Calculate an approximate reading-time for a post.
   *
   * @param string $wordsCount
   *   The content to be measured.
   * @param string $measuringUnit
   *   The measuring unit for reading time.
   *
   * @return string
   *   Reading-time .
   */
  public static function readingTime($wordsCount, $measuringUnit) {

    switch ($measuringUnit) {

      case 'hours':
        $time = (($wordsCount / 200) / 60);
        break;

      case 'minutes':
        $time = ($wordsCount / 200);
        break;

      case 'seconds':
        $time = (($wordsCount / 200) * 60);
        break;

      default:
        $time = ($wordsCount / 200);
        break;

    }

    return $time;
  }

}
