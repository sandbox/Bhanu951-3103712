<?php

namespace Drupal\rest_examples\Plugin\rest\resource;

use Drupal\node\Entity\Node;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\Core\Session\AccountProxyInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Provides a resource to get node details with nid.
 *
 * @RestResource(
 *   id = "rest_resource_get_example",
 *   label = @Translation("Rest Resource Get Example"),
 *   uri_paths = {
 *     "canonical" = "/rest/api/get/node/{id}"
 *   }
 * )
 */
class RestResourceGetExample extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, AccountProxyInterface $current_user) {

    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest_examples'),
      $container->get('current_user')
    );
  }

  /**
   * Responds to GET requests.
   *
   * Returns the details of the node.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get($nodeId) {

    // Use current user after pass authentication to validate access.
    if (!$this->currentUser->hasPermission('administer rest examples')) {

      // Display the default access denied page.
      throw new AccessDeniedHttpException('Access Denied.');
    }

    if ($nodeId) {
      // Load node.
      $node = Node::load($nodeId);

      if (is_object($node)) {

        $data = [
          'nid' => $node->id(),
          'type' => $node->getType(),
          'title' => $node->getTitle(),
          'body' => $node->body->value,
          'created_time' => $node->getCreatedTime(),
          'promoted' => $node->isPromoted(),
          'sticky' => $node->isSticky(),
        ];

        $response = new ResourceResponse($data, 200);
        $response->addCacheableDependency($node);

        return $response;
      }

      $this->logger->warning($this->t("Node doesn't exist for NID : @nid", ['@nid' => $nodeId]));

      return new ResourceResponse("Node doesn't exist", 400);
    }

    return new ResourceResponse("Node Id is required", 400);
  }

}
