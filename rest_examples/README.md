Curl Syntax to make POST request :

```shell
curl \
    --user uname:password \
    --header 'Accept: application/json' \
    --header 'Content-type: application/json' \
    --request POST http://example.com/rest/api/post/node-create \
    --data-binary @post_data.json
```

Curl Syntax to make GET request :

```shell
curl \
    --user uname:password \
    --header 'Accept: application/json' \
    --header 'Content-type: application/json' \
    --request GET http://example.com/rest/api/get/node/NID
```
