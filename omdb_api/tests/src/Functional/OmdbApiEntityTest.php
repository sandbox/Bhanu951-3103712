<?php

namespace Drupal\Tests\omdb_api\Functional;

use Drupal\Component\Utility\Html;
use Drupal\Tests\BrowserTestBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Tests\omdb_api\Traits\OmdbApiEntityCreationTrait;
use Drupal\core\Url;

/**
 * Class to Test OMDB API Entity.
 *
 * @group omdb_api
 */
class OmdbApiEntityTest extends BrowserTestBase {

  use StringTranslationTrait;
  use OmdbApiEntityCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'bartik';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'block',
    'path',
    'options',
    'taxonomy',
    'omdb_api',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {

    parent::setUp();

    $this->drupalPlaceBlock('page_title_block');

  }

  /**
   * Tests Homepage after enabling OMDB API Module.
   */
  public function testHomepage() {

    // Test homepage.
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);

    // Minimal homepage title.
    $this->assertSession()->pageTextContains('Log in');

  }

  /**
   * Tests OMDB API Entity Creation.
   */
  public function testOmdbApiEntityCreation() {

    $this->drupalLogin($this->rootUser);
    $this->drupalGet('/admin/content/omdb-api/list');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('OMDB API Contents');
    $this->assertSession()->pageTextContains('Total OMDB API Count: 0');
    // Search Form URL.
    $settings_form_url = Url::fromRoute('entity.omdb_api_credentials.settings_form', [], [])->toString();
    $this->assertSession()->pageTextContains('OMDB API Entity credentials can be configured on OMDB API Credentials Settings Form page.');
    $this->clickLink('OMDB API Credentials Settings Form page');
    $this->assertSession()->addressEquals($settings_form_url);

    // Generate OMDB API Entity content.
    $edit = [
      'imdb_title[0][value]' => '!SimpleTest! ' . $this->randomMachineName(20),
      'imdb_id[0][value]' => 'tt' . mt_rand(1000000, 9999999),
    ];

    // Create the OMDB API Entity with sample data.
    $this->drupalGet('content/omdb-api/add');
    $this->assertSession()->responseContains("The IMDB Title of the Movie or Series.");
    $this->submitForm($edit, 'Save');

    // Make sure OMDB API Entity is present in the database.
    $omdb_api_entity = $this->drupalGetOmdbApiEntityByImdbTitle($edit['imdb_title[0][value]']);
    $this->assertNotNull($omdb_api_entity, 'OMDB API Entity created and found in database');
    $this->assertSession()->responseContains(Html::escape($edit['imdb_title[0][value]']));
    $this->drupalGet("content/omdb-api/" . $omdb_api_entity->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains($edit['imdb_id[0][value]']);
    $this->assertSession()->responseContains(Html::escape($edit['imdb_title[0][value]']));

    // Check Revisions Access.
    $this->drupalGet("content/omdb-api/" . $omdb_api_entity->id() . "/revisions");
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains($this->t("Revisions for @title", ['@title' => $edit['imdb_title[0][value]']]));
    $this->assertSession()->responseContains('Current revision');

    $values = [
      'revision_log_message' => $this->randomMachineName(20),
      'revision_creation_time' => \Drupal::time()->getRequestTime(),
    ];
    $omdb_api_entity_revision = $this->drupalCreateOmdbApiEntityRevision($omdb_api_entity->id(), $values, TRUE);

    // Check One of the Revisions Detailed View.
    $this->drupalGet("content/omdb-api/" . $omdb_api_entity->id() . "/revisions/" . $omdb_api_entity_revision->getLoadedRevisionId() . "/view");
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains($edit['imdb_id[0][value]']);
    $revision_date = $this->container->get('date.formatter')->format($omdb_api_entity_revision->getRevisionCreationTime(), 'short');
    $this->assertSession()->responseContains($revision_date);
    $this->assertEquals($values['revision_creation_time'], $omdb_api_entity_revision->getRevisionCreationTime());
    $this->assertEquals($values['revision_log_message'], $omdb_api_entity_revision->getRevisionLogMessage());

    // Check the Revisions Revert Access.
    $this->drupalGet("content/omdb-api/" . $omdb_api_entity->id() . "/revisions/" . $omdb_api_entity->getLoadedRevisionId() . "/revert");
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains("Are you sure you want to revert to the revision from");
    $this->assertSession()->responseContains($revision_date);
    $this->assertSession()->buttonExists('Revert');
    $this->assertSession()->responseContains('Cancel');
    $this->clickLink('Cancel');

    // $this->submitForm([], $this->t('Revert'));
    $this->assertSession()->addressEquals("content/omdb-api/" . $omdb_api_entity->id() . "/revisions");

    $this->drupalGet('/admin/content/omdb-api/list');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('OMDB API Contents');
    // $this->assertSession()->pageTextNotContains(
    // 'There are no omdb apis yet.');
    // Generate OMDB API Entity content.
    $edit1 = [
      'imdb_title' => '!SimpleTest! ' . $this->randomMachineName(20),
      'imdb_id' => 'tt' . mt_rand(1000000, 9999999),
    ];
    // Create the OMDB API Entity with sample data.
    $omdb_api_entity1 = $this->drupalCreateOmdbApiEntity($edit1);

    // Make sure OMDB API Entity is present in the database.
    $omdb_api_entity1 = $this->drupalGetOmdbApiEntityByImdbTitle($edit1['imdb_title']);
    $this->assertNotNull($omdb_api_entity1, 'OMDB API Entity created and found in database');
    $this->drupalGet("content/omdb-api/" . $omdb_api_entity1->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains($edit1['imdb_id']);
    $this->assertSession()->responseContains(Html::escape($edit1['imdb_title']));
    // Check Revisions Access.
    $this->drupalGet("content/omdb-api/" . $omdb_api_entity1->id() . "/revisions");
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains($this->t("Revisions for @title", ['@title' => $edit1['imdb_title']]));
    $this->assertSession()->responseContains('Current revision');

    $values1 = [
      'revision_log_message' => $this->randomMachineName(20),
      'revision_creation_time' => \Drupal::time()->getRequestTime(),
    ];
    $omdb_api_entity_revision1 = $this->drupalCreateOmdbApiEntityRevision($omdb_api_entity1->id(), $values1, TRUE);

    // Check One of the Revisions Detailed View.
    $this->drupalGet("content/omdb-api/" . $omdb_api_entity1->id() . "/revisions/" . $omdb_api_entity1->getLoadedRevisionId() . "/view");
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains(Html::escape($edit1['imdb_title']));
    $this->assertSession()->responseContains($edit1['imdb_id']);
    $revision_date1 = $this->container->get('date.formatter')->format($omdb_api_entity1->getRevisionCreationTime(), 'short');
    $this->assertSession()->responseContains($revision_date1);
    $this->assertEquals($values1['revision_creation_time'], $omdb_api_entity_revision1->getRevisionCreationTime());
    $this->assertEquals($values1['revision_log_message'], $omdb_api_entity_revision1->getRevisionLogMessage());

    // Check the Revisions Revert Access.
    $this->drupalGet("content/omdb-api/" . $omdb_api_entity1->id() . "/revisions/" . $omdb_api_entity1->getLoadedRevisionId() . "/revert");
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains("Are you sure you want to revert to the revision from");
    $this->assertSession()->responseContains($revision_date1);
    $this->assertSession()->buttonExists('Revert');
    $this->assertSession()->responseContains('Cancel');
    $this->clickLink('Cancel');
    $this->assertSession()->addressEquals("content/omdb-api/" . $omdb_api_entity1->id() . "/revisions");

    // $this->submitForm([], $this->t('Revert'));
    // $this->assertSession()->addressEquals("content/omdb-api/"
    // . $omdb_api_entity1->id() . "/revisions");
  }

}
