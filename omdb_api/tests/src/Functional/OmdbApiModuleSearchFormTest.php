<?php

namespace Drupal\Tests\omdb_api\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\core\Url;

/**
 * OMDB API Module Search Form Test.
 *
 * @group omdb_api
 */
class OmdbApiModuleSearchFormTest extends BrowserTestBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'bartik';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'content_translation',
    'devel',
    'devel_generate',
    'language',
    'block',
    'views',
    'node',
    'path',
    'path_alias',
    'taxonomy',
    'omdb_api',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalPlaceBlock('page_title_block');
  }

  /**
   * Test OMDB API Module Search Form Access.
   */
  public function testOmdbApiModuleSearchFormAccess() {

    // Search Form URL.
    $search_form_url = Url::fromRoute('omdb_api.search_form', [], [])->toString();

    // Test OMDB API Module Search Form Access with Test User.
    $test_user = $this->drupalCreateUser(['administer omdb api configuration']);
    $this->drupalLogin($test_user);
    $this->drupalGet($search_form_url);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Form to Search OMDB API');
    $this->assertSession()->pageTextContains('OMDB Movie Search');
    $this->assertSession()->pageTextContains('Search By');
    $this->assertSession()->pageTextContains('Search Movies or Series');
    $this->assertSession()->fieldExists('search_string');
    $this->assertSession()->fieldExists('api_end_point');
    $this->assertSession()->fieldExists('search_by');
    $this->assertSession()->buttonExists('Search');
    $this->drupalLogout();

    // Test OMDB API Module Search Form Access with Admin User
    // without required permission to access OMDB API Module Settings Form.
    $admin_user = $this->drupalCreateUser(['administer site configuration']);
    $this->drupalLogin($admin_user);
    $this->drupalGet($search_form_url);
    $this->assertSession()->statusCodeEquals(403);
    $this->assertSession()->pageTextContains('Access denied');
    $this->assertSession()->pageTextContains('You are not authorized to access this page.');
    $this->drupalLogout();

    // Test OMDB API Module Search Form Access with Annon User.
    $annon_user = $this->drupalCreateUser(['access content']);
    $this->drupalLogin($annon_user);
    $this->drupalGet($search_form_url);
    $this->assertSession()->statusCodeEquals(403);
    $this->assertSession()->pageTextContains('Access denied');
    $this->assertSession()->pageTextContains('You are not authorized to access this page.');

  }

  /**
   * Test OMDB API Module Search Form Submit.
   */
  public function testOmdbApiModuleSearchFormSubmit() {

    // Search Form URL.
    $search_form_url = Url::fromRoute('omdb_api.search_form', [], [])->toString();
    $api_end_point = ['movie', 'series'];
    $search_by = ['name', 'id', 'search-parameter'];
    $search_string = [
      $this->generateRandomString(random_int(0, 99999)),
      random_int(0, 99999),
    ];

    $data_set = [
      "0" => [
        'search_string' => $search_string[mt_rand(0, count($search_string) - 1)],
        'api_end_point' => $api_end_point[mt_rand(0, count($api_end_point) - 1)],
        'search_by' => $search_by[mt_rand(0, count($search_by) - 1)],
      ],
      "1" => [
        'search_string' => $this->generateRandomString(5),
        'api_end_point' => 'series',
        'search_by' => 'name',
      ],
      "2" => [
        'search_string' => random_int(0, 9999),
        'api_end_point' => 'series',
        'search_by' => 'id',
      ],
      "3" => [
        'search_string' => $this->generateRandomString(5),
        'api_end_point' => 'movie',
        'search_by' => 'name',
      ],
      "4" => [
        'search_string' => random_int(0, 999),
        'api_end_point' => 'movie',
        'search_by' => 'id',
      ],
      "5" => [
        'search_string' => $this->generateRandomString(5),
        'api_end_point' => 'movie',
        'search_by' => 'search-parameter',
      ],
      "6" => [
        'search_string' => $this->generateRandomString(5),
        'api_end_point' => 'series',
        'search_by' => 'search-parameter',
      ],
      "7" => [
        'search_string' => $this->generateRandomString(5),
        'api_end_point' => 'movie',
        'search_by' => 'id',
      ],
      "8" => [
        'search_string' => $this->generateRandomString(5),
        'api_end_point' => 'series',
        'search_by' => 'id',
      ],
      "9" => [
        'search_string' => $this->generateRandomString(5),
        'api_end_point' => 'movie',
        'search_by' => 'name',
      ],
    ];

    // Test OMDB API Module Search Form Access with Test User.
    $test_user = $this->drupalCreateUser(['administer omdb api configuration']);
    $this->drupalLogin($test_user);
    $this->drupalGet($search_form_url);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Form to Search OMDB API');
    $this->assertSession()->pageTextContains('OMDB Movie Search');
    $this->assertSession()->pageTextContains('Search By');
    $this->assertSession()->pageTextContains('Search Movies or Series');
    $this->assertSession()->fieldExists('search_string');
    $this->assertSession()->fieldExists('api_end_point');
    $this->assertSession()->fieldExists('search_by');
    $this->assertSession()->buttonExists('Search');

    // Create first set of form data array item to search by Movie Name.
    $data_set1 = [];
    $data_set1 = [
      'search_string' => $this->generateRandomString(5),
      'api_end_point' => 'series',
      'search_by' => 'name',
    ];
    $this->submitForm($data_set1, 'Search');
    $this->assertSession()->pageTextContains('Click here to search another movie or series.');
    $this->assertSession()->pageTextContains($this->t('OMDB API Response for : @data', ['@data' => $data_set1['search_string']]));

    $this->drupalGet($search_form_url);

    // Create second set of form data array item to search.
    $data_set2 = [];
    $data_set2 = [
      'search_string' => $this->generateRandomString(5),
      'api_end_point' => 'movie',
      'search_by' => 'name',
    ];
    $this->submitForm($data_set2, 'Search');
    $this->assertSession()->pageTextContains($this->t('OMDB API Response for : @data', ['@data' => $data_set2['search_string']]));
    // A message to go back to search form should appear.
    $this->assertSession()->pageTextContains('Click here to search another movie or series.');
    $this->clickLink('here');
    $this->assertSession()->addressEquals($search_form_url);

    $this->drupalGet($search_form_url);

    // Create third set of form data array item to search.
    $data_set3 = [];
    $data_set3 = [
      'search_string' => random_int(100, 999999),
      'api_end_point' => 'series',
      'search_by' => 'id',
    ];
    $this->submitForm($data_set3, 'Search');
    $this->assertSession()->pageTextContains($this->t('OMDB API Response for : @data', ['@data' => $data_set3['search_string']]));
    // A message to go back to search form should appear.
    $this->assertSession()->pageTextContains('Click here to search another movie or series.');
    $this->clickLink('here');
    $this->assertSession()->addressEquals($search_form_url);

    $this->drupalGet($search_form_url);

    // Create fourth set of form data array item to search.
    $data_set4 = [];
    $data_set4 = [
      'search_string' => random_int(0, 999),
      'api_end_point' => 'series',
      'search_by' => 'search-parameter',
    ];
    $this->submitForm($data_set4, 'Search');
    $this->assertSession()->pageTextContains('Search string should be at least 4 characters.');
    $this->assertSession()->addressEquals($search_form_url);

    $this->drupalGet($search_form_url);

    // Create fifth set of form data array item to search.
    $data_set5 = [];
    $data_set5 = [
      'search_string' => $this->generateRandomString(3),
      'api_end_point' => 'movie',
      'search_by' => 'search-parameter',
    ];
    $this->submitForm($data_set5, 'Search');
    $this->assertSession()->pageTextContains('Search string should be at least 4 characters.');
    $this->assertSession()->addressEquals($search_form_url);

    // Create sixth set of form data array item to search.
    $data_set6 = [];
    $data_set6 = $data_set[mt_rand(0, count($data_set) - 1)];
    $this->submitForm($data_set6, 'Search');

    if (strlen($data_set6['search_string']) < 4) {
      $this->assertSession()->pageTextContains('Search string should be at least 4 characters.');
      $this->assertSession()->addressEquals($search_form_url);
    }
    else {
      $this->assertSession()->pageTextContains($this->t('OMDB API Response for : @data', ['@data' => $data_set6['search_string']]));
      // A message to go back to search form should appear.
      $this->assertSession()->pageTextContains('Click here to search another movie or series.');
      $this->clickLink('here');
      $this->assertSession()->addressEquals($search_form_url);
    }

  }

  /**
   * Method to generate Random String.
   */
  private function generateRandomString($length = 10) {

    $pieces = [];
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $max = mb_strlen($characters, '8bit') - 1;
    for ($i = 0; $i < $length; ++$i) {
      $pieces[] = $characters[random_int(0, $max)];
    }
    return implode('', $pieces);

  }

}
