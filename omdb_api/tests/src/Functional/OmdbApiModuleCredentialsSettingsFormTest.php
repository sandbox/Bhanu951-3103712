<?php

namespace Drupal\Tests\omdb_api\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * OMDB API Module Credentials Settings Form Test.
 *
 * @group omdb_api
 */
class OmdbApiModuleCredentialsSettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'bartik';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['node', 'block', 'omdb_api'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalPlaceBlock('page_title_block');

  }

  /**
   * Test OMDB API Module Settings Form Access.
   */
  public function testOmdbApiModuleSettingsFormAccess() {

    // Test OMDB API Module Settings Form Access with Test User.
    $test_user = $this->drupalCreateUser(['administer omdb api entities settings']);
    $this->drupalLogin($test_user);
    $this->drupalGet('admin/structure/omdb-api/credentials-settings');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('OMDB API Credentials Settings Form');
    $this->assertSession()->pageTextContains('OMDB API URL');
    $this->assertSession()->pageTextContains('OMDB API Key');
    $this->assertSession()->pageTextContains('OMDB API Data Max Cache Duration');
    $this->assertSession()->pageTextContains('OMDB API Credentials Settings Form is used to store OMDB API Credentials.');
    $this->assertSession()->fieldExists('omdb_api_url');
    $this->assertSession()->fieldExists('omdb_api_key');
    $this->assertSession()->fieldExists('omdb_api_cache_max_lifetime');
    $this->assertSession()->buttonExists('Save configuration');
    $this->drupalLogout();

    // Test OMDB API Module Settings Form Access with Admin User
    // without required permission to access OMDB API Module Settings Form.
    $admin_user = $this->drupalCreateUser(['administer site configuration']);
    $this->drupalLogin($admin_user);
    $this->drupalGet('admin/structure/omdb-api/credentials-settings');
    $this->assertSession()->statusCodeEquals(403);
    $this->assertSession()->pageTextContains('Access denied');
    $this->assertSession()->pageTextContains('You are not authorized to access this page.');
    $this->drupalLogout();

    // Test OMDB API Module Settings Form Access with Annon User.
    $annon_user = $this->drupalCreateUser(['access content']);
    $this->drupalLogin($annon_user);
    $this->drupalGet('admin/structure/omdb-api/credentials-settings');
    $this->assertSession()->statusCodeEquals(403);
    $this->assertSession()->pageTextContains('Access denied');
    $this->assertSession()->pageTextContains('You are not authorized to access this page.');

  }

  /**
   * Test Example Config Form Submit.
   */
  public function testOmdbApiModuleSettingsFormSubmit() {

    $valid_urls = [
      'http://example-server',
      'http://example.com',
      'http://example.com/',
      'http://subdomain.example.com/path/?parameter1=value1&parameter2=value2',
      'random-protocol://example.com',
      'http://example.com:80',
      'http://example.com?no-path-separator',
      'http://example.com/pa%20th/',
      'ftp://example.org/resource.txt',
      'file://../../../relative/path/needs/protocol/resource.txt',
      'http://example.com/#one-fragment',
      'http://example.edu:8080#one-fragment',
    ];

    $invalid_urls = [
      'only-text',
      'http//missing.colon.example.com/path/?parameter1=value1&parameter2=value2',
      'missing.protocol.example.com/path/',
      'http://example.com\\bad-separator',
      'http://example.com|bad-separator',
      'ht tp://example.com',
      'http://exampl e.com',
      'http://example.com/pa th/',
      '../../../relative/path/needs/protocol/resource.txt',
      'http://example.edu:portMustBeANumber#one-fragment',
      // Need to check RFC for this URL.
      // 'http://example.com/#two-fragments#not-allowed'.
    ];

    // Test OMDB API Module Settings Form Access with Test User.
    $test_user = $this->drupalCreateUser(['administer omdb api entities settings']);
    $this->drupalLogin($test_user);
    $this->drupalGet('admin/structure/omdb-api/credentials-settings');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('OMDB API Credentials Settings Form');
    $this->assertSession()->pageTextContains('OMDB API URL');
    $this->assertSession()->pageTextContains('OMDB API Key');
    $this->assertSession()->pageTextContains('OMDB API Data Max Cache Duration');
    $this->assertSession()->pageTextContains('OMDB API Credentials Settings Form is used to store OMDB API Credentials.');
    $this->assertSession()->fieldExists('omdb_api_url');
    $this->assertSession()->fieldExists('omdb_api_key');
    $this->assertSession()->fieldExists('omdb_api_cache_max_lifetime');
    $this->assertSession()->buttonExists('Save configuration');

    // Create first set of invalid form data array item.
    $data_set1 = [];
    $index1 = mt_rand(0, count($invalid_urls) - 1);
    $data_set1 = [
      'omdb_api_url' => $invalid_urls[$index1],
      'omdb_api_key' => $this->randomString(5),
      'omdb_api_cache_max_lifetime' => $this->randomString(5),
    ];
    $this->submitForm($data_set1, 'Save configuration');
    $this->assertSession()->pageTextContains("The OMDB API URL Entered is not valid.");
    $this->assertSession()->pageTextContains("The OMDB API Key Entered is not valid.");
    $this->assertSession()->pageTextContains("The OMDB API Data Max Cache Duration Value Entered is not valid.");
    $this->assertSession()->pageTextNotContains("The configuration options have been saved.");
    $this->assertSession()->addressEquals('admin/structure/omdb-api/credentials-settings');

    // Create second set of invalid form data array item.
    $data_set2 = [];
    $index2 = mt_rand(0, count($valid_urls) - 1);
    $data_set2 = [
      'omdb_api_url' => $valid_urls[$index2],
      'omdb_api_key' => $this->randomString(5),
      'omdb_api_cache_max_lifetime' => $this->randomString(5),
    ];
    $this->submitForm($data_set2, 'Save configuration');
    $this->assertSession()->pageTextContains("The OMDB API Key Entered is not valid.");
    $this->assertSession()->pageTextContains("The OMDB API Data Max Cache Duration Value Entered is not valid.");
    $this->assertSession()->pageTextNotContains("The configuration options have been saved.");
    $this->assertSession()->addressEquals('admin/structure/omdb-api/credentials-settings');

    // Create third set of invalid form data array item.
    $data_set3 = [];
    $index3 = mt_rand(0, count($valid_urls) - 1);
    $data_set3 = [
      'omdb_api_url' => $valid_urls[$index3],
      'omdb_api_key' => $this->randomString(8),
      'omdb_api_cache_max_lifetime' => $this->randomString(5),
    ];
    $this->submitForm($data_set3, 'Save configuration');
    $this->assertSession()->pageTextContains("The OMDB API Data Max Cache Duration Value Entered is not valid.");
    $this->assertSession()->pageTextNotContains("The configuration options have been saved.");
    $this->assertSession()->addressEquals('admin/structure/omdb-api/credentials-settings');

    // Create fourth set of invalid form data array item.
    $data_set4 = [];
    $index4 = mt_rand(0, count($valid_urls) - 1);
    $data_set4 = [
      'omdb_api_url' => $valid_urls[$index4],
      'omdb_api_key' => $this->randomString(40),
      'omdb_api_cache_max_lifetime' => rand('60', '25000'),
    ];
    $this->submitForm($data_set4, 'Save configuration');
    $this->assertSession()->pageTextContains("The OMDB API Key Entered is not valid.");
    $this->assertSession()->pageTextNotContains("The configuration options have been saved.");
    $this->assertSession()->addressEquals('admin/structure/omdb-api/credentials-settings');

    // Create fifth set of invalid form data array item.
    $data_set5 = [];
    $index5 = mt_rand(0, count($invalid_urls) - 1);
    $data_set5 = [
      'omdb_api_url' => $invalid_urls[$index5],
      'omdb_api_key' => $this->randomString(40),
      'omdb_api_cache_max_lifetime' => rand('60', '25000'),
    ];
    $this->submitForm($data_set5, 'Save configuration');
    $this->assertSession()->pageTextContains("The OMDB API URL Entered is not valid.");
    $this->assertSession()->pageTextContains("The OMDB API Key Entered is not valid.");
    $this->assertSession()->pageTextNotContains("The configuration options have been saved.");
    $this->assertSession()->addressEquals('admin/structure/omdb-api/credentials-settings');

    // Create sixth set of form data array item.
    $data_set6 = [];
    $index6 = mt_rand(0, count($invalid_urls) - 1);
    $data_set6 = [
      'omdb_api_url' => $valid_urls[$index6],
      'omdb_api_key' => $this->randomString(8),
      'omdb_api_cache_max_lifetime' => rand('60', '25000'),
    ];
    $this->submitForm($data_set6, 'Save configuration');
    $this->assertSession()->pageTextContains("The configuration options have been saved.");
    $this->assertSession()->addressEquals('admin/structure/omdb-api/credentials-settings');

    // Get the config factory service.
    $config_factory = $this->container->get('config.factory');

    // Get omdb_api settings variables from config.
    $omdb_api_url = $config_factory->get('omdb_api_credentials.settings')->get('omdb_api_url');
    $omdb_api_key = $config_factory->get('omdb_api_credentials.settings')->get('omdb_api_key');
    $omdb_api_cache_max_lifetime = $config_factory->get('omdb_api_credentials.settings')->get('omdb_api_cache_max_lifetime');

    // Verify the config values are stored.
    $this->assertEquals($data_set6['omdb_api_url'], $omdb_api_url);
    $this->assertEquals($data_set6['omdb_api_key'], $omdb_api_key);
    $this->assertEquals($data_set6['omdb_api_cache_max_lifetime'], $omdb_api_cache_max_lifetime);

    // Create seventh set of invalid form data array item.
    $data_set7 = [];
    $data_set7 = [
      'omdb_api_url' => '',
      'omdb_api_key' => '',
      'omdb_api_cache_max_lifetime' => '',
    ];
    $this->submitForm($data_set7, 'Save configuration');

    $this->assertSession()->pageTextContains("OMDB API URL field is required.");
    $this->assertSession()->pageTextContains("OMDB API Key field is required.");
    $this->assertSession()->pageTextContains("OMDB API Data Max Cache Duration field is required.");
    $this->assertSession()->addressEquals('admin/structure/omdb-api/credentials-settings');

  }

}
