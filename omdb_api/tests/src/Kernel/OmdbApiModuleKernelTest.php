<?php

namespace Drupal\Tests\omdb_api\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\user\RoleInterface;

/**
 * OMDB API Module API Callback Kernel Test.
 *
 * @group omdb_api
 */
class OmdbApiModuleKernelTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'system',
    'node',
    'block',
    'path_alias',
    'options',
    'taxonomy',
    'omdb_api',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['user', 'omdb_api']);
    $this->installSchema('system', ['sequences']);
    $this->installSchema('omdb_api', []);
    $this->installEntitySchema('user');
    // Create anonymous user.
    $anonymous = $this->container->get('entity_type.manager')
      ->getStorage('user')
      ->create([
        'uid' => 0,
        'status' => 0,
        'name' => '',
      ]);
    $anonymous->save();
    /** @var \Drupal\user\RoleInterface $anonymous_role */
    $anonymous_role = $this->container->get('entity_type.manager')
      ->getStorage('user_role')
      ->load(RoleInterface::ANONYMOUS_ID);
    $anonymous_role->grantPermission('access content');
    $anonymous_role->save();

  }

  /**
   * Tests that OMDB API Module can be Installed.
   */
  public function testModulesCanBeInstalled() {

    $module = $this->container->get('module_handler')->moduleExists('omdb_api');

    $this->assertTrue($module);
  }

  /**
   * Test OMDB API Response With ImbdId.
   */
  public function testOmdbApiResponseWithImbdId() {

    $imdb_id = mt_rand(100, 99999);
    $media_type = ['movie', 'series'];
    $api_end_point = $media_type[mt_rand(0, count($media_type) - 1)];
    $result = $this->container->get('omdb_api.client_service')->getDataByImbdId($api_end_point, $imdb_id);

    if (!empty($result)) {
      if ($result['Response'] == 'True') {
        $this->assertStringContainsStringIgnoringCase($imdb_id, $result['imdbID']);
      }
      else {
        $this->assertEquals('False', $result['Response']);
        $this->assertArrayHasKey('Error', $result, 'IMDb ID is not available');
        $this->assertStringContainsStringIgnoringCase('Error getting data.', $result['Error']);
      }
    }
    else {
      $this->assertNull($result);
    }

  }

  /**
   * Test OMDB API Response With Imbd Movie Title.
   */
  public function testOmdbApiResponseWithImbdTitle() {

    $movie_title = $this->generateRandomString(5);
    $media_type = ['movie', 'series'];
    $api_end_point = $media_type[mt_rand(0, count($media_type) - 1)];
    $result = $this->container->get('omdb_api.client_service')->getDataByImbdTitle($api_end_point, $movie_title);

    if ($result['Response'] == 'True') {
      $this->assertEquals($movie_title, $result['Title']);
    }
    else {
      $this->assertEquals('False', $result['Response']);
      $this->assertArrayHasKey('Error', $result, 'Imbd Movie Title is not available');
      $this->assertStringContainsStringIgnoringCase('not found!', $result['Error']);
    }

  }

  /**
   * Test OMDB API Response With Imbd Search Parameter.
   */
  public function testOmdbApiResponseWithImbdSearchParameter() {

    $search_parameter = $this->generateRandomString(5);
    $media_type = ['movie', 'series'];
    $api_end_point = $media_type[mt_rand(0, count($media_type) - 1)];
    $result = $this->container->get('omdb_api.client_service')->getDataByImbdSearchParameter($api_end_point, $search_parameter);

    if ($result['Response'] == 'True') {
      $this->assertEquals($search_parameter, $result['Title']);
    }
    else {
      $this->assertEquals('False', $result['Response']);
      $this->assertSame('False', $result['Response']);
      $this->assertStringContainsStringIgnoringCase('not found!', $result['Error']);
    }

  }

  /**
   * Method to generate Random String.
   */
  private function generateRandomString($length = 10) {

    $pieces = [];
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $max = mb_strlen($characters, '8bit') - 1;
    for ($i = 0; $i < $length; ++$i) {
      $pieces[] = $characters[random_int(0, $max)];
    }
    return implode('', $pieces);

  }

}
