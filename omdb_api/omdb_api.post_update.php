<?php

/**
 * @file
 * Contains post update hooks.
 */

/**
 * Successful post-update.
 */
function omdb_api_post_update_a() {
  // Note that this is called 'a' so that it will run first. The post-updates
  // are executed in alphabetical order.
  return t('This is the update message from omdb_api_post_update_a');
}

/**
 * Failing post-update.
 */
function omdb_api_post_update_failing() {
  throw new \Exception('This is the exception message thrown in omdb_api_post_update_failing');
}
