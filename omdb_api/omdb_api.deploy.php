<?php

/**
 * @file
 * Contains deploy hook.
 *
 * This is a NAME.deploy.php file. It contains "deploy" functions. These are
 * one-time functions that run *after* config is imported during a deployment.
 *
 * These are a higher level alternative to hook_update_n and
 * hook_post_update_NAME functions.
 *
 * @see https://www.drush.org/latest/deploycommand/#authoring-update-functions
 * for a detailed comparison .
 *
 * @see https://github.com/drush-ops/drush/blob/11.x/tests/fixtures/modules/woot/
 * See woot module for reference.
 */

/**
 * Successful deploy hook.
 */
function omdb_api_deploy_a() {
  // Note that this is called 'a' so that it will run first. The deploy hooks
  // are executed in alphabetical order.
  return t('This is the update message from omdb_api_deploy_a');
}
