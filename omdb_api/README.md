# OMDB API

                                             .,.
                                            .cd:..
                                            .xXd,,'..
                                           .lXWx;;;,,..
                                         .,dXWXo;;;;;,,..
                                       .;dKWWKx:;;;;;;;,,'..
                                    .;oOXNXKOo;;;;;;;;;;;;,,,'..
                                 .:dOXWMMN0Okl;;;;;;;;;;;;;;;;,,,'..
                             .,lk0NMMMMMMNKOxc;;;;;;;;;;;;;;;;;;;;,,,'..
                         .'cx0XWMMMMMMMWX0kd:;;;;;;;;;;;;;;;;;;;;;;;;;,,,..
                      .'cx0NMMMMMMMMMWX0Oxl;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;,,'..
                    .;d0NMMMMMMMMMMWX0Oxl:;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;,,...
                  .:kXWMMMMMMMMMWNK0kdl:;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;,'..
                .cONMMMMMMMMMWNX0Okoc;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;,'.
              .;kNMMMMMMMMWNX0Okdl:;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;,'..
             .oXMMMMMMWWXK0Oxdl:;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;,'..
            ,oKWWWWNXKK0kxoc:;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;,'..
           'lOO0000OOxdlc;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;,'''.
          .,lxkxxdolc:;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;,''''..
         .,;;;::;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;,,'''''..
        .,;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;,'''''''.
       .';;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;,,'''''''..
    .',;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;,,'''''''''..
    .,;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;,,''''''''''..
    ',;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;,,''''''''''''.
    ,,;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;,,''''''''''''''.
    ,;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;,,'''''''''''''''.
    ,;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;,,''''''''''''''''''
    ,;;;;;;;;;;;;;;;;;;;;;;;;;;;cldxkkOkkxxdlc;;;;;;;;;;;;;;;;;;;;;;;;,,''''''''''',''''''''
    ,;;;;;;;;;;;;;;;;;;;;;;;:ox0XWMMMMMMMMMWNX0kxdc;;;;;;;;;;;;;;;;,,,'''''''';cdk00Okl,''''
    ,;;;;;;;;;;;;;;;;;;;;;cxKWMMMMMMMMMMMMMMMMMMMWN0xl;;;;;;;;;;,,,'''''''';lkKWMMMMMMW0c'''
    ',;;;;;;;;;;;;;;;;;;:dKWMMMMMMMMMMMMMMMMMMMMMMMMMN0xl;;;;;,,'''''''';okXWMMMMMMMMMMM0:'.
    .,;;;;;;;;;;;;;;;;;:kNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMN0xl;''''''',:oOXWMMMMMMMMMMMMMMNd'.
    .',;;;;;;;;;;;;;;;;xWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMN0xolccoxONMMMMMMMMMMMMMMMMMMWd'.
     .,;;;;;;;;;;;;;;;oXMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWNWMMMMMMMMMMMMMMMMMMMMMMXl..
     .',;;;;;;;;;;;;;;xNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWX0OOOKNMMMMMMMMMMMMMMMMMMMM0;.
      .',;;;;;;;;;;;;;dNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWN0xl:,''',cxXWMMMMMMMMMMMMMMMMWd.
       .',;;;;;;;;;;;;lKMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWKko:,'''''''''':dKWMMMMMMMMMMMMMWk,
        .',;;;;;;;;;;;;dXMMMMMMMMMMMMMMMMMMMMMMMMWX0xl;'''',;:::;,''''';oKWMMMMMMMMMMWO,
         ..',,;;;;;;;;;;o0NMMMMMMMMMMMMMMMMMMN0xdo:,''',cdO0XXXXK0kl,'''';o0WMMMMMMMNd,
           .''',,,,,,,,,,;lk0XWMMMMMMMMWNX0ko:,'''''';oONN0xdoodx0NNx,''''';lOXWWWXkc.
            ..'''''''''''''',:lloodddoolc;'''''''''',xN0dc,'''''',oK0:''''''',:clc;..
             ...''''''''''''''''''''''''''''''''''''':c,''''''''''';;''''''''''''..
               ..''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''...
                 ...'''''''''''''''''''''',lxdc,'''''''''''''''''',:oxkl''''''..
                    ...''''''''''''''''''':kNMNK0OxdollcccclllodxO0XX0d;'''..
                       ..'''''''''''''''''',cxOKXXNWMMWWWWWWWWNXKOxo:'''..
                          ...'.'''''''''''''''',;:clooodddoollc:,'''...
                               ....'''''''''''''''''''''''''''.....
                                   ....'..''''''''''''........


## Module Functionality :

This Module creates functionality to integrate Drupal and OMDB API.

This Module Creates a new Content Entity called as OMDB API.

This Module provides Movie details when search form is submitted in Drupal.
The detail are obtained by making an API call to the OMDB API end point.

This Module provides a page where all the omdb api entities are listed.

This Module provides a block on omdb api enity pages where a qr code for
the particlur page exists.

This Module provides custom readcrumb for the omdb api enity pages.

This Module provides migration plugin to import data from csv source.

http://drupal-entity-training.github.io/event/

## Drush Command :

`drush devel-generate:omdb-api 10 --kill --feedback=10`

## Get Data by OMDB Id .

https://www.omdbapi.com/?apikey=$API_KEY&i=tt0113375


https://www.omdbapi.com/?apikey=$API_KEY&t=batman


tt0113375
tt0000000

https://www.omdbapi.com/?s=space&type=movie&r=json&apikey=$API_KEY

https://www.omdbapi.com/?apikey=$API_KEY&i=tt0113375

https://www.omdbapi.com/?apikey=$API_KEY&i=tt0000001


{{baseUrl}}/?t=laborum amet Duis aliqua&y=-32152082&
type=movie&plot=short&r=xml&callback=laborum amet Duis aliqua&apikey=<API Key>

{{baseUrl}}/?i=laborum amet Duis aliqua&plot=short&r=xml&
callback=laborum amet Duis aliqua&apikey=<API Key>

{{baseUrl}}/?s=laborum amet Duis aliqua&y=-32152082&type=movie&r=xml&
page=-32152082&callback=laborum amet Duis aliqua&apikey=<API Key>


https://gorannikolovski.com/blog/ajax-dependent-select-drupal-8-and-9

## To Do :

Add CSV Export of the Entity.

~ Add QR Code Block. ~

Add Print Option to download Page as PDF.


```

    [0] => Title
    [1] => Year
    [2] => Rated
    [3] => Released
    [4] => Runtime
    [5] => Genre
    [6] => Director
    [7] => Writer
    [8] => Actors
    [9] => Plot
    [10] => Language
    [11] => Country
    [12] => Awards
    [13] => Poster
    [14] => Ratings
    [15] => Metascore
    [16] => imdbRating
    [17] => imdbVotes
    [18] => imdbID
    [19] => Type
    [20] => DVD
    [21] => BoxOffice
    [22] => Production
    [23] => Website
    [24] => Response

```

```

imdb_title
imdb_id
released_year
viewer_rating
released_date
runtime
genre
director
writer
actors
plot
language
country
awards
poster
ratings
metascore
imdb_rating
imdb_votes
type
dvd
box_office
production
website
api_response

```

```

      $data = [
        'imdb_title' => $response_data['Title'],
        'imdb_id' => $response_data['imdbID'],
        'released_year' => $response_data['Year'],
        'viewer_rating' => $response_data['Rated'],
        'released_date' => $response_data['Released'],
        'runtime' => $response_data['Runtime'],
        'genre' => $response_data['Genre'],
        'director' => $response_data['Director'],
        'writer' => $response_data['Writer'],
        'actors' => $response_data['Actors'],
        'plot' => $response_data['Plot'],
        'language' => $response_data['Language'],
        'country' => $response_data['Country'],
        'awards' => $response_data['Awards'],
        'poster' => $response_data['Poster'],
        'ratings' => $response_data['Ratings'],
        'metascore' => $response_data['Metascore'],
        'imdb_rating' => $response_data['imdbRating'],
        'imdb_votes' => $response_data['imdbVotes'],
        'type' => $response_data['Type'],
        'dvd_released_year' => $response_data['DVD'],
        'box_office' => $response_data['BoxOffice'],
        'production' => $response_data['Production'],
        'website' => $response_data['Website'],
        'api_response' => $response_data['Response'],
      ];

```

https://drupal.stackexchange.com/questions/296035/how-do-i-add-a-revisions-tab-to-a-custom-content-entity-to-list-revisions


```
$data = [
        'imdb_title' => $response_data['Title'],
        'imdb_id' => $response_data['imdbID'],
        'released_year' => $response_data['Year'],
        'viewer_rating' => $response_data['Rated'],
        'released_date' => $response_data['Released1'],
        'runtime' => $response_data['Runtime'],
        'genre' => $response_data['Genre'],
        'director' => $response_data['Director'],
        'writer' => $response_data['Writer'],
        'actors' => $response_data['Actors'],
        'plot' => $response_data['Plot'],
        'language' => $response_data['Language'],
        'country' => $response_data['Country'],
        'awards' => $response_data['Awards'],
        'poster' => $response_data['Poster'],
        'ratings' => $response_data['Ratings1'],
        'metascore' => $response_data['Metascore'],
        'imdb_rating' => $response_data['imdbRating'],
        'imdb_votes' => $response_data['imdbVotes'],
        'type' => $response_data['Type'],
        'dvd_released_year' => $response_data['DVD'],
        'box_office' => $response_data['BoxOffice'],
        'production' => $response_data['Production'],
        'website' => $response_data['Website'],
        'api_response' => $response_data['Response'],
      ];
      /** @var \Drupal\omdb_api\Entity\OmdbApiEntityInterface $omdb_data */
      $omdb_entity_data = $this->entityTypeManager->getStorage('omdb_api')->loadByProperties(
        [
          'imdb_title' => $response_data['Title'],
          'imdb_id' => $response_data['imdbID'],
        ],
      );
      If (!empty($omdb_entity_data)) {
      $existing_entity_id = key($omdb_entity_data);
        /** @var \Drupal\omdb_api\Entity\OmdbApiEntityInterface $omdb_entity */
        $omdb_entity = $this->entityTypeManager->getStorage('omdb_api')->load($existing_entity_id);
        $omdb_entity->imdb_title = $response_data['Title'];
        $omdb_entity->imdb_id->value = $response_data['imdbID'];
        $omdb_entity->language->value = $response_data['Language'];
        $omdb_entity->setNewRevision(TRUE);
        $omdb_entity->revision_log = 'Created revision for omdb_api' . $existing_entity_id;
        $omdb_entity->save();
      }
      else {
      /** @var \Drupal\omdb_api\Entity\OmdbApiEntityInterface $omdb_entity */
        $omdb_entity = $this->entityTypeManager->getStorage('omdb_api')->create($data);
        $omdb_entity->save();
      }
```

https://www.drupal.org/docs/drupal-apis/update-api/writing-automated-update-tests-for-drupal-8-or-later

https://www.drupal.org/project/drupal/issues/3150294#comment-14264087


php ./web/core/scripts/db-tools.php dump-database-d8-mysql | gzip > /app/web/modules/custom/example_codes/omdb_api/tests/fixtures/update/omdb_api_fixtures_db.php.gz


```

public function getImdbTitle();
public function setImdbTitle($imdb_title);

public function getImdbId();
public function setImdbId($imdb_id);

public function getCreatedTime();
public function setCreatedTime($timestamp);

public function isPublished();

public function setPublished($published);

public function getRevisionCreationTime();
public function setRevisionCreationTime($timestamp);

public function getRevisionUser();

public function setRevisionUserId($uid);

public function getReleasedYear();
public function setReleasedYear($released_year);

public function getReleasedDate();
public function setReleasedDate($released_date);

public function getViewerRating();
public function setViewerRating($viewer_rating);

public function getRuntime();
public function setRuntime($runtime);

public function getGenre();
public function setGenre($genre);

public function getDirector();
public function setDirector($director);

public function getWriter();
public function setWriter($writer);

public function getActors();
public function setActors($actors);

public function getPlot();
public function setPlot($plot);

public function getLanguage();
public function setLanguage($language);

public function getCountry();
public function setCountry($country);

public function getAwards();
public function setAwards();

public function getPoster();
public function setPoster();

public function getRatings();
public function setRatings();

public function getMetascore();
public function setMetascore();

public function getimdbRating();
public function setimdbRating();

public function getimdbVotes();
public function setimdbVotes();

public function getType();
public function setType();

public function getDvdReleasedYear();
public function setDvdReleasedYear();

public function getBoxOfficeCollections();
public function setBoxOfficeCollections();

public function getProductionHouse();
public function setProductionHouse();

public function getWebsite();
public function setWebsite();

public function getApiResponse();
public function setApiResponse();

public function get();
public function set();

```

// @codingStandardsIgnoreFile
// http://grep.xnddx.ru/node/30856622
// https://git.drupalcode.org/project/boardgames/-/tree/8.x-0.1

// @codingStandardsIgnoreStart
protected $customer_id = 0;
// @codingStandardsIgnoreEnd


http://grep.xnddx.ru/node/30936935

http://grep.xnddx.ru/node/30867769

http://grep.xnddx.ru/node/32240257


```

      $data_array = [
        'imdb_title' => $omdb_api_entity->getImdbTitle(),
        'imdb_id' => $omdb_api_entity->getImdbId(),
        'released_year' => $omdb_api_entity->getReleasedYear(),
        'viewer_rating' => $omdb_api_entity->getViewerRating(),
        'released_date' => $omdb_api_entity->getReleasedDate(),
        'runtime' => $omdb_api_entity->getRuntime(),
        'genre' => $omdb_api_entity->getGenre(),
        'director' => $omdb_api_entity->getDirector(),
        'writer' => $omdb_api_entity->getWriter(),
        'actors' => $omdb_api_entity->getActors(),
        'plot' => $omdb_api_entity->getPlot(),
        'language' => $omdb_api_entity->getLanguage(),
        'country' => $omdb_api_entity->getCountry(),
        'awards' => $omdb_api_entity->getAwards(),
        'poster' => $omdb_api_entity->getPoster(),
        'ratings' => $omdb_api_entity->getRatings(),
        'metascore' => $omdb_api_entity->getMetascore(),
        'imdb_rating' => $omdb_api_entity->getimdbRating(),
        'imdb_votes' => $omdb_api_entity->getimdbVotes(),
        'type' => $omdb_api_entity->getType(),
        'dvd_released_year' => $omdb_api_entity->getDvdReleasedYear(),
        'box_office_collections' => $omdb_api_entity->getBoxOfficeCollections(),
        'production' => $omdb_api_entity->getProductionHouse(),
        'website' => $omdb_api_entity->getWebsite(),
        'api_response' => $omdb_api_entity->getApiResponseStatus(),

      ];

```

    $this->drush('devel-generate-terms', [55], ['kill' => NULL, 'bundles' => $this->vocabulary->id()]);

drush genc 20 4 --kill

https://www.keboca.com/drupal-8-batch-operations

https://github.com/scotteuser/drupalcamp-london-2020-training



+    $a = Settings::get('file_temp_path');
+    // Ensure the default temp directory exist and is writable. The configured
+    // temp directory may be removed during update.
+    $temp_dir = \Drupal::service('file_system')->getTempDirectory();
+    \Drupal::service('file_system')->prepareDirectory($temp_dir, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
+
+    $default_scheme = \Drupal::config('system.file')->get('default_scheme');
+    $destination = \Drupal::service('file_system')->realpath($default_scheme . "://omdb-api/qrcodes");
+    // Prepare directory for QR codes storing.
+    \Drupal::service('file_system')->prepareDirectory($destination, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
+
+    // @see https://www.drupal.org/files/issues/2019-07-25/3039026-121.patch


$url_token = $this->requestStack->getCurrentRequest()->query->get('token');
if ($csrf_token !== $url_token) {
  return AccessResult::forbidden('CSRF validation failed')->setCacheMaxAge(0);
}

$log_path = \Drupal::service('file_system')->realpath('public://new_relic_insights.log');
    error_log(implode('|', $entry) . PHP_EOL, 3, $log_path);

("uid", "type", "message", "variables", "severity", "link", "location", "referer", "hostname", "timestamp")

uuid|request_uri|user_agent|ip|protocol|referer|operation|type|title|id|langcode|user|date|link|uid|severity|message

'verify' => 'data/selfsigned_webfutura_eu.crt',
