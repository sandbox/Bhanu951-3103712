<?php

// @codingStandardsIgnoreStart
// https://gist.github.com/jaywilliams/385876
// http://grep.xnddx.ru/node/32233244
// @codingStandardsIgnoreEnd

namespace Drupal\omdb_api\Plugin\migrate\source;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\Plugin\migrate\source\SourcePluginBase;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\migrate\MigrateException;

/**
 * Migrate Source plugin for OMDB API Entity from CSV File.
 *
 * @MigrateSource(
 *   id = "omdb_api_entity_csv_source",
 *   source_module = "omdb_api"
 * )
 */
class OmdbApiEntityCsvSource extends SourcePluginBase implements ContainerFactoryPluginInterface {

  /**
   * Path to the directory which contains the source file.
   *
   * @var string
   */
  protected $filePath = '';

  /**
   * Iterator.
   *
   * @var \ArrayIterator
   */
  protected $iterator;

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * List of required Config Keys.
   *
   * @var array
   */
  protected $requiredConfigKeys = [
    'file_path',
    'delimiter',
    'enclosure',
    'header_offset',
  ];

  /**
   * Mapping of required Config Keys.
   *
   * @var array
   */
  protected $configKeyMapping = [
    'file_path' => 'file_path',
    'delimiter' => 'delimiter',
    'enclosure' => 'enclosure',
    'header_offset' => 'header_offset',
  ];

  /**
   * Constructs Disqus comments destination plugin.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implemetation definition.
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The migration.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, LoggerChannelFactoryInterface $logger_factory) {

    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
    $this->logger = $logger_factory->get('omdb_api');
    $this->assertConfiguration()->setPropertiesFromConfiguration();

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('logger.factory')
    );

  }

  /**
   * Verifies the required Config Keys Mappings Exists.
   *
   * @return $this
   */
  protected function assertConfiguration() {

    $missing_keys = array_diff(
      $this->requiredConfigKeys,
      array_keys($this->configuration)
    );

    assert(
      count($missing_keys) === 0,
      'Source configuration must include the following keys: ' . implode(', ', $missing_keys)
    );

    return $this;

  }

  /**
   * Sets the required Config Keys Mappings.
   *
   * @return $this
   */
  protected function setPropertiesFromConfiguration() {

    foreach ($this->configKeyMapping as $property => $config) {
      if (array_key_exists($config, $this->configuration)) {
        $this->$property = $this->configuration[$config];
      }
    }

    return $this;

  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['id']['type'] = 'integer';
    return $ids;
  }

  /**
   * {@inheritDoc}
   */
  public function fields() {
    return $this->fields;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $row->setSourceProperty('uid', '0');
    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function __toString() {
    return 'OMDB API Entity CSV Source';
  }

  /**
   * {@inheritDoc}
   */
  public function count($refresh = FALSE) {
    return count($this->getItems());
  }

  /**
   * Get protected values.
   *
   * @todo Do we need this?
   *
   * @param string $property
   *   Property name.
   *
   * @return mixed
   *   Value of the property.
   */
  public function get(string $property) {
    return $this->{$property};
  }

  /**
   * Get all items.
   *
   * @return array
   *   All items.
   */
  protected function getItems(): array {

    if ($this->items === NULL) {
      $this->initItems();
    }

    return $this->items;

  }

  /**
   * Gets Array of the source data.
   */
  protected function initItems() {

    $this->items = $this->fileGetContents($this->getFilePath(), $this->getHeaderOffset(), $this->getDelimiter());
    $ids = $this->getIds();
    if (count($ids) === 1 && key($this->items) !== 0) {
      $id_field = key($ids);
      foreach (array_keys($this->items) as $id) {
        $this->items[$id][$id_field] = $id;
      }
    }

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  protected function initializeIterator() {
    return new \ArrayIterator($this->getItems());
  }

  /**
   * Get File Path.
   */
  protected function getFilePath(): string {

    if (empty($this->configuration['file_path'])) {
      throw new MigrateException('Required parameter "file_path" not defined.');
    }
    else {
      return (string) $this->configuration['file_path'];
    }

  }

  /**
   * Get File Path.
   */
  protected function getDelimiter(): string {

    if (empty($this->configuration['delimiter'])) {
      throw new MigrateException('Required parameter "delimiter" not defined.');
    }
    else {
      return (string) $this->configuration['delimiter'];
    }

  }

  /**
   * Get File Path.
   */
  protected function getHeaderOffset(): string {

    if ((empty($this->configuration['header_offset'])) && (!isset($this->configuration['header_offset']))) {
      throw new MigrateException('Required parameter "header_offset" not defined.');
    }
    else {
      return (string) $this->configuration['header_offset'];
    }

  }

  /**
   * Get File Content From file in the given path.
   */
  protected function fileGetContents(string $file_path, $header_offset, $delimiter = ','): array {

    $data = file_get_contents($file_path);

    if ($data === FALSE) {

      $this->logger->error($this->t("There was an error while reading the file at path <em> @file_path </em> , Please check the provided path", ["@file_path" => $file_path]));
      throw new \RuntimeException("file '$file_path' count not be read", 1);

    }
    else {
      if (!file_exists($file_path) || !is_readable($file_path)) {

        $this->logger->error($this->t("There was an error while reading the file at path <em> @file_path </em> , Please check the provided path", ["@file_path" => $file_path]));
        return FALSE;

      }

      $header = NULL;
      $data = [];
      if (($handle = fopen($file_path, 'r')) !== FALSE) {
        while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE) {
          if (!$header) {
            $header = $row;
          }
          else {
            $data[] = array_combine($header, $row);
          }
        }
        fclose($handle);
      }
      return $data;
    }
  }

}
