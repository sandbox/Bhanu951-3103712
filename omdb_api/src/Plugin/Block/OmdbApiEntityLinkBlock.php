<?php

namespace Drupal\omdb_api\Plugin\Block;

use Drupal\omdb_api\Entity\OmdbApiEntityInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\omdb_api\Service\OmdbApiContentServiceInterface;

/**
 * Provides an OMDB API Entity Link Block.
 *
 * @Block(
 *   id = "omdb_api_entity_link_block",
 *   admin_label = @Translation("OMDB API Entity Link Block"),
 *   category = @Translation("Custom")
 * )
 */
class OmdbApiEntityLinkBlock extends BlockBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The omdb_api.content_creation_service service.
   *
   * @var \Drupal\omdb_api\Service\OmdbApiContentServiceInterface
   */
  protected $omdbContentService;

  /**
   * Constructs a new OMDB API Entity Link Block instance.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $currentRouteMatch
   *   The current route match.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   Request object to get request params.
   * @param \Drupal\omdb_api\Service\OmdbApiContentServiceInterface $omdb_content_service
   *   The OMDB Content Creation Service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerChannelFactoryInterface $logger_factory, EntityTypeManagerInterface $entity_type_manager, CurrentRouteMatch $currentRouteMatch, RequestStack $request, OmdbApiContentServiceInterface $omdb_content_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->logger = $logger_factory->get('omdb_api');
    $this->entityTypeManager = $entity_type_manager;
    $this->currentRouteMatch = $currentRouteMatch;
    $this->requestStack = $request;
    $this->omdbContentService = $omdb_content_service;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory'),
      $container->get('entity_type.manager'),
      $container->get('current_route_match'),
      $container->get('request_stack'),
      $container->get('omdb_api.content_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $build = [];

    $omdb_api = $this->currentRouteMatch->getParameter('omdb_api');
    $current_route = $this->currentRouteMatch->getRouteName();

    // Check if the route is OMDB API Entity View Page.
    if (($current_route == 'entity.omdb_api.canonical') && ($omdb_api instanceof OmdbApiEntityInterface)) {

      $domain = $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost();
      $uri = $this->requestStack->getCurrentRequest()->getRequestUri();
      $imdb_id = strtoupper($omdb_api->getImdbId());
      $imdb_title = ucfirst($omdb_api->getImdbTitle());

      $link = Url::fromUri($domain . $uri);

      // Get the generated QR Code URl for the given data.
      $result = $this->omdbContentService->generateQrCodeImage($imdb_id, $link->toString()) ?? '';

      $build['qr_code_image'] = [
        '#type' => 'inline_template',
        '#template' => '<img src=' . $result . ' alt=' . $imdb_title . '>',
        '#cache' => [
          'tags' => $omdb_api->getCacheTags(),
        ],
      ];

    }

    $build['#cache']['contexts'] = ['route'];

    return $build;

  }

}
