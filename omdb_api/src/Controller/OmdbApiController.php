<?php

namespace Drupal\omdb_api\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\omdb_api\Service\OmdbApiClientServiceInterface;
use Drupal\omdb_api\Service\OmdbApiContentServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Returns responses for OMDB API routes.
 */
class OmdbApiController extends ControllerBase {

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The omdb_api.client_service service.
   *
   * @var \Drupal\omdb_api\Service\OmdbApiClientServiceInterface
   */
  protected $omdbClientService;

  /**
   * The omdb_api.content_creation_service service.
   *
   * @var \Drupal\omdb_api\Service\OmdbApiContentServiceInterface
   */
  protected $omdbContentService;

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The OMDB API Controller constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\omdb_api\Service\OmdbApiClientServiceInterface $omdb_client_service
   *   The omdb_api.client_service service.
   * @param \Drupal\omdb_api\Service\OmdbApiContentServiceInterface $omdb_content_service
   *   The OMDB Content Creation Service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   Request object to get request params.
   */
  public function __construct(LoggerChannelFactoryInterface $logger_factory, EntityTypeManagerInterface $entity_type_manager, OmdbApiClientServiceInterface $omdb_client_service, OmdbApiContentServiceInterface $omdb_content_service, RequestStack $request) {
    $this->logger = $logger_factory->get('omdb_api');
    $this->entityTypeManager = $entity_type_manager;
    $this->omdbClientService = $omdb_client_service;
    $this->omdbContentService = $omdb_content_service;
    $this->requestStack = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory'),
      $container->get('entity_type.manager'),
      $container->get('omdb_api.client_service'),
      $container->get('omdb_api.content_service'),
      $container->get('request_stack')
    );
  }

  /**
   * Builds the Title of OMDB API response.
   */
  public function getTitle(string $search_string) {
    return $this->t('OMDB API Response for : @data', ['@data' => $search_string]);
  }

  /**
   * Builds the OMDB API response.
   */
  public function build(string $api_end_point, string $search_by, string $search_string) {

    // $this->omdbContentService->generatePdf();
    $header = $rows = $response_data = $build = [];

    if ($search_by == 'id') {
      $response_data = $this->omdbClientService->getDataByImbdId($api_end_point, $search_string);
      $this->omdbContentService->createContent($response_data);
    }
    elseif ($search_by == 'name') {
      $response_data = $this->omdbClientService->getDataByImbdTitle($api_end_point, $search_string);
      $this->omdbContentService->createContent($response_data);

    }
    else {
      $response_data = $this->omdbClientService->getDataByImbdSearchParameter($api_end_point, $search_string);
    }

    $domain = $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost();
    $uri = $this->requestStack->getCurrentRequest()->getRequestUri();
    $link = Link::fromTextAndUrl('View', Url::fromUri($domain . $uri));

    $message = $this->t('Data Received : @data');
    $args = [
      '@data' => print_r($response_data, TRUE),
      'link' => $link->toString(),
    ];

    $this->logger->notice($message, $args);

    // var_dump($response_data);
    if (isset($response_data['Search']) && !empty($response_data['Search'])) {

      // Get Header data.
      $header = [
        $this->t("Title"),
        $this->t("Year"),
        $this->t("ImdbId"),
        $this->t("Type"),
        $this->t("Poster"),
      ];

      // Get Rows data.
      $rows = $response_data['Search'];

      $build['table'] = [
        '#type' => 'table',
        '#header' => $header,
        '#rows' => $rows,
        '#empty' => $this->t('No search results has been found.'),
      ];

    }
    elseif ((!empty($response_data))) {
      // && ($response_data['Response'] == 'False')
      // Get Header data.
      $header = array_keys($response_data);
      // Get Rows data.
      $rows[] = $response_data;

      $build['output'] = [
        '#theme' => 'omdb_api_response_output_table',
        '#results' => $response_data,
        '#empty' => $this->t('No search results has been found.'),
        '#attached' => [
          'library' => [
            'omdb_api/response_data_table.styles',
          ],
        ],
      ];

    }

    return $build;
  }

}
