<?php

namespace Drupal\omdb_api\Entity\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a OMDB API Entity Filter Form.
 */
class OmdbApiEntityFilterForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'omdb_api_entity_filter_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $request = $this->getRequest();

    $form['filter'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['form--inline', 'clearfix'],
      ],
    ];

    $form['filter']['type'] = [
      '#type' => 'select',
      '#title' => 'Select Type',
      '#options' => [
        'movie' => $this->t('Movie'),
        'series' => $this->t('Series'),
      ],
      '#default_value' => $request->get('type') ?? 0,
    ];

    $form['filter']['status'] = [
      '#type' => 'select',
      '#title' => 'Select Status',
      '#options' => [
        'enabled' => $this->t('Published'),
        'disabled' => $this->t('Un-Published'),
      ],
      '#default_value' => $request->get('status') ?? 0,
    ];

    $form['filter']['imdb_title'] = [
      '#type' => 'textfield',
      '#title' => 'IMDB Title',
      '#default_value' => $request->get('imdb_title') ?? '',
    ];

    $form['actions']['wrapper'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['form-item']],
    ];

    $form['actions']['wrapper']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Filter',
    ];

    if ($request->getQueryString()) {
      $form['actions']['wrapper']['reset'] = [
        '#type' => 'submit',
        '#value' => 'Reset',
        '#submit' => ['::resetForm'],
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $query = [];

    $type = $form_state->getValue('type') ?? 0;
    if ($type) {
      $query['type'] = $type;
    }

    $status = $form_state->getValue('status') ?? 0;
    if ($status) {
      $query['status'] = $status;
    }

    $imdb_title = $form_state->getValue('imdb_title') ?? '';
    if ($imdb_title) {
      $query['imdb_title'] = $imdb_title;
    }

    $form_state->setRedirect('entity.omdb_api.collection', $query);
  }

  /**
   * {@inheritdoc}
   */
  public function resetForm(array $form, FormStateInterface &$form_state) {
    $form_state->setRedirect('entity.omdb_api.collection');
  }

}
