<?php

namespace Drupal\omdb_api\Entity\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\File\FileSystemInterface;

/**
 * Configure OMDB API Entity Logger Settings.
 */
class OmdbApiEntityLoggerSettingsForm extends ConfigFormBase {


  /**
   * Object of applications file system service.
   *
   * @var object
   */
  public $fileSystem;

  /**
   * The OMDB API Controller constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   */
  public function __construct(FileSystemInterface $file_system) {
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('file_system')

    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'omdb_api_entity_logger_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['omdb_api_entity_logger.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['log_file_path'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Log File Path'),
      '#default_value' => $this->config('omdb_api_entity_logger.settings')->get('log_file_path'),
    ];

    $form['log_file_name'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Log File Name'),
      '#default_value' => $this->config('omdb_api_entity_logger.settings')->get('log_file_name') ?? 'omdb-api.log',
    ];

    $form['log_file_header'] = [
      '#type' => 'textarea',
      '#required' => TRUE,
      '#title' => $this->t('Log File Header'),
      '#default_value' => $this->config('omdb_api_entity_logger.settings')->get('log_file_header'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $destination = $this->fileSystem->realpath($form_state->getValue('log_file_path'));
    $this->fileSystem->prepareDirectory($destination, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
    $is_writable = is_writable($destination);
    $is_directory = is_dir($destination);
    if (!$is_writable || !$is_directory) {
      if (!$is_directory) {
        $error = $this->t('The directory @directory does not exist.', ['@directory' => $destination]);
      }
      else {
        $error = $this->t('The directory @directory is not writable.', ['@directory' => $destination]);
      }
    }

    if (!empty($error)) {
      $form_state->setErrorByName('log_file_path', $this->t('The value of Log File Path is not valid. An Error Occourred @error', ['@error' => $error]));
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $log_file_path = $form_state->getValue('log_file_path');
    $log_file_name = $form_state->getValue('log_file_name');
    $log_file_header = $form_state->getValue('log_file_header');

    $this->config('omdb_api_entity_logger.settings')
      ->set('log_file_path', $log_file_path)
      ->set('log_file_name', $log_file_name)
      ->set('log_file_header', $log_file_header)
      ->save();
    $log_file = $log_file_path . DIRECTORY_SEPARATOR . $log_file_name;
    file_put_contents($log_file, $log_file_header . PHP_EOL, FILE_APPEND);
    parent::submitForm($form, $form_state);
  }

}
