<?php

namespace Drupal\omdb_api\Entity\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManager;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\omdb_api\Entity\OmdbApiEntityInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\omdb_api\Service\OmdbApiEntityExportService;

/**
 * Form controller for the omdb api entity export.
 */
class OmdbApiEntityExportForm extends FormBase {

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Batch Builder.
   *
   * @var \Drupal\Core\Batch\BatchBuilder
   */
  protected $batchBuilder;

  /**
   * Configuration Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Entity Stream Wrapper.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManager
   */
  protected $streamWrapperManager;

  /**
   * The extension path resolver.
   *
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected $extensionPathResolver;

  /**
   * Object of applications file system service.
   *
   * @var object
   */
  public $fileSystem;

  /**
   * The omdb_api entity export service.
   *
   * @var \Drupal\omdb_api\Service\OmdbApiEntityExportService
   */
  protected $omdbEntityExportService;

  /**
   * The OMDB API Controller constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManager $stream_wrapper_manager
   *   The stream_wrapper_manager service.
   * @param \Drupal\Core\Extension\ExtensionPathResolver $extension_path_resolver
   *   The extension path resolver.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   Object of file_system service.
   * @param \Drupal\omdb_api\Service\OmdbApiEntityExportService $omdb_entity_export_service
   *   The OMDB Entity Export Service.
   */
  public function __construct(LoggerChannelFactoryInterface $logger_factory, EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory, StreamWrapperManager $stream_wrapper_manager, ExtensionPathResolver $extension_path_resolver, FileSystemInterface $file_system, OmdbApiEntityExportService $omdb_entity_export_service) {
    $this->logger = $logger_factory->get('omdb_api');
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->streamWrapperManager = $stream_wrapper_manager;
    $this->extensionPathResolver = $extension_path_resolver;
    $this->fileSystem = $file_system;
    $this->omdbEntityExportService = $omdb_entity_export_service;
    $this->batchBuilder = new BatchBuilder();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory'),
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('stream_wrapper_manager'),
      $container->get('extension.path.resolver'),
      $container->get('file_system'),
      $container->get('omdb_api.entity_export_service')

    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'omdb_api_entty_batch_export_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['#prefix'] = '<p>Form to Export OMDB API Entities.</p>';

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Export'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $ids = $submissions = [];

    $header = [
      'id', 'imdb_title', 'imdb_id', 'released_year', 'viewer_rating', 'released_date', 'runtime', 'genre', 'director', 'writer', 'actors', 'plot', 'language', 'country', 'awards', 'poster', 'ratings', 'metascore', 'imdb_rating', 'imdb_votes', 'type', 'dvd_released_year', 'box_office_collections', 'production', 'website', 'api_response', 'created',
    ];

    $storage = $this->entityTypeManager->getStorage('omdb_api');

    $query = $storage->getQuery()->condition('status', 1);

    $ids = $query->execute();
    // Get all the results.
    if ($ids != NULL && count($ids) >= 1) {
      $submissions = $storage->loadMultiple($ids);
    }

    if (count($submissions) > 0) {
      foreach ($submissions as $omdb_api_entity) {

        if ($omdb_api_entity instanceof OmdbApiEntityInterface) {

          $data_array = [
            'id' => $omdb_api_entity->id(),
            'imdb_title' => $omdb_api_entity->getImdbTitle(),
            'imdb_id' => $omdb_api_entity->getImdbId(),
            'released_year' => $omdb_api_entity->getReleasedYear(),
            'viewer_rating' => $omdb_api_entity->getViewerRating(),
            'released_date' => $omdb_api_entity->getReleasedDate(),
            'runtime' => $omdb_api_entity->getRuntime(),
            'genre' => $omdb_api_entity->getGenre(),
            'director' => $omdb_api_entity->getDirector(),
            'writer' => $omdb_api_entity->getWriter(),
            'actors' => $omdb_api_entity->getActors(),
            'plot' => $omdb_api_entity->getPlot(),
            'language' => $omdb_api_entity->getLanguage(),
            'country' => $omdb_api_entity->getCountry(),
            'awards' => $omdb_api_entity->getAwards(),
            'poster' => $omdb_api_entity->getPoster(),
            'ratings' => $omdb_api_entity->getRatings(),
            'metascore' => $omdb_api_entity->getMetascore(),
            'imdb_rating' => $omdb_api_entity->getimdbRating(),
            'imdb_votes' => $omdb_api_entity->getimdbVotes(),
            'type' => $omdb_api_entity->getType(),
            'dvd_released_year' => $omdb_api_entity->getDvdReleasedYear(),
            'box_office_collections' => $omdb_api_entity->getBoxOfficeCollections(),
            'production' => $omdb_api_entity->getProductionHouse(),
            'website' => $omdb_api_entity->getWebsite(),
            'api_response' => $omdb_api_entity->getApiResponseStatus(),
            'created' => $omdb_api_entity->getCreatedTime(),

          ];
        }

        $output[] = array_combine($header, $data_array);
      }
    }

    $file_name = $this->omdbEntityExportService->getCsvFilePath();

    $this->batchBuilder
      ->setTitle($this->t('Processing Batch...'))
      ->setInitMessage($this->t('Initializing Batch...'))
      ->setProgressMessage($this->t('Completed @current of @total. Estimated remaining time: @estimate. Elapsed time : @elapsed.'))
      ->setErrorMessage($this->t('An error has occurred.'));
    $this->batchBuilder->addOperation([
      $this->omdbEntityExportService,
      'processItems',
    ], [$file_name, $header, $output]);
    $this->batchBuilder->setFinishCallback([
      $this->omdbEntityExportService,
      'finished',
    ]);
    batch_set($this->batchBuilder->toArray());

  }

}
