<?php

namespace Drupal\omdb_api\Entity\ListBuilder;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\views\Views;

/**
 * Provides a list controller for the omdb api entity type.
 */
class OmdbApiEntityListBuilder extends EntityListBuilder {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Constructs a new OmdbApiListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The Form Builder Service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   Request object to get request params.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, DateFormatterInterface $date_formatter, FormBuilderInterface $form_builder, RequestStack $request) {
    parent::__construct($entity_type, $storage);
    $this->dateFormatter = $date_formatter;
    $this->formBuilder = $form_builder;
    $this->requestStack = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('date.formatter'),
      $container->get('form_builder'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function render() {

    $options = [
      'absolute' => TRUE,
      'attributes' => [
        'target' => '_blank',
      ],
    ];

    $link = Link::fromTextAndUrl('OMDB API Credentials Settings Form page', Url::fromRoute('entity.omdb_api_credentials.settings_form', [], $options));

    $settings_form_url = $link->toString();

    $build['description'] = [
      '#markup' => $this->t('<strong>OMDB API Entity credentials can be configured on </strong> @settings_form_url.', [
        '@settings_form_url' => $settings_form_url,
      ]),
    ];

    // Start of code to render View on Entity List Builder Page.
    $view = Views::getView('omdb_api_entity_search');

    if (is_object($view)) {
      $view->setDisplay('omdb_api_entity_list_embed');
      $view->setArguments([]);
      $view->execute();
      $rendered = $view->render();
      $build['view_output'] = $rendered;
    }
    else {
      $build['view_output'] = [
        '#markup' => $this->t('OMDB API List View doesnt exist, please create it.'),
      ];
    }

    // End of code to render View on Entity List Builder Page.
    $total = $this->getStorage()
      ->getQuery()
      ->accessCheck(FALSE)
      ->count()
      ->execute();

    $build['summary'] = [
      '#markup' => $this->t('Total OMDB API Count: <strong>@total</strong>', ['@total' => $total]),
    ];

    return $build;

  }

}
