<?php

namespace Drupal\omdb_api\Entity\Breadcrumb;

use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\omdb_api\Entity\OmdbApiEntityInterface;
use Drupal\Core\Link;

/**
 * Provides a breadcrumb builder for omdb api.
 */
class OmdbApiEntityBreadcrumbBuilder implements BreadcrumbBuilderInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    $omdb_api = $route_match->getParameter('omdb_api');
    return $omdb_api instanceof OmdbApiEntityInterface;
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {

    $breadcrumb = new Breadcrumb();

    $links[] = Link::createFromRoute($this->t('Home'), '<front>');

    $omdb_api = $route_match->getParameter('omdb_api');
    $links[] = Link::createFromRoute($this->t('OMDB API'), 'entity.omdb_api.collection');
    $links[] = Link::createFromRoute(ucfirst($omdb_api->getType()), '<none>');

    $links[] = Link::createFromRoute($omdb_api->label(), '<none>');

    $breadcrumb->setLinks($links);

    // This breadcrumb builder is based on a route parameter, and hence it
    // depends on the 'route' cache context.
    $breadcrumb->addCacheContexts(['route']);
    return $breadcrumb;

  }

}
