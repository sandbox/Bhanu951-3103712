<?php

namespace Drupal\omdb_api\Service;

/**
 * Interface Class to Create or Update OMDB API Content.
 */
interface OmdbApiContentServiceInterface {

  /**
   * Method to Create Content.
   *
   * @parm array $response_data
   *   Array of the Response data.
   */
  public function createContent($response_data);

  /**
   * Method to Update Content.
   *
   * @parm array $response_data
   *   Array of the Response data.
   */
  public function updateContent($response_data);

  /**
   * Method to generate QR Code Image.
   *
   * @param string $imdb_id
   *   File Name of the QR Code Image Generated.
   * @param string $url
   *   URL Data to be used for QR Code Image.
   */
  public function generateQrCodeImage($imdb_id, $url);

}
