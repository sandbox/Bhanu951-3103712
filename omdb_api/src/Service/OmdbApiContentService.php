<?php

namespace Drupal\omdb_api\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\omdb_api\Entity\OmdbApiEntityInterface;
use Endroid\QrCode\Color\Color;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelLow;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\Label\Label;
use Endroid\QrCode\Logo\Logo;
use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use Endroid\QrCode\Writer\PngWriter;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\File\FileSystemInterface;

/**
 * Class to Create or Update OMDB API Content.
 */
class OmdbApiContentService implements OmdbApiContentServiceInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * File system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  public $fileSystem;

  /**
   * The extension path resolver.
   *
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected $extensionPathResolver;

  /**
   * The file_url_generator service.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * Constructs an Omdb Api Content Service object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   Object of file_system service.
   * @param \Drupal\Core\Extension\ExtensionPathResolver $extension_path_resolver
   *   The extension path resolver.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   The file_url_generator service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory, LoggerChannelFactoryInterface $logger_factory, MessengerInterface $messenger, FileSystemInterface $file_system, ExtensionPathResolver $extension_path_resolver, FileUrlGeneratorInterface $file_url_generator) {
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->logger = $logger_factory->get('omdb_api');
    $this->messenger = $messenger;
    $this->fileSystem = $file_system;
    $this->extensionPathResolver = $extension_path_resolver;
    $this->fileUrlGenerator = $file_url_generator;
  }

  /**
   * Method to Create Content.
   *
   * @parm array $response_data
   *   Array of the Response data.
   */
  public function createContent($response_data) {

    if (!isset($response_data['Title']) || !isset($response_data['imdbID'])) {
      return '';
    }

    /** @var \Drupal\omdb_api\Entity\OmdbApiEntityInterface $omdb_entity_data */
    $omdb_entity_data = $this->entityTypeManager->getStorage('omdb_api')->loadByProperties(
      [
        'imdb_title' => $response_data['Title'],
        'imdb_id' => $response_data['imdbID'],
      ],
    );

    if (!empty($omdb_entity_data)) {

      $existing_entity_id = key($omdb_entity_data);

      /** @var \Drupal\omdb_api\Entity\OmdbApiEntityInterface $omdb_entity */
      $omdb_entity = $this->entityTypeManager->getStorage('omdb_api')->load($existing_entity_id);
      $omdb_entity_object = $this->prepareData($omdb_entity, $response_data);
      if ($omdb_entity_object instanceof OmdbApiEntityInterface) {
        $omdb_entity_object->setNewRevision(TRUE);
        $omdb_entity_object->setRevisionUserId(0);
        $omdb_entity_object->setRevisionCreationTime(time());
        $omdb_entity_object->setRevisionLogMessage('Created New Revision for omdb_api ' . $existing_entity_id);
        $omdb_entity_object->save();
      }

    }
    else {

      /** @var \Drupal\omdb_api\Entity\OmdbApiEntityInterface $omdb_entity */
      $omdb_entity = $this->entityTypeManager->getStorage('omdb_api')->create();
      $omdb_entity_object = $this->prepareData($omdb_entity, $response_data);
      if ($omdb_entity_object instanceof OmdbApiEntityInterface) {
        $omdb_entity_object->save();
      }

    }

    return '';
  }

  /**
   * Method to Update Content.
   */
  public function updateContent($response_data) {

    return '';
  }

  /**
   * Method to Update Content.
   */
  protected function prepareData($omdb_entity, $response_data) {

    if ($omdb_entity instanceof OmdbApiEntityInterface) {

      if (isset($response_data['Title'])) {
        $omdb_entity->setImdbTitle($response_data['Title']);
      }
      if (isset($response_data['imdbID'])) {
        $omdb_entity->setImdbId($response_data['imdbID']);
      }
      if (isset($response_data['Language'])) {
        $omdb_entity->setLanguage($response_data['Language']);
      }
      if (isset($response_data['Year'])) {
        $omdb_entity->setReleasedYear($response_data['Year']);
      }
      if (isset($response_data['Released'])) {
        $omdb_entity->setReleasedDate($response_data['Released']);
      }
      if (isset($response_data['Runtime'])) {
        $omdb_entity->setRuntime($response_data['Runtime']);
      }
      if (isset($response_data['Genre'])) {
        $omdb_entity->setGenre($response_data['Genre']);
      }
      if (isset($response_data['Director'])) {
        $omdb_entity->setDirector($response_data['Director']);
      }
      if (isset($response_data['Writer'])) {
        $omdb_entity->setWriter($response_data['Writer']);
      }
      if (isset($response_data['Actors'])) {
        $omdb_entity->setActors($response_data['Actors']);
      }
      if (isset($response_data['Language'])) {
        $omdb_entity->setLanguage($response_data['Language']);
      }
      if (isset($response_data['Country'])) {
        $omdb_entity->setCountry($response_data['Country']);
      }
      if (isset($response_data['Awards'])) {
        $omdb_entity->setAwards($response_data['Awards']);
      }
      if (isset($response_data['Poster'])) {
        $omdb_entity->setPoster($response_data['Poster']);
      }
      if (isset($response_data[''])) {
        $omdb_entity->setRatings($response_data['Ratings']);
      }
      if (isset($response_data['Metascore'])) {
        $omdb_entity->setMetascore($response_data['Metascore']);
      }
      if (isset($response_data['imdbRating'])) {
        $omdb_entity->setimdbRating($response_data['imdbRating']);
      }
      if (isset($response_data['imdbVotes'])) {
        $omdb_entity->setimdbVotes($response_data['imdbVotes']);
      }
      if (isset($response_data['Type'])) {
        $omdb_entity->setType($response_data['Type']);
      }
      if (isset($response_data['DVD'])) {
        $omdb_entity->setDvdReleasedYear($response_data['DVD']);
      }
      if (isset($response_data['BoxOffice'])) {
        $omdb_entity->setBoxOfficeCollections($response_data['BoxOffice']);
      }
      if (isset($response_data['Production'])) {
        $omdb_entity->setProductionHouse($response_data['Production']);
      }
      if (isset($response_data['Website'])) {
        $omdb_entity->setWebsite($response_data['Website']);
      }
      if (isset($response_data['Response'])) {
        $omdb_entity->setApiResponseStatus($response_data['Response']);
      }
      if (isset($response_data['Response'])) {
        $omdb_entity->setPublished($response_data['Response']);
      }

      return $omdb_entity;

    }

  }

  /**
   * Method to generate QR Code Image.
   *
   * @param string $imdb_id
   *   File Name of the QR Code Image Generated.
   * @param string $url
   *   URL Data to be used for QR Code Image.
   */
  public function generateQrCodeImage($imdb_id, $url) {

    $writer = new PngWriter();

    // Create QR code.
    $qrCode = QrCode::create($url)
      ->setEncoding(new Encoding('UTF-8'))
      ->setErrorCorrectionLevel(new ErrorCorrectionLevelLow())
      ->setSize(300)
      ->setMargin(10)
      ->setRoundBlockSizeMode(new RoundBlockSizeModeMargin())
      ->setForegroundColor(new Color(0, 0, 0))
      ->setBackgroundColor(new Color(255, 255, 255, 127));

    // Get the Module path to get logo path.
    $module_path = $this->extensionPathResolver->getPath('module', 'omdb_api');
    $logo_path = $module_path . '/source/images/symfony.png';

    // Create generic logo.
    $logo = Logo::create($logo_path)->setResizeToWidth(50);

    // Create generic label.
    $label = Label::create($imdb_id)->setTextColor(new Color(255, 0, 0));

    $result = $writer->write($qrCode, $logo, $label);

    $default_scheme = $this->configFactory->get('system.file')->get('default_scheme');
    $destination = $this->fileSystem->realpath($default_scheme . "://omdb-api/qrcodes");
    $this->fileSystem->prepareDirectory($destination, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
    $is_writable = is_writable($destination);
    $is_directory = is_dir($destination);
    if (!$is_writable || !$is_directory) {
      $destination = $this->fileSystem->getTempDirectory();
    }

    // Set the generated qr code image path.
    $qr_image_path = 'public://omdb-api/qrcodes/' . $imdb_id . '.png';

    // Save the image to the given path.
    $result->saveToFile($qr_image_path);

    // Get the absolute path of the QR Code Image generated.
    $qr_code_image_link = $this->fileUrlGenerator->generateAbsoluteString($qr_image_path);

    return $qr_code_image_link;
  }

}
