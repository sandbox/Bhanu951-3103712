<?php

namespace Drupal\omdb_api\Service;

/**
 * OMDB API Http Client Service Interface.
 */
interface OmdbApiClientServiceInterface {

  /**
   * Perform a GET request against the OMDB API with IMDB ID.
   *
   * @param string $api_end_point
   *   API End Point Type.
   * @param string $imdb_id
   *   IMDB ID.
   *
   * @return array
   *   The JSON response decoded to an array.
   */
  public function getDataByImbdId(string $api_end_point, string $imdb_id);

  /**
   * Perform a GET request against the OMDB API with IMDB Title.
   *
   * @param string $api_end_point
   *   API End Point Type.
   * @param string $imdb_title
   *   IMDB Title.
   *
   * @return array
   *   The JSON response decoded to an array.
   */
  public function getDataByImbdTitle(string $api_end_point, string $imdb_title);

  /**
   * Perform a GET request against the OMDB API with Search Parameter.
   *
   * @param string $api_end_point
   *   API End Point Type.
   * @param string $search_parameter
   *   IMDB Search Parameter.
   *
   * @return array
   *   The JSON response decoded to an array.
   */
  public function getDataByImbdSearchParameter(string $api_end_point, string $search_parameter);

}
