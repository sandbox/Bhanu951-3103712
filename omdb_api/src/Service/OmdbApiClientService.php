<?php

namespace Drupal\omdb_api\Service;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * OMDB API Http Client Service.
 */
class OmdbApiClientService implements OmdbApiClientServiceInterface {

  use StringTranslationTrait;

  /**
   * The default cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * OMDB API config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The HTTP client service.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructor for OMDB API Client Service.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The default cache backend.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(CacheBackendInterface $cache_backend, ConfigFactoryInterface $config_factory, TimeInterface $time, ClientInterface $http_client, LoggerChannelFactoryInterface $logger_factory, MessengerInterface $messenger) {
    $this->cache = $cache_backend;
    $this->config = $config_factory->get('omdb_api_credentials.settings');
    $this->time = $time;
    $this->httpClient = $http_client;
    $this->logger = $logger_factory->get('omdb_api');
    $this->messenger = $messenger;
  }

  /**
   * Perform a GET request against the OMDB API with IMDB ID.
   *
   * @param string $api_end_point
   *   API End Point Type.
   * @param string $imdb_id
   *   IMDB ID.
   *
   * @return array
   *   The JSON response decoded to an array.
   */
  public function getDataByImbdId(string $api_end_point, string $imdb_id) {

    $response = [];

    $omdb_api_key = $this->config->get('omdb_api_key') ?: getenv('OMDB_API_KEY');

    if (empty($omdb_api_key)) {
      // Display the access denied page if the Credentials are Empty.
      throw new AccessDeniedHttpException('Access Denied. Credentials are Empty.');
    }

    // Cache key is like - 'omdb_api:imdb_id:data:tt0000001'.
    $cache_key = 'omdb_api:imdb_id:data:';
    // Get cached data.
    $cache = $this->cache->get($cache_key);
    // If already cached return cached data.
    if ($cache) {
      return (array) Json::decode($cache->data);
    }

    $imdb_id = str_replace('tt', '', $imdb_id);
    $imdb_id = 'tt' . str_pad($imdb_id, 7, '0', STR_PAD_LEFT);

    $headers = [
      'Content-Type' => 'application/json charset=utf-8',
      "Cache-Control" => "no-cache",
    ];
    $method = 'GET';
    $query = [
      'apikey' => $omdb_api_key,
      'i' => $imdb_id,
      'type' => $api_end_point,
      'r' => 'json',
    ];

    $options = [
      'headers' => $headers,
      // Response timeout.
      'timeout' => 60,
      // Connection timeout.
      'connect_timeout' => 60,
      'synchronous' => TRUE,
      'version' => '1.1',
      'http_errors' => FALSE,
      // Use the system's CA bundle .
      // SSL Verification.
      // @todo Change it to TRUE After Debugging.
      'verify' => TRUE,
      'allow_redirects' => [
        // @todo Change it to TRUE After Debugging.
        'strict' => TRUE,
        'protocols' => ['https', 'http'],
        'max' => 10,
        'referer' => TRUE,
        'track_redirects' => TRUE,
      ],
      'query' => $query,
    ];

    // Call the API to get response Data.
    $response = $this->getApiData($method, $options, $cache_key, $imdb_id);
    return $response;
  }

  /**
   * Perform a GET request against the OMDB API with IMDB Title.
   *
   * @param string $api_end_point
   *   API End Point Type.
   * @param string $imdb_title
   *   IMDB Title.
   *
   * @return array
   *   The JSON response decoded to an array.
   */
  public function getDataByImbdTitle(string $api_end_point, string $imdb_title) {

    $response = [];

    $omdb_api_key = $this->config->get('omdb_api_key') ?: getenv('OMDB_API_KEY');

    // Cache key is like - 'omdb_api:data:some title text string'.
    $cache_key = 'omdb_api:imdb_title:data:' . $imdb_title;
    // Get cached data.
    $cache = $this->cache->get($cache_key);
    // If already cached return cached data.
    if ($cache) {
      return (array) Json::decode($cache->data);
    }

    $headers = [
      'Content-Type' => 'application/json charset=utf-8',
      "Cache-Control" => "no-cache",
    ];
    $method = 'GET';
    $query = [
      'apikey' => $omdb_api_key,
      't' => $imdb_title,
      'type' => $api_end_point,
      'r' => 'json',
    ];

    $options = [
      'headers' => $headers,
      // Response timeout.
      'timeout' => 60,
      // Connection timeout.
      'connect_timeout' => 60,
      'synchronous' => TRUE,
      'version' => '1.1',
      'http_errors' => FALSE,
      // Use the system's CA bundle .
      // SSL Verification.
      // @todo Change it to TRUE After Debugging.
      'verify' => TRUE,
      'allow_redirects' => [
        // @todo Change it to TRUE After Debugging.
        'strict' => TRUE,
        'protocols' => ['https', 'http'],
        'max' => 10,
        'referer' => TRUE,
        'track_redirects' => TRUE,
      ],
      'query' => $query,
    ];

    // Call the API to get response Data.
    $response = $this->getApiData($method, $options, $cache_key, $imdb_title);
    return $response;

  }

  /**
   * Perform a GET request against the OMDB API with Search Parameter.
   *
   * @param string $api_end_point
   *   API End Point Type.
   * @param string $search_parameter
   *   IMDB Search Parameter.
   *
   * @return array
   *   The JSON response decoded to an array.
   */
  public function getDataByImbdSearchParameter(string $api_end_point, string $search_parameter) {

    $response = [];

    $omdb_api_key = $this->config->get('omdb_api_key') ?: getenv('OMDB_API_KEY');

    // Cache key is like -
    // 'omdb_api:imdb_search_parameter:data:some-text-string'.
    $cache_key = 'omdb_api:imdb_search_parameter:data:';
    // Get cached data.
    $cache = $this->cache->get($cache_key);
    // If already cached return cached data.
    if ($cache) {
      return (array) Json::decode($cache->data);
    }

    $headers = [
      'Content-Type' => 'application/json charset=utf-8',
      "Cache-Control" => "no-cache",
    ];
    $method = 'GET';
    $query = [
      'apikey' => $omdb_api_key,
      's' => $search_parameter,
      'type' => $api_end_point,
      'r' => 'json',
    ];

    $options = [
      'headers' => $headers,
      // Response timeout.
      'timeout' => 60,
      // Connection timeout.
      'connect_timeout' => 60,
      'synchronous' => TRUE,
      'version' => '1.1',
      'http_errors' => FALSE,
      // Use the system's CA bundle .
      // SSL Verification.
      // @todo Change it to TRUE After Debugging.
      'verify' => TRUE,
      'allow_redirects' => [
        // @todo Change it to TRUE After Debugging.
        'strict' => TRUE,
        'protocols' => ['https', 'http'],
        'max' => 10,
        'referer' => TRUE,
        'track_redirects' => TRUE,
      ],
      'query' => $query,
    ];

    // Call the API to get response Data.
    $response = $this->getApiData($method, $options, $cache_key, $search_parameter);
    return $response;

  }

  /**
   * Method to get data from API Endpoint.
   *
   * @param string $method
   *   API Endpoint method.
   * @param array $options
   *   API Endpoint options.
   * @param string $cache_key
   *   Response Cache Key.
   * @param string $search_parameter
   *   IMDB Search Parameter.
   *
   * @return array|Null
   *   The JSON response decoded to an array.
   */
  protected function getApiData($method, array $options, $cache_key, $search_parameter) {

    $omdb_api_url = $this->config->get('omdb_api_url') ?: 'https://www.omdbapi.com/';
    $cache_lifetime = $this->config->get('omdb_api_cache_max_lifetime');

    if (!isset($omdb_api_url) || !isset($cache_lifetime)) {
      // Display the access denied page if the Credentials are Empty.
      throw new AccessDeniedHttpException('Access Denied. Credentials are Empty.');
    }
    $input = preg_replace("/[^a-zA-Z]+/", "-", $search_parameter);

    $cache_id = $cache_key . $input;

    try {
      $response = $this->httpClient->request(
        $method,
        $omdb_api_url,
        $options
      );

      if ($response->getStatusCode() == 200) {

        $data = (string) $response->getBody();

        // Set the cache.
        if (!empty($cache_lifetime)) {
          $cache_lifetime += $this->time->getRequestTime();
          $this->cache->set($cache_id, $data, $cache_lifetime, [$cache_key . $search_parameter]);
        }
        return (array) Json::decode($data);
      }
      else {
        $this->messenger->addMessage($this->t('Invalid Data Received'), 'error');
        return NULL;
      }
    }
    catch (GuzzleException $e) {
      $this->logger->notice($e->getMessage());
      $this->messenger->addMessage($e->getMessage(), 'error');
      return NULL;
    }
  }

}
