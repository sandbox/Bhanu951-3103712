<?php

namespace Drupal\omdb_api\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Access\CsrfTokenGenerator;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class to Export OMDB API Content.
 */
class OmdbApiEntityExportService {

  use StringTranslationTrait;
  use DependencySerializationTrait;

  /**
   * Path of log file for instant generations.
   */
  protected const CSV_EXPORT_DIR = 'omdb-api/csv-files';

  /**
   * Path of log file for instant generations.
   */
  protected const CSV_FILE_NAME = 'omdb-api-csv-export-';

  /**
   * Path of log file for instant generations.
   */
  protected const CSV_EXT = '.csv';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * File system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  public $fileSystem;

  /**
   * The file URL generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface|null
   */
  protected $fileUrlGenerator;

  /**
   * The CSRF token generator.
   *
   * @var \Drupal\Core\Access\CsrfTokenGenerator
   */
  protected $csrfToken;

  /**
   * Protected requestStack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs an Omdb Api Content Service object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   Object of file_system service.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   The file URL generator service.
   * @param \Drupal\Core\Access\CsrfTokenGenerator $csrf_token
   *   The CSRF token generator.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   Request object to get request params.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory, LoggerChannelFactoryInterface $logger_factory, MessengerInterface $messenger, FileSystemInterface $file_system, FileUrlGeneratorInterface $file_url_generator, CsrfTokenGenerator $csrf_token, RequestStack $request) {
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->logger = $logger_factory->get('omdb_api');
    $this->messenger = $messenger;
    $this->fileSystem = $file_system;
    $this->fileUrlGenerator = $file_url_generator;
    $this->csrfToken = $csrf_token;
    $this->requestStack = $request;

  }

  /**
   * Generate CSV file from Array.
   *
   * @param array $file_data
   *   Data to export as CSV.
   */
  public function generateCsvFile(array $file_data) {

    $dir = $this->getDirPath();
    // Open CSV only if the respective directory exist and writable.
    if ($this->ensureDirExistAndWritable($dir)) {
      $destination = $this->getCompleteFileName($dir);
      $file = fopen($destination, "w+");

      // Defining header for csv.
      fputcsv($file, array_keys($file_data[0]));

      foreach ($file_data as $data) {
        // Convert timestamp to human readable datetime format.
        if (isset($data['created'])) {
          $data['created'] = $this->timestampToDate($data['created'], 'Y-M-d H:i:s');
        }
        // Convert each element to string to avoid long integer converted to
        // scintfic notation issue.
        fputcsv($file, array_map(function ($value) {
          return $value;
        }, $data));
      }

      fclose($file);

      // Return file path.
      return $destination;
    }
    else {
      throw new \Exception('The export directory is not created or writable.');
    }
  }

  /**
   * Gets the path where Csv file will be created.
   *
   * @return string
   *   Path of the CSV File.
   */
  public function getCsvFilePath() {

    $dir = $this->getDirPath();
    // Open CSV only if the respective directory exist and writable.
    if ($this->ensureDirExistAndWritable($dir)) {
      $destination = $this->getCompleteFileName($dir);
      // Return file path.
      return $destination;
    }
    else {
      throw new \Exception('The export directory is not created or writable.');
    }
  }

  /**
   * Gets the directory path where log file will be created.
   *
   * @return string
   *   Directory of where the log file will be created.
   */
  public function getDirPath() {

    $default_scheme = $this->configFactory->get('system.file')->get('default_scheme');
    $location = $default_scheme . "://" . self::CSV_EXPORT_DIR;

    return $location;
  }

  /**
   * Ensure that directory exist and writable for logs.
   *
   * @param string $path
   *   The directory to for files to create.
   *
   * @return bool
   *   TRUE if the directory exists and is writable. FALSE otherwise.
   */
  public function ensureDirExistAndWritable(string $path) {
    return $this->fileSystem->prepareDirectory(
      $path,
      FileSystemInterface::CREATE_DIRECTORY || FileSystemInterface::MODIFY_PERMISSIONS
    );
  }

  /**
   * Gets the complete fil name for log.
   *
   * @param string $directory
   *   The directory where file needs to be created.
   *
   * @return string
   *   Complete file path with file name.
   */
  public function getCompleteFileName(string $directory) {
    $create_time = date('Y-m-d-H-i-s');
    $path = $directory . DIRECTORY_SEPARATOR . self::CSV_FILE_NAME . $create_time . self::CSV_EXT;
    return $path;
  }

  /**
   * Convert timestamp to Human Readable Date.
   *
   * @param int $timestamp
   *   Unix timestamp in miliseconds.
   * @param string $format
   *   PHP date-time format.
   *
   * @return string
   *   Date in specified format.
   */
  public function timestampToDate($timestamp, $format) {

    // Converts Timestamp to human readable date.
    $date = DrupalDateTime::createFromTimestamp($timestamp, 'UTC');
    return $date->format($format);
  }

  /**
   * Processor for batch operations.
   *
   * @param string $file_name
   *   CSV File Name.
   * @param array $header
   *   Header For CSV File.
   * @param array $entity_data
   *   Entity Data of OMDB API.
   * @param array $context
   *   Batch Context array.
   *
   * @see https://www.keboca.com/drupal-8-batch-operations
   */
  public function processItems($file_name, array $header, array $entity_data, array &$context) {

    // Elements per operation.
    $limit = 50;

    // Set default progress values.
    if (empty($context['sandbox']['progress'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($entity_data);
      // Create a empty file and set initial row, which is header.
      file_put_contents($file_name, implode(",", $header) . "\n", FILE_APPEND);
    }

    // Save items to array which will be changed during processing.
    if (empty($context['sandbox']['items'])) {
      $context['sandbox']['items'] = $entity_data;
    }

    $counter = 0;
    if (!empty($context['sandbox']['items'])) {
      // Remove already processed items.
      if ($context['sandbox']['progress'] != 0) {
        array_splice($context['sandbox']['items'], 0, $limit);
      }

      foreach ($context['sandbox']['items'] as $data) {
        if ($counter != $limit) {

          $this->processItem($file_name, $header, $data);
          $counter++;
          $context['sandbox']['progress']++;

          $context['message'] = $this->t('Now Processing Data Item :progress of :count', [
            ':progress' => $context['sandbox']['progress'],
            ':count' => $context['sandbox']['max'],
          ]);

          // Increment total processed item values. Will be used in finished
          // callback.
          $context['results']['processed'] = $context['sandbox']['progress'];
        }
      }

    }

    // If not finished all tasks, we count percentage of process. 1 = 100%.
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
    $context['results']['file_name'] = $file_name;

  }

  /**
   * Process single item.
   *
   * @param string $file_name
   *   CSV File Name.
   * @param array $header
   *   Header For CSV File.
   * @param array $data
   *   OMDB API Content Data.
   */
  public function processItem($file_name, array $header, array $data) {

    if (!empty($data)) {

      file_put_contents($file_name, implode(",", $data) . "\n", FILE_APPEND);

      $message = $this->t(
        'OMDB API Content with : @data.',
        [
          '@data' => print_r($data, TRUE),
        ]
      );
      $this->logger->notice($message);
      // $this->messenger->addStatus($message);
    }
  }

  /**
   * The finished callback for the OMDB API entity content export.
   *
   * @param bool $success
   *   A boolean if the batch process was successful.
   * @param array $results
   *   An array of results for the given batch process.
   * @param array $operations
   *   An array of batch operations that were performed.
   */
  public function finished($success, array $results, array $operations) {

    $message = ($success) ? $this->t('Number of Items processed by batch: @count', [
      '@count' => $results['processed'],
    ]) : $this->t('Some Error Occurred.');

    $this->messenger->addStatus($message);
    $this->logger->info($message);
    if ($success && isset($results['file_name'])) {
      $file_name = $results['file_name'];
      /** @var \Drupal\Core\Access\CsrfTokenGenerator $csrf_token */
      $csrf_token = $this->csrfToken->get($file_name);
      $download_url = $this->fileUrlGenerator->generateAbsoluteString($file_name);

      $export_message = $this->t("File Created Success Fully. Click <a data-auto-download href='@download_url?token=@token'>here</a> to download the file.", [
        '@download_url' => $download_url,
        '@token' => $csrf_token,
      ]);
      $this->messenger->addStatus($export_message);
    }

  }

}
