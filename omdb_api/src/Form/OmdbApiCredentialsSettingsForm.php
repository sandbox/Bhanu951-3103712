<?php

namespace Drupal\omdb_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure OMDB API Credentials Settings for this site.
 */
class OmdbApiCredentialsSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'omdb_api_credentials_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['omdb_api_credentials.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['omdb_api_credentials_settings_form_description'] = [
      '#type' => 'item',
      '#markup' => $this->t('OMDB API Credentials Settings Form is used to store OMDB API Credentials.'),
    ];

    $form['omdb_api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OMDB API URL'),
      '#description' => $this->t('Enter OMDB API URL'),
      '#default_value' => $this->config('omdb_api_credentials.settings')->get('omdb_api_url'),
      '#required' => TRUE,
    ];
    $form['omdb_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OMDB API Key'),
      '#description' => $this->t('Enter OMDB API Key'),
      '#default_value' => $this->config('omdb_api_credentials.settings')->get('omdb_api_key'),
      '#required' => TRUE,
    ];
    $form['omdb_api_cache_max_lifetime'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OMDB API Data Max Cache Duration'),
      '#description' => $this->t('Enter OMDB API Data Max Cache Duration'),
      '#default_value' => $this->config('omdb_api_credentials.settings')->get('omdb_api_cache_max_lifetime') ?? '10800',
      '#required' => TRUE,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $omdb_api_key = $form_state->getValue('omdb_api_key');
    $omdb_api_url = $form_state->getValue('omdb_api_url');
    $cache_max_lifetime = $form_state->getValue('omdb_api_cache_max_lifetime');
    if (filter_var($omdb_api_url, FILTER_VALIDATE_URL) === FALSE) {
      $form_state->setErrorByName('omdb_api_url', $this->t('The OMDB API URL Entered is not valid.'));
    }
    if (strlen($omdb_api_key) != 8) {
      $form_state->setErrorByName('omdb_api_key', $this->t('The OMDB API Key Entered is not valid.'));
    }
    if (!is_numeric($cache_max_lifetime)) {
      $form_state->setErrorByName('omdb_api_cache_max_lifetime', $this->t('The OMDB API Data Max Cache Duration Value Entered is not valid.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('omdb_api_credentials.settings')
      ->set('omdb_api_key', $form_state->getValue('omdb_api_key'))
      ->set('omdb_api_url', $form_state->getValue('omdb_api_url'))
      ->set('omdb_api_cache_max_lifetime', $form_state->getValue('omdb_api_cache_max_lifetime'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
