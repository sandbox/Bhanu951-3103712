<?php

namespace Drupal\omdb_api\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\core\Url;
use Drupal\Component\Utility\Html;

/**
 * Provides a OMDB API Search Form.
 */
class OmdbApiSearchForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'omdb_api_search_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['search_string'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OMDB Movie Search'),
      '#description' => $this->t('Enter Movie Name or OMDB API Id.'),
      '#required' => TRUE,
    ];

    $form['api_end_point'] = [
      '#type' => 'radios',
      '#title' => $this->t('Search Movies or Series'),
      '#default_value' => 'movie',
      '#required' => TRUE,
      '#options' => [
        'movie' => $this->t('Movie'),
        'series' => $this->t('Series'),
      ],
    ];

    $form['search_by'] = [
      '#type' => 'select',
      '#title' => $this->t('Search By'),
      '#default_value' => 'search-parameter',
      '#required' => TRUE,
      '#options' => [
        'id' => $this->t('Imdb Id'),
        'name' => $this->t('Movie or Series Name'),
        'search-parameter' => $this->t('Search by String'),
      ],
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (mb_strlen($form_state->getValue('search_string')) < 4) {
      $form_state->setErrorByName('search_string', $this->t('Search string should be at least 4 characters.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $search_string = Html::escape($form_state->getValue('search_string'));
    $api_end_point = $form_state->getValue('api_end_point');
    $search_by = $form_state->getValue('search_by');

    // Redirect with params and options.
    $route_name = 'omdb_api.response_controller';
    $params = [
      'api_end_point' => $api_end_point,
      'search_by' => $search_by,
      'search_string' => $search_string,
    ];
    $options = [];
    $form_state->setRedirect($route_name, $params, $options);
    $url_params = [];
    $url_options = [
      'absolute' => TRUE,
      'attributes' => [
        'class' => 'omdb-api-search-form-class',
      ],
    ];
    // Create Link for the search form.
    $url = Url::fromRoute('omdb_api.search_form', $url_params, $url_options)->toString();
    $this->messenger()->addStatus($this->t('Click <a href="@search_form" target="_blank">here</a> to search another movie or series.', ['@search_form' => $url]));

  }

}
