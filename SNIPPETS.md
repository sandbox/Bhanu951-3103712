# Reference for Snippets.

## Form API Reference.
https://api.drupal.org/api/drupal/developer%21topics%21forms_api_reference.html/7.x

## DB Query Examples.
https://web.archive.org/web/20171117192314/http://www.eilyin.name/note/database-queries-drupal-8-7

## Form and render elements.
https://api.drupal.org/api/drupal/elements

## Less is More.
https://guusvandewal.nl/drupal-blog/less-more

http://lesscss.org

## Drupal API Documentation!.
https://api.drupal.org/api/drupal/developer--topics--forms_api_reference.html

https://jbloomfield.codes/2018/09/10/drupal-8-creating-your-own-services.html

https://gitlab.com/davidjguru/drupal-custom-modules-examples

https://druki.ru/wiki/8/services/dependency-injection

## Config Updater:
$module='module_name';

\Drupal::service('config.installer')->installDefaultConfig('module', $module);

## Create entities by code
https://www.flocondetoile.fr/blog/creer-des-entites-par-le-code

## Invalidate the block cache to update custom block-based derivatives.

`\Drupal::service('plugin.manager.block')->clearCachedDefinitions();`

## DI Example.

https://jbloomfield.codes/2018/09/10/drupal-8-creating-your-own-services.html

## Review a drupal.org project online

https://pareview.sh/

## Countries List :

https://raw.githubusercontent.com/annexare/Countries/master/data/countries.json

https://mledoze.github.io/countries/

https://raw.githubusercontent.com/wedeploy-examples/supermarket-web-example/master/products.json

## Menu Plugin Derivative 

https://vdmi.nl/blog/generating-menu-items-drupal-8

## Breadcrumb Reference 

https://medium.com/@kporras07/creating-breadcrumbs-in-drupal-8-3a5e6d888e5b

https://www.valuebound.com/resources/blog/how-to-create-breadcrumb-drupal-8

https://www.flocondetoile.fr/blog/customizing-breadcrumb-trail-drupal-8

https://drupal.stackexchange.com/questions/151133/how-do-you-implement-a-breadcrumb

https://drupal.stackexchange.com/questions/179857/how-to-add-the-current-title-to-the-breadcrumb-without-preprocess-function

https://git.drupalcode.org/project/current_page_crumb/-/blob/8.x-1.x/src/BreadcrumbBuilder.php

## Drush Commands 

> List Modules :

 drush pml --no-core --type=module --status=enabled

## Constrain Examples

https://makedrupaleasy.com/articles/drupal-8-custom-validator-checking-field-depending-value-another-field

https://jigarius.com/blog/drupal-entity-validation-api

https://www.philfrilling.com/blog/2018-09/custom-validation-multiple-dependent-entity-fields-drupal-8	

## Ajax Form Example.

https://github.com/lucius-digital/custom-drupal-ajax-form