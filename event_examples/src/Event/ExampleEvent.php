<?php

namespace Drupal\event_examples\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Class ExampleEvent.
 *
 * @package Drupal\event_examples\Event
 */
class ExampleEvent extends Event {

  const CUSTOM_EVENT_EXAMPLE = 'custom_event_example';

  /**
   * Email.
   *
   * @var string
   */
  protected $email;

  /**
   * Subject.
   *
   * @var string
   */
  protected $subject;

  /**
   * Body.
   *
   * @var string
   */
  protected $body;

  /**
   * ExampleEvent constructor.
   *
   * @param string $email
   *   Email address from the form.
   * @param string $subject
   *   Subject of the Email.
   * @param string $body
   *   Body of the Email.
   */
  public function __construct($email, $subject, $body) {
    $this->email = $email;
    $this->subject = $subject;
    $this->body = $body;
  }

  /**
   * Return email.
   *
   * @return string
   *   Returns EmailID of the email.
   */
  public function getEmail() {
    return $this->email;
  }

  /**
   * Return subject.
   *
   * @return string
   *   Returns Subject of the email.
   */
  public function getSubject() {
    return $this->subject;
  }

  /**
   * Return body.
   *
   * @return string
   *   Returns Body of the email.
   */
  public function getBody() {
    return $this->body;
  }

}
