<?php

namespace Drupal\event_examples\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\event_examples\Event\ExampleEvent;

/**
 * Class EventExamplesForm.
 *
 * @package Drupal\event_examples\Form
 */
class EventExamplesForm extends FormBase {

  /**
   * The event dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * EventExamplesForm constructor.
   *
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   Event Dispatcher.
   */
  public function __construct(EventDispatcherInterface $event_dispatcher) {
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('event_dispatcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['intro'] = [
      '#markup' => '<p>' . $this->t('This form will dispach event when submit') . '</p>',
    ];

    $form['email'] = [
      '#type' => 'email',
      '#required' => TRUE,
      '#title' => $this->t('Email'),
    ];

    $form['subject'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Email Subject'),
    ];

    $form['body'] = [
      '#type' => 'textarea',
      '#required' => FALSE,
      '#title' => $this->t('Statics Body'),
      '#cols' => 60,
      '#rows' => 5,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'event_examples_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $email = $form_state->getValue('email');
    $subject = $form_state->getValue('subject');
    $body = $form_state->getValue('body');

    $this->logger('event_examples')->notice('Sent an email to : @email', ['@email' => $email]);
    $this->messenger()->addMessage('Sent an email to ' . $email);

    $event = new ExampleEvent($email, $subject, $body);

    $this->eventDispatcher->dispatch(ExampleEvent::CUSTOM_EVENT_EXAMPLE, $event);

  }

}
