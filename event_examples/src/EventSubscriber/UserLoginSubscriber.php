<?php

namespace Drupal\event_examples\EventSubscriber;

use Drupal\event_examples\Event\UserLoginEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Datetime\DateFormatter;

/**
 * Class UserLoginSubscriber.
 *
 * @package Drupal\event_examples\EventSubscriber
 */
class UserLoginSubscriber implements EventSubscriberInterface {

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * UserLoginSubscriber constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger service.
   * @param \Drupal\Core\Datetime\DateFormatter $date_formatter
   *   The date formatter service.
   */
  public function __construct(Connection $database, MessengerInterface $messenger, LoggerChannelFactoryInterface $logger, DateFormatter $date_formatter) {
    $this->database = $database;
    $this->messenger = $messenger;
    $this->logger = $logger->get('event_examples');
    $this->dateFormatter = $date_formatter;

  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      // Static class constant => method on this class.
      UserLoginEvent::EVENT_EXAMPLES_USER_LOGIN => 'onUserLogin',
    ];
  }

  /**
   * React to the user login event dispatched.
   *
   * @param \Drupal\event_examples\Event\UserLoginEvent $event
   *   Date event object.
   */
  public function onUserLogin(UserLoginEvent $event) {

    $date = new DrupalDateTime('now');
    $dateNow = $this->dateFormatter->format($date->getTimestamp(), 'l, F j, Y - H:i');

    $lastLogin = $event->account->getLastLoginTime();

    $account_created = $this->database->select('users_field_data', 'ud')
      ->fields('ud', ['created'])
      ->condition('ud.uid', $event->account->id())
      ->execute()
      ->fetchField();

    $createdDate = $this->dateFormatter->format($account_created, 'long');
    $lastLoginDate = $this->dateFormatter->format($lastLogin, 'long');

    $this->messenger->addMessage(t('Welcome, your account was created on %createdDate', ['%createdDate' => $createdDate]), 'notice');

    $this->messenger->addMessage(t('You have last logged in at %login_time', ['%login_time' => $lastLoginDate]), 'notice');

    $this->logger->notice('User with UID : @uid logged at %login_time', ['@uid' => $event->account->id(), '%login_time' => $dateNow]);
  }

}
