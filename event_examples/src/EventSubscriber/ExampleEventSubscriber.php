<?php

namespace Drupal\event_examples\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\event_examples\Event\ExampleEvent;

/**
 * Class ExampleEventSubscriber.
 *
 * @package Drupal\event_examples\EventSubscriber
 */
class ExampleEventSubscriber implements EventSubscriberInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[ExampleEvent::CUSTOM_EVENT_EXAMPLE][] = ['sendEmail'];
    return $events;
  }

  /**
   * {@inheritdoc}
   */
  public function sendEmail(ExampleEvent $event) {

    $mailManager = \Drupal::service('plugin.manager.mail');
    $module = 'event_examples';
    // This will be your template id.
    $key = 'event_examples_email_statics';
    $to = $event->getEmail();
    $params = [];

    // $params['email'] = $event->getEmail();
    $params['subject'] = $event->getSubject();
    $params['body'] = $event->getBody();

    $langcode = \Drupal::currentUser()->getPreferredLangcode();
    $send = TRUE;

    $result = $mailManager->mail($module, $key, $to, $langcode, $params, $reply = NULL, $send);
    // https://api.drupal.org/comment/62797#comment-62797
    if ($result['result'] !== TRUE) {
      \Drupal::messenger()->addStatus('Email Sent.');
      // Success message.
    }
    else {
      \Drupal::messenger()->addStatus('Email Sent Failed.');
      // Failed message.
    }
  }

}
